package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.models.Chat;
import com.damagecontrolhq.internationalswagg.services.VoiceNoteService;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * InternationalSwagg
 * Created by dwilson on 11/13/15.
 */
public class VoiceMessageHolder extends SwaggViewHolder {
    private static final String TAG = VoiceMessageHolder.class.getName();
    private ImageButton actionButton;
    private SeekBar seekBar;
    private TextView playTime;
    private TextView createdAt;
    private TextView userName;

    private String audioUrl;
    private boolean isPaused = true;
    private int position;
    private Handler handler = new Handler();
    private Context context;

    private Chat chat;
    private VoiceNoteService service;

    VoiceMessageHolder(View v) {
        super(v);
        actionButton = (ImageButton) v.findViewById(R.id.audio_player_action);
        seekBar = (SeekBar) v.findViewById(R.id.seekbar);
        playTime = (TextView) v.findViewById(R.id.duration_tv);
        createdAt = (TextView)v.findViewById(R.id.date_tv);
        userName = (TextView)v.findViewById(R.id.username_tv);
        context = v.getContext();
    }

    public void fillView(final Chat chat, final VoiceNoteService service){
        Log.d(TAG, "Chat: " + chat.toJSON());
        this.service = service;
        this.chat = chat;
        this.audioUrl = chat.getMedia();

        checkFile();

        setPaused();

        if (service != null) service.setUrl(getAudioUrl());
        playTime.setText(AppUtils.milliSecondsToTimer(chat.getDuration()));
        createdAt.setText(chat.getFormattedTime());
        userName.setText(chat.getUsername());
        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserProfile(chat.getUserId());
            }
        });

        seekBar.setMax(chat.getDuration());

        buildBackground(chat);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (service == null) return;
                if (isPaused) {
                    if (fileExists()){
                        service.setUrl(SwaggApp.getInstance().getVoiceNoteDir() + chat.getFileName());
                        service.playSong();
                        setIsPlaying();
                        updateProgressBar();
                    }else{
                        downloadVoiceNote();
                    }
                }else{
                    service.pausePlayer();
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    service.seekTo(progress);
                    playTime.setText(AppUtils.milliSecondsToTimer(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // remove message Handler from updating progress bar
                handler.removeCallbacks(onEverySecond);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // remove message Handler from updating progress bar
                handler.removeCallbacks(onEverySecond);
                service.seekTo(service.getPosition());
                playTime.setText(AppUtils.milliSecondsToTimer(service.getPosition()));

                // update timer progress again
                updateProgressBar();
            }
        });
    }

    public void setService(VoiceNoteService service) {
        this.service = service;
        service.setUrl(getAudioUrl());
    }

    private void checkFile(){
        File file = new File(SwaggApp.getInstance().getVoiceNoteDir() + chat.getFileName());
        if(!file.exists()) downloadVoiceNote();
    }
    private boolean fileExists(){
        File file = new File(SwaggApp.getInstance().getVoiceNoteDir() + chat.getFileName());
        return file.exists();
    }

    private String getAudioUrl(){
        Log.d(TAG, "Chat: " + chat.toJSON());
        return SwaggApp.getInstance().getVoiceNoteDir() + chat.getFileName();
    }

    private void downloadVoiceNote(){
        SwaggApp.downloadApi().getFile(audioUrl).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, "REQUEST URL: " + call.request().url());
                if (response.isSuccessful()) {
                    try {
                        handleResponse(response.body().bytes());
                    } catch (IOException e) {
                        Log.e(TAG, "Error getting file. " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Error downloading voicenote. " + t.getMessage());
                actionButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_file_download));
            }
        });

    }

    private void handleResponse(byte[] response) {
        HashMap<String, Object> map = new HashMap<>();
        int count;
        if (response!=null) {
            try{
                InputStream input = new ByteArrayInputStream(response);

                File path = new File(SwaggApp.getInstance().getVoiceNoteDir());
                if (!path.exists()){ path.mkdirs(); }
                File file = new File(path, chat.getFileName());
                map.put("resume_path", file.toString());
                BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                byte data[] = new byte[1024];

                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    output.write(data, 0, count);
                }

                output.flush();

                output.close();
                input.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    private Runnable onEverySecond = new Runnable() {
        @Override
        public void run() {
            if (!isPaused){
                try{
                    if (service.isPlaying()) {
                        // Update seekbar position
                        position = service.getPosition();
                        // Displaying time completed playing
                        playTime.setText(AppUtils.milliSecondsToTimer(service.getPosition()));
                        seekBar.setProgress(position);
                        // Running this thread after 100 milliseconds
                        seekBar.postDelayed(onEverySecond, 100);
                    }else{
                        seekBar.setProgress(0);
                        setPaused();
                    }
                }catch (IllegalStateException e){
                    Log.d(TAG, "Service error: " + e.toString());
                    service = null;
                    setPaused();
                }
            }
        }
    };

    private void setPaused(){
        isPaused = true;
        actionButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_play_arrow_black_48dp));
    }

    private void setIsPlaying(){
        isPaused = false;
        actionButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_play_arrow_black_48dp));
    }

    private void updateProgressBar(){
        handler.postDelayed(onEverySecond, 100);
    }
}

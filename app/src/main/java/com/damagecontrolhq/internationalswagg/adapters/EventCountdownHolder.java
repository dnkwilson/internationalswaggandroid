package com.damagecontrolhq.internationalswagg.adapters;

import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.SwaggCountdownTimer;

/**
 * InternationalSwagg
 * Created by dwilson on 1/18/16.
 */
public class EventCountdownHolder extends SwaggViewHolder{
    public static final String TAG = CountdownViewHolder.class.getName();

    ImageView imageView;
    private final TextView daysTextView;
    private final TextView hoursTextView;
    private final TextView minsTextView;
    private final TextView secsTextView;
    private final ViewTimer mTimer;
    public boolean timerIsRunning = false;

    private final SwaggCountdownTimer swaggTimer = new SwaggCountdownTimer();

    EventCountdownHolder(View cdView){
        super(cdView);
        imageView     = (ImageView)cdView.findViewById(R.id.event_image);
        daysTextView  = (TextView)cdView.findViewById(R.id.days_text);
        hoursTextView = (TextView)cdView.findViewById(R.id.hours_text);
        minsTextView  = (TextView)cdView.findViewById(R.id.minutes_text);
        secsTextView  = (TextView)cdView.findViewById(R.id.seconds_text);
        secsTextView.setTextColor(ContextCompat.getColor(cdView.getContext(), R.color.accent));
        mTimer = new ViewTimer();
        mTimer.start();
    }

    public ViewTimer getTimer() {
        return mTimer;
    }

    class ViewTimer extends CountDownTimer {
        ViewTimer () {
            super(swaggTimer.getIntervalMillis(), 1000);
            Log.d("ViewTimer", "Interval in ms: " + swaggTimer.getIntervalMillis());
        }

        @Override
        public void onTick(long millisUntilFinished) {
            timerIsRunning = true;

            int secsLeft = (int)(millisUntilFinished / 1000);
            int days = secsLeft / 86400;
            int hours   = (secsLeft % 86400)/3600;
            int minutes = (secsLeft - (days * 86400) - (hours * 3600)) / 60;
            int seconds = (secsLeft % 60);

            String dayStr  = String.format("%02d", days);
            String hourStr = String.format("%02d", hours);
            String minStr  = String.format("%02d", minutes);
            String secStr  = String.format("%02d", seconds);

            daysTextView.setText(dayStr);
            hoursTextView.setText(hourStr);
            minsTextView.setText(minStr);
            secsTextView.setText(secStr);
        }

        @Override
        public void onFinish() {
            timerIsRunning = false;
        }
    }
}



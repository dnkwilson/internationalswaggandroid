package com.damagecontrolhq.internationalswagg.adapters;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.SwaggCountdownTimer;

/**
 * InternationalSwagg
 * Created by dwilson on 9/9/15.
 */
public class CountdownViewHolder extends SwaggViewHolder{
    public static final String TAG = CountdownViewHolder.class.getName();

    private final TextView daysTextView;
    private final TextView hoursTextView;
    private final TextView minsTextView;
    private final TextView secsTextView;
    private final ViewTimer mTimer;
    public boolean timerIsRunning = false;

    private SwaggCountdownTimer swaggTimer;


    CountdownViewHolder(View cdView){
        super(cdView);
        daysTextView  = (TextView)cdView.findViewById(R.id.days_text);
        hoursTextView = (TextView)cdView.findViewById(R.id.hours_text);
        minsTextView  = (TextView)cdView.findViewById(R.id.minutes_text);
        secsTextView  = (TextView)cdView.findViewById(R.id.seconds_text);
        swaggTimer    = new SwaggCountdownTimer();
        mTimer = new ViewTimer();
        mTimer.start();
    }

    public ViewTimer getTimer() {
        return mTimer;
    }

    public class ViewTimer extends CountDownTimer {
        public ViewTimer () {
            super(swaggTimer.getIntervalMillis(), 1000);
            Log.d("ViewTimer", "Interval in ms: " + swaggTimer.getIntervalMillis());
        }

        @Override
        public void onTick(long millisUntilFinished) {
            timerIsRunning = true;

            int secsLeft = (int)(millisUntilFinished / 1000);
            int days = secsLeft / 86400;
            int hours   = (secsLeft % 86400)/3600;
            int minutes = (secsLeft - (days * 86400) - (hours * 3600)) / 60;
            int seconds = (secsLeft % 60);

            String dayStr  = String.format("%02d", days);
            String hourStr = String.format("%02d", hours);
            String minStr  = String.format("%02d", minutes);
            String secStr  = String.format("%02d", seconds);

            daysTextView.setText(dayStr);
            hoursTextView.setText(hourStr);
            minsTextView.setText(minStr);
            secsTextView.setText(secStr);
        }

        @Override
        public void onFinish() {
            timerIsRunning = false;
        }
    }
}

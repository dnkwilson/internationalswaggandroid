package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.interfaces.RecyclerItemClickListener;
import com.damagecontrolhq.internationalswagg.models.Reservation;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by dwilson on 1/28/17.
 */

public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.PackageViewHolder>{

    private ArrayList<Reservation> mReservations;
    private Context mContext;
    private RecyclerItemClickListener mListener;

    public ReservationAdapter(Context c, RecyclerItemClickListener listener,  ArrayList<Reservation> list) {
        this.mContext = c;
        this.mReservations = list;
        this.mListener = listener;
    }

    @Override
    public PackageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_package_list_item, parent, false);
        return new PackageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PackageViewHolder holder, int position) {
        Reservation reservation = mReservations.get(position);

        Picasso.with(mContext).load(ImageUtils.getImageUrl(reservation.getPhoto())).into(holder.packageImage);
        String name = reservation.getName().toLowerCase();
        int bgColor, textColor;

        String packageName = mContext.getPackageName();
        bgColor = mContext.getResources().getIdentifier(name + "_bg", "color", packageName);
        textColor = mContext.getResources().getIdentifier(name, "color", packageName);

        holder.packageImage.setBackgroundColor(ContextCompat.getColor(mContext, bgColor));
        holder.packagePrice.setText(AppUtils.formatPrice(reservation.getPrice().getFormattedPrice()));
        holder.packagePrice.setTextColor(ContextCompat.getColor(mContext, textColor));

        holder.reservePackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClicked(holder, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mReservations.size();
    }

    public class PackageViewHolder extends RecyclerView.ViewHolder{
        public ImageView packageImage;
        public TextView packagePrice;
        public Button reservePackage;

        public PackageViewHolder(View view){
            super(view);
            packageImage = (ImageView) view.findViewById(R.id.package_image);
            reservePackage = (Button) view.findViewById(R.id.package_reserve_button);
            packagePrice = (TextView) view.findViewById(R.id.package_price);
        }
    }
}

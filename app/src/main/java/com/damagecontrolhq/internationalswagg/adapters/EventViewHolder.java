package com.damagecontrolhq.internationalswagg.adapters;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.damagecontrolhq.internationalswagg.R;

/**
 * InternationalSwagg
 * Created by dwilson on 9/1/15.
 */
class EventViewHolder extends SwaggViewHolder {
    final Button eventPhotos;
    final Button eventVideos;
    final ImageView eventFlyer;

    EventViewHolder(View eventView){
        super(eventView);
        eventPhotos = (Button)eventView.findViewById(R.id.event_photo_button);
        eventVideos = (Button)eventView.findViewById(R.id.event_video_button);
        eventFlyer  = (ImageView)eventView.findViewById(R.id.event_image);
    }
}

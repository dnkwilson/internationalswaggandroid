package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.ReservationItem;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;

import java.util.ArrayList;

/**
 * Created by dwilson on 2/3/17.
 */

public class ReservationProductAdapter extends RecyclerView.Adapter<ReservationProductAdapter.ReservationProductViewHolder> {
    private ArrayList<ReservationItem> mReservationItems;
    private Context mContext;

    public ReservationProductAdapter(Context context, ArrayList<ReservationItem> items){
        this.mContext = context;
        this.mReservationItems = items;
    }

    @Override
    public ReservationProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_reservation_product_item, parent, false);
        ReservationProductViewHolder holder = new ReservationProductViewHolder(v);
        v.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(ReservationProductViewHolder holder, int position) {
        holder.bind(mReservationItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mReservationItems.size();
    }

    public class ReservationProductViewHolder extends RecyclerView.ViewHolder{
        TextView productName;
        public TextView productPrice;

        public ReservationProductViewHolder(View v) {
            super(v);
            productName = (TextView) v.findViewById(R.id.product_name);
            productPrice = (TextView) v.findViewById(R.id.product_price);
        }

        public void bind(ReservationItem item){
            productName.setText(item.getProduct().getName());
            productPrice.setText(AppUtils.formatPrice(item.getProduct().getPrice().getFormattedPrice()));
        }
    }
}

package com.damagecontrolhq.internationalswagg.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.CommentListActivity;
import com.damagecontrolhq.internationalswagg.LoginActivity;
import com.damagecontrolhq.internationalswagg.PhotoAttachmentActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.DamageControlAPI;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.fragments.DashboardFragment;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.models.Chat;
import com.damagecontrolhq.internationalswagg.models.Post;
import com.damagecontrolhq.internationalswagg.models.Vote;
import com.damagecontrolhq.internationalswagg.views.SocialButton;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * InternationalSwagg
 * Created by dwilson on 10/16/15.
 */
public class PostListAdapter extends RecyclerView.Adapter<PostItemViewHolder> {
    private static final String TAG = PostListAdapter.class.getSimpleName();

    private List<Post> postList;
    private Context context;
    private SessionManager session;

    public PostListAdapter(Context context, List<Post> posts){
        this.context = context;
        this.postList = posts;
        this.session = SwaggApp.getInstance().getSession();
    }

    @Override
    public PostItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_post_item, parent, false);
        return new PostItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PostItemViewHolder holder, int position) {
        final Post post = postList.get(position);

        holder.setViews(post);
        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwaggApp.getInstance().checkLogin();
                Log.d(TAG, "Like button clicked");
                holder.like.setEnabled(false);
                if (session.isLoggedIn()) {
                    voteOnPost(holder.like, post);
                } else {
                    Intent i = new Intent(context, LoginActivity.class);
                    i.putExtra(SwaggApp.REQUESTED_PAGE, DashboardFragment.class.getName());
                    i.putExtra(SwaggApp.LOGIN_MESSAGE, "You must login before liking a post");
                    context.startActivity(i);
                }
            }
        });

        holder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwaggApp.getInstance().checkLogin();
                Intent intent = new Intent(context, CommentListActivity.class);
                intent.putExtra("post", post);
                Activity activity = (Activity)context;
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_up, R.anim.abc_fade_out);
            }
        });

        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(post.getCategory().equals("video")){
                    if(YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(context)
                            .equals(YouTubeInitializationResult.SUCCESS)){
                        //This means that your device has the Youtube API Service (the app) and you are safe to launch it.
                        Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity)context,
                                DamageControlAPI.YOUTUBE_KEY, post.getVideo(), 0, true, false);
                        context.startActivity(intent);
                    }else{
                        // Log the outcome, take necessary measure, like playing the video in webview :)
                        Toast.makeText(context, "Please install youtube to view", Toast.LENGTH_LONG).show();
                    }

                }else if(post.getCategory().equals("image")){
                    Intent i = new Intent(context, PhotoAttachmentActivity.class);
                    i.putExtra(Chat.KEY_MEDIA_FILE, post.getImage());
                    i.putExtra(Post.KEY_TITLE, post.getTitle());
                    context.startActivity(i);
                }
            }
        });
    }

    private void voteOnPost(final SocialButton like, final Post post) {
        SwaggApp.api(false).vote(post.getID()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String str = response.body().getAsJsonObject().get("meta").toString();
                try{
                    JSONObject json = new JSONObject(str);
                    boolean hasVoted = json.getBoolean("user_voted");
                    if (hasVoted){
                        Vote userVote = new Vote(json.getJSONObject("user_vote"));
                        post.getVotes().add(userVote);
                    }else {
                        post.getVotes().remove(post.userVote(session.getUser().getID()));
                    }
                    like.updateUI(post, "like");
                    like.setEnabled(true);
                }catch (JSONException e){e.printStackTrace();}
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}

package com.damagecontrolhq.internationalswagg.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damagecontrolhq.internationalswagg.PhotoAttachmentActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Chat;
import com.damagecontrolhq.internationalswagg.services.VoiceNoteService;

import java.util.List;

/**
 * InternationalSwagg
 * Created by dwilson on 9/29/15.
 */
public class ChatListAdapter extends RecyclerView.Adapter<SwaggViewHolder>{
    private static final String TAG = "ChatListAdapter";

    private static final int TEXT_MSG  = 0;
    private static final int VOICE_MSG = 1;
    private static final int PHOTO_MSG = 2;

    private final List<Chat> chatList;
    private final Activity activity;
    private final VoiceNoteService service;


    public ChatListAdapter(Activity activity, List<Chat> list, VoiceNoteService service) {
        this.activity = activity;
        this.service = service;
        this.chatList = list;
    }

    @Override
    public SwaggViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case VOICE_MSG:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_voice_message, parent, false);
                VoiceMessageHolder vHolder = new VoiceMessageHolder(view);
                view.setTag(vHolder);
                return vHolder;
            case PHOTO_MSG:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_photo_message, parent, false);
                PhotoMessageHolder pHolder = new PhotoMessageHolder(view);
                view.setTag(pHolder);
                return pHolder;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_text_message, parent, false);
                TextMessageHolder tHolder = new TextMessageHolder(view);
                view.setTag(tHolder);
                return tHolder;
        }
    }

    @Override
    public void onBindViewHolder(SwaggViewHolder holder, int position) {
        final Chat chat = chatList.get(position);
        switch (holder.getItemViewType()) {
            case VOICE_MSG:
                VoiceMessageHolder vHolder = (VoiceMessageHolder)holder;
                vHolder.fillView(chat, service);
                break;
            case PHOTO_MSG:
                PhotoMessageHolder pHolder = (PhotoMessageHolder)holder;
                pHolder.fillView(chat);
                pHolder.photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(activity, PhotoAttachmentActivity.class);
                        i.putExtra(Chat.KEY_MEDIA_FILE, chat.getMedia());
                        activity.startActivity(i);
                    }
                });
                break;
            default:
                TextMessageHolder tHolder = (TextMessageHolder)holder;
                tHolder.fillView(chat);
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Chat chat = chatList.get(position);
        switch (chat.getCategory()){
            case Chat.VOICE_MESSAGE:
                return VOICE_MSG;
            case Chat.PHOTO_MESSAGE:
                return PHOTO_MSG;
            default:
                return TEXT_MSG;
        }
    }
}
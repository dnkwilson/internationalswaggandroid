package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.DamageControlAPI;
import com.damagecontrolhq.internationalswagg.models.Post;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.damagecontrolhq.internationalswagg.views.SocialButton;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.squareup.picasso.Picasso;

/**
 * InternationalSwagg
 * Created by dwilson on 10/30/15.
 */
public class PostItemViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = PostItemViewHolder.class.getSimpleName();
    private TextView title;
    private TextView date;
    RelativeLayout body;
    SocialButton like;
    private SocialButton share;
    SocialButton comment;
    private Context context;
    private Post post;

    PostItemViewHolder(View view) {
        super(view);
        this.context = view.getContext();
        this.title   = (TextView)view.findViewById(R.id.tv_title);
        this.date    = (TextView)view.findViewById(R.id.tv_date);
        this.body    = (RelativeLayout)view.findViewById(R.id.post_container);
        this.like    = (SocialButton)view.findViewById(R.id.btn_like);
        this.comment = (SocialButton)view.findViewById(R.id.btn_comment);
        this.share   = (SocialButton)view.findViewById(R.id.btn_share);
    }

    public void setViews(Post post){
        this.post = post;
        switch (post.getCategory()){
            case "video":
                getVideoView();
                break;
            case "image":
                getImageView();
                break;
            default:
                getTextView();
        }
    }

    private void buildBaseView(){
        title.setText(post.getTitle());
        title.setMaxLines(1);
        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setTextColor(ContextCompat.getColor(context, R.color.primary_text));
        date.setText(DateUtils.getRelativeTimeSpanString(post.getDate(),
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE));

        like.updateUI(post, "like");
        comment.updateUI(post, "comment");
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_SUBJECT, post.getTitle());
                String sAux = context.getString(R.string.string_friend_invite);
                share.putExtra(Intent.EXTRA_TEXT, sAux);
                v.getContext().startActivity(Intent.createChooser(share, "Share this post"));
            }
        });
    }


    private void getTextView(){
        buildBaseView();
        TextView textView = new TextView(context);
        textView.setText(post.getBody());
        textView.setMaxLines(7);
        textView.setEllipsize(TextUtils.TruncateAt.END);
        body.addView(textView);
        textView.setTextColor(ContextCompat.getColor(context, R.color.gray_dark));
    }

    private void getVideoView(){
        buildBaseView();

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                720);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        YouTubeThumbnailView thumbnail = new YouTubeThumbnailView(context);
        thumbnail.setLayoutParams(params);
        thumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
        thumbnail.setTag(post.getVideo());
        thumbnail.initialize(DamageControlAPI.YOUTUBE_KEY, new YouTubeThumbnailView.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
                youTubeThumbnailLoader.setVideo(post.getVideo());

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.CENTER_IN_PARENT);
                ImageView playBtn = new ImageView(context);
                playBtn.setLayoutParams(params);
                playBtn.setImageResource(R.drawable.ic_youtube);
                body.addView(playBtn);
            }

            @Override
            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                final String errorMessage = youTubeInitializationResult.toString();
                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
        body.addView(thumbnail);
    }

    private void getImageView(){
        buildBaseView();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 720);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        ImageView imageView = new ImageView(context);
        Picasso.with(imageView.getContext()).load(ImageUtils.getImageUrl(post.getImage())).into(imageView);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(params);
        body.addView(imageView);
    }
}
package com.damagecontrolhq.internationalswagg.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.EventMedia;

import java.util.List;

/**
 * InternationalSwagg
 * Created by dwilson on 12/17/15.
 */
public class EventMediaListAdapter extends RecyclerView.Adapter<SwaggViewHolder> {
    public static final String TAG = EventMediaListAdapter.class.getName();

    private List<EventMedia> mList;

    public EventMediaListAdapter(List<EventMedia> list){
        this.mList = list;
    }

    @Override
    public SwaggViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_media_list_row, parent, false);
        EventMediaHolder holder = new EventMediaHolder(view);
        view.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(SwaggViewHolder holder, int position) {
        EventMedia media = mList.get(position);
        EventMediaHolder mediaHolder = (EventMediaHolder) holder;
        mediaHolder.fillView(media);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}

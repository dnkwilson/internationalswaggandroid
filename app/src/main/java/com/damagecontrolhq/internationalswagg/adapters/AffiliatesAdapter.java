package com.damagecontrolhq.internationalswagg.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.AffiliatesAdapter.AffiliateViewHolder;
import com.damagecontrolhq.internationalswagg.models.Affiliate;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.damagecontrolhq.internationalswagg.views.AffiliateFieldView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * InternationalSwagg
 * Created by dwilson on 1/11/16.
 */
public class AffiliatesAdapter extends RecyclerView.Adapter<AffiliateViewHolder>{
    public static final String TAG = AffiliatesAdapter.class.getName();
    private ArrayList<Affiliate> aList;

    public AffiliatesAdapter(ArrayList<Affiliate> affiliates){
        this.aList = affiliates;
    }

    @Override
    public AffiliateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_affiliate, parent, false);
        return new AffiliateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AffiliateViewHolder holder, int position) {
        Affiliate item = aList.get(position);
        holder.buildView(item);
    }

    @Override
    public int getItemCount() {
        return aList.size();
    }


    class AffiliateViewHolder extends RecyclerView.ViewHolder {
        AffiliateFieldView phone, ig, fb, email;
        TextView name, info;
        LinearLayout body;
        ImageView logo;

        AffiliateViewHolder(View v) {
            super(v);
            logo        = (ImageView)v.findViewById(R.id.logo);
            body        = (LinearLayout)v.findViewById(R.id.body);
            name        = (TextView)v.findViewById(R.id.name);
            info        = (TextView)v.findViewById(R.id.info);
            phone       = new AffiliateFieldView(v.getContext());
            ig          = new AffiliateFieldView(v.getContext());
            fb          = new AffiliateFieldView(v.getContext());
            email       = new AffiliateFieldView(v.getContext());
        }

        void buildView(Affiliate item){
            Picasso.with(logo.getContext()).load(ImageUtils.getImageUrl(item.getLogo())).into(logo);
            name.setText(item.getName());
            info.setText(item.getInfo());
            phone.buildField("{faw-phone}", item.getPhone(), Linkify.PHONE_NUMBERS);
            ig.buildField("{faw-instagram}", item, "Instagram");
            fb.buildField("{faw-facebook-official}", item, "Facebook");
            email.buildField("{faw-envelope}", item.getEmail(), Linkify.EMAIL_ADDRESSES);
            body.addView(phone);
            body.addView(ig);
            body.addView(fb);
            body.addView(email);
        }
    }
}

package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.ReservationItemActivity;
import com.damagecontrolhq.internationalswagg.interfaces.NestedRecyclerViewItemClickListener;
import com.damagecontrolhq.internationalswagg.models.Category;
import com.damagecontrolhq.internationalswagg.models.ReservationItem;

import java.util.ArrayList;

/**
 * Created by dwilson on 1/30/17.
 */

public class ReservationItemAdapter extends ExpandableRecyclerAdapter<Category, ReservationItem, ReservationParentViewHolder, ReservationChildViewHolder> {

    private LayoutInflater mInflater;
    private NestedRecyclerViewItemClickListener mListener;

    public ReservationItemAdapter(Context context, ArrayList<Category> items, ReservationItemActivity parent){
        super(items);
        mInflater = LayoutInflater.from(context);
        mListener = parent;
    }

    @NonNull
    @Override
    public ReservationParentViewHolder onCreateParentViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = mInflater.inflate(R.layout.view_list_item_reservation_parent, viewGroup, false);
        return new ReservationParentViewHolder(view);
    }

    @NonNull
    @Override
    public ReservationChildViewHolder onCreateChildViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = mInflater.inflate(R.layout.view_list_item_reservation_child, viewGroup, false);
        return new ReservationChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(@NonNull final ReservationChildViewHolder childViewHolder, final int parentPosition, final int childPosition, @NonNull final ReservationItem child) {
        childViewHolder.productTitle.setText(child.getProduct().getName());
        childViewHolder.productQty.setText(String.valueOf(child.getQty()));

        childViewHolder.productSwap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClicked(childViewHolder, parentPosition, childPosition, child);
            }
        });
    }

    @Override
    public void onBindParentViewHolder(@NonNull ReservationParentViewHolder parentViewHolder, int parentPosition, @NonNull Category parent) {
        parentViewHolder.categoryItemCount.setText(String.valueOf(parent.getItemCount()));
        parentViewHolder.categoryTitle.setText(parent.getName());
    }
}

package com.damagecontrolhq.internationalswagg.adapters;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.damagecontrolhq.internationalswagg.R;

/**
 * Created by dwilson on 1/30/17.
 */

public class ReservationChildViewHolder extends ChildViewHolder {
    public TextView productQty;
    public TextView productTitle;
    Button productSwap;

    public ReservationChildViewHolder(@NonNull View itemView) {
        super(itemView);

        productTitle = (TextView) itemView.findViewById(R.id.product_title);
        productQty = (TextView) itemView.findViewById(R.id.product_qty);
        productSwap = (Button) itemView.findViewById(R.id.product_swap_btn);
    }
}

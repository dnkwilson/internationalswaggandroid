package com.damagecontrolhq.internationalswagg.adapters;

import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Chat;

/**
 * InternationalSwagg
 * Created by dwilson on 11/13/15.
 */
class TextMessageHolder extends SwaggViewHolder{
    private TextView time;
    private TextView username;
    private TextView message;

    TextMessageHolder(View v) {
        super(v);
        message = (TextView)v.findViewById(R.id.message);
        username = (TextView)v.findViewById(R.id.username);
        time = (TextView)v.findViewById(R.id.time);
    }

    void fillView(final Chat chat){
        message.setText(chat.getBody());
        username.setText(chat.getUsername());
        time.setText(chat.getFormattedTime());
        Linkify.addLinks(message, Linkify.ALL);

        username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserProfile(chat.getUserId());
            }
        });

        buildBackground(chat);
    }
}

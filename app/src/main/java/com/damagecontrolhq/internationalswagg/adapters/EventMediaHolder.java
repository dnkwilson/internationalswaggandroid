package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.damagecontrolhq.internationalswagg.PhotoGalleryActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.VideoActivity;
import com.damagecontrolhq.internationalswagg.models.EventMedia;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

/**
 * InternationalSwagg
 * Created by dwilson on 12/17/15.
 */
public class EventMediaHolder extends SwaggViewHolder{
    ImageView preview;
    Button galleryBtn;

    EventMediaHolder(View v) {
        super(v);
        this.preview = (ImageView)v.findViewById(R.id.media_image);
        this.galleryBtn  = (Button)v.findViewById(R.id.galleryBtn);
    }

    public void fillView(final EventMedia item){
        Picasso.with(preview.getContext()).load(ImageUtils.getImageUrl(item.getPreview())).into(preview);
        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;

                if (item.getType().equals(EventMedia.IMAGE_MEDIA_TYPE)){
                    i = new Intent(v.getContext(), PhotoGalleryActivity.class);
                    i.putExtra(EventListAdapter.EVENT_ID, item.getEventId());
                    i.putExtra(EventListAdapter.EVENT_NAME, item.getName());
                    i.putExtra(EventListAdapter.GALLERY_ID, item.getId());
                }else{
                    i = new Intent(v.getContext(), VideoActivity.class);
                    i.putExtra(EventListAdapter.VIDEO_URL, item.getPreview());
                    v.getContext().startActivity(i);
                }

                v.getContext().startActivity(i);
            }
        });

        galleryBtn.setText("View " + item.getName() + " Gallery");
        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;

                if (item.getType().equals(EventMedia.IMAGE_MEDIA_TYPE)){
                    i = new Intent(v.getContext(), PhotoGalleryActivity.class);
                    i.putExtra(EventListAdapter.EVENT_ID, item.getEventId());
                    i.putExtra(EventListAdapter.EVENT_NAME, item.getName());
                    i.putExtra(EventListAdapter.GALLERY_ID, item.getId());
                }else{
                    i = new Intent(v.getContext(), VideoActivity.class);
                    i.putExtra(EventListAdapter.VIDEO_URL, item.getPreview());
                    v.getContext().startActivity(i);
                }

                v.getContext().startActivity(i);
            }
        });
    }
}
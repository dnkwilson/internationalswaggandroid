package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.damagecontrolhq.internationalswagg.PhotoActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Photo;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * InternationalSwagg
 * Created by dwilson on 9/1/15.
 */
public class GalleryAdapter extends BaseAdapter {
    private static final String TAG = "GalleryAdapter";

    private static final String PHOTO_ID = "photo_id";
    public static final String PHOTOS   = "photos";
    public static final String PHOTO_NAME = "photo_name";
    public static final String POSITION = "position";

    private final List<Photo> photos;
    private Photo photo;
    private final Context context;
    private final int imageWidth;

    public GalleryAdapter(List<Photo> photos, Context context, int imageWidth){
        this.photos = photos;
        this.context = context;
        this.imageWidth = imageWidth;
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public Object getItem(int position) {
        return photos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_photo, parent, false);

            holder = new ViewHolder();

            holder.imageView = (ImageView) convertView.findViewById(R.id.photo_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        photo = photos.get(position);

        holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        holder.imageView.setLayoutParams(new RelativeLayout.LayoutParams(imageWidth,
                imageWidth));
        holder.imageView.setOnClickListener(new OnImageClickListener(position));
        Picasso.with(context).load(ImageUtils.getImageUrl(photo.getUrl())).into(holder.imageView);

        return convertView;
    }

    class OnImageClickListener implements View.OnClickListener {

        final int position;

        // constructor
        public OnImageClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            // on selecting grid view image
            // launch full screen activity
            Intent i = new Intent(context, PhotoActivity.class);
            i.putExtra(PHOTO_ID, String.valueOf(photo.getId()));
            i.putExtra(PHOTO_NAME, photo.getName());
            i.putExtra(POSITION, (Integer) position);
            i.putParcelableArrayListExtra(PHOTOS, (ArrayList<Photo>) photos);
            i.putExtra(EventListAdapter.EVENT_ID, String.valueOf(photo.getId()));
            context.startActivity(i);
        }

    }

    public static class ViewHolder {
        ImageView imageView;
    }
}

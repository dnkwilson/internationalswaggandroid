package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Photo;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * InternationalSwagg
 * Created by dwilson on 9/11/15.
 */
public class PhotoAdapter extends PagerAdapter{
    private final Context context;
    private final List<Photo> photos;

    public PhotoAdapter(Context context, List<Photo> photos){
        this.context = context;
        this.photos = photos;
    }

    @Override
    public int getCount() {
        return this.photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final ImageView imageView;

        Photo photo = photos.get(position);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = layoutInflater.inflate(R.layout.fragment_photo, container,
                false);

        imageView = (ImageView) viewLayout.findViewById(R.id.photo_image);

        Picasso.with(context).load(ImageUtils.getImageUrl(photo.getUrl())).into(imageView);

        container.addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);

    }
}

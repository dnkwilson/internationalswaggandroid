package com.damagecontrolhq.internationalswagg.adapters;

import android.view.View;

import com.damagecontrolhq.internationalswagg.models.DashboardItem;
import com.damagecontrolhq.internationalswagg.views.DashIconView;

/**
 * InternationalSwagg
 * Created by dwilson on 11/28/15.
 */
class DashItemHolder extends SwaggViewHolder {
    final DashIconView icon;

    DashItemHolder(View v) {
        super(v);
        icon = (DashIconView)v;
    }

    void fillView(final DashboardItem item){
        icon.setButtonColor(item.getColor());
        icon.setTitle(item.getName());
        if (item.getName() == null) icon.getTitle().setVisibility(View.GONE);
        icon.setImage(item.getResource());
    }

}

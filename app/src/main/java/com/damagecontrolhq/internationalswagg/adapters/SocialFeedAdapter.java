package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.damagecontrolhq.internationalswagg.models.InstagramPost;
import com.damagecontrolhq.internationalswagg.views.FeedItemView;

import java.util.List;

/**
 * InternationalSwagg
 * Created by dwilson on 9/12/15.
 */
public class SocialFeedAdapter extends BaseAdapter{
    private static final String TAG = "SocialFeedAdapter";

    private List<InstagramPost> posts;
    private Context context;
    private int imageWidth;

    public SocialFeedAdapter(Context context, List<InstagramPost> posts, int imageWidth) {
        this.context = context;
        this.posts = posts;
        this.imageWidth = imageWidth;
        int startCount = 0;
        startCount = Math.min(startCount, posts.size());
    }

    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public Object getItem(int position) {
        return posts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FeedItemView feedItemView;
        if (convertView == null) {
            feedItemView = new FeedItemView(context);
        }else{
            feedItemView = (FeedItemView)convertView;
        }

        InstagramPost post = posts.get(position);

        feedItemView.buildView(post, imageWidth);
        return feedItemView;
    }
}

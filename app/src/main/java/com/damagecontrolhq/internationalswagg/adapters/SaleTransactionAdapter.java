package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.SaleTransactionItem;

import java.util.ArrayList;

/**
 * Created by dwilson on 2/12/17.
 */

public class SaleTransactionAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SaleTransactionItem> mSaleTransactionItems;

    public SaleTransactionAdapter(Context c, ArrayList<SaleTransactionItem> items){
        this.mContext = c;
        this.mSaleTransactionItems = items;
    }

    @Override
    public int getCount() {
        return mSaleTransactionItems.size();
    }

    @Override
    public Object getItem(int i) {
        return mSaleTransactionItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        SaleTransactionItem item = mSaleTransactionItems.get(i);

        final SaleTransactionAdapter.ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.view_sale_transaction_item, viewGroup, false);

            holder = new SaleTransactionAdapter.ViewHolder();

            holder.qty = (TextView) view.findViewById(R.id.transaction_item_qty);
            holder.name = (TextView) view.findViewById(R.id.transaction_item_name);

            view.setTag(holder);
        } else {
            holder = (SaleTransactionAdapter.ViewHolder) view.getTag();
        }

        holder.qty.setText(String.valueOf(item.getQty()));
        holder.name.setText(item.getProduct().getName());

        return view;
    }

    public static class ViewHolder {
        TextView qty, name;
    }
}
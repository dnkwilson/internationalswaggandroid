package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.MainActivity;
import com.damagecontrolhq.internationalswagg.PhotoGalleryActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.VideoActivity;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.fragments.EventMediaFragment;
import com.damagecontrolhq.internationalswagg.models.Event;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * InternationalSwagg
 * Created by dwilson on 8/28/15.
 */
public class EventListAdapter extends RecyclerView.Adapter<SwaggViewHolder>{
    public static final String TAG = EventListAdapter.class.getSimpleName();
    public static final String EVENT_ID = "event_id";
    public static final String EVENT_NAME = "event_name";
    public static final String VIDEO_URL = "video_url";

    public static final String EVENT_MEDIA= "media";
    public static final String EVENT_MEDIA_URL= "url";
    public static final String EVENT_GALLERIES = "galleries";
    public static final String EVENT_VIDEOS = "videos";
    public static final String GALLERY_ID = "gallery_id";
    private static final int TYPE_EVENT = 0;
    private static final int TYPE_CLOCK = 1;

    private EventCountdownHolder.ViewTimer timer;
    private final ArrayList<Event> eventList;
    private Context context;

    public EventListAdapter(Context context, ArrayList<Event> eventList){
        this.eventList = eventList;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        Event event = eventList.get(position);
        if (event.getDate().getTime() > System.currentTimeMillis()){
            return TYPE_CLOCK;
        }else{
            return TYPE_EVENT;
        }
    }

    @Override
    public SwaggViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case TYPE_CLOCK:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_event_upcoming, parent, false);
                EventCountdownHolder viewHolder = new EventCountdownHolder(view);
                timer = viewHolder.getTimer();
                view.setTag(viewHolder);
                return viewHolder;
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_row, parent, false);
                EventViewHolder eHolder = new EventViewHolder(view);
                view.setTag(eHolder);
                return eHolder;
        }
    }

    @Override
    public void onBindViewHolder(final SwaggViewHolder holder, int position) {
        final Event e = eventList.get(position);
        switch (holder.getItemViewType()) {
            case TYPE_EVENT:
                final EventViewHolder eventViewHolder = (EventViewHolder) holder;
                Picasso.with(context).load(ImageUtils.getImageUrl(e.getPhoto())).into(eventViewHolder.eventFlyer);
                eventViewHolder.eventPhotos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (timer != null) {
                            timer.cancel();
                        }

                        if (e.getVideos()==null || e.getGalleries().size()==0){
                            Toast.makeText(v.getContext(), "No gallery to display", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        // Open event galleries
                        Intent i;
                        if (e.getGalleries().size() > 1){
                            i = new Intent(v.getContext(), MainActivity.class);
                            i.putExtra(SwaggApp.REQUESTED_PAGE, EventMediaFragment.class.getName());
                            i.putParcelableArrayListExtra(EVENT_MEDIA, e.getGalleries());
                        }else{
                            i = new Intent(v.getContext(), PhotoGalleryActivity.class);
                            i.putExtra(GALLERY_ID, e.getGalleries().get(0).getId());
                        }
                        i.putExtra(EVENT_MEDIA_URL, EVENT_GALLERIES);
                        i.putExtra(EventListAdapter.EVENT_ID, String.valueOf(e.getId()));
                        i.putExtra(EventListAdapter.EVENT_NAME, e.getName());
                        v.getContext().startActivity(i);

                    }
                });

                eventViewHolder.eventVideos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (timer != null) {
                            timer.cancel();
                        }

                        if (e.getVideos()==null|| e.getVideos().size()==0){
                            Toast.makeText(v.getContext(), "No video to display", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        // Go to event videos
                        Intent i;
                        if (e.getVideos().size() > 1){
                            i = new Intent(v.getContext(), MainActivity.class);
                            i.putExtra(SwaggApp.REQUESTED_PAGE, EventMediaFragment.class.getName());
                            i.putParcelableArrayListExtra(EVENT_MEDIA, e.getVideos());
                            i.putExtra(EVENT_MEDIA_URL, EVENT_VIDEOS);
                            i.putExtra(EventListAdapter.EVENT_ID, String.valueOf(e.getId()));
                            i.putExtra(EventListAdapter.EVENT_NAME, e.getName());
                        }else{
                            i = new Intent(v.getContext(), VideoActivity.class);
                            i.putExtra(EventListAdapter.VIDEO_URL, e.getVideos().get(0).getInfo());
                            v.getContext().startActivity(i);
                        }
                        v.getContext().startActivity(i);
                    }
                });
                break;

            default:
                EventCountdownHolder cHolder = (EventCountdownHolder) holder;
                Picasso.with(context).load(ImageUtils.getImageUrl(e.getPhoto())).into(cHolder.imageView);
                break;
        }
    }

    @Override
    public void onViewDetachedFromWindow(SwaggViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (holder.getItemViewType() == TYPE_CLOCK){
            EventCountdownHolder cdHolder = (EventCountdownHolder)holder;
            if (cdHolder.timerIsRunning){
                timer.cancel();
                cdHolder.timerIsRunning = false;
            }
        }
    }

    @Override
    public void onViewAttachedToWindow(SwaggViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (holder.getItemViewType() == TYPE_CLOCK){
            EventCountdownHolder cdHolder = (EventCountdownHolder)holder;
            if (!cdHolder.timerIsRunning){
                timer.start();
                cdHolder.timerIsRunning = true;
            }
        }
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

}
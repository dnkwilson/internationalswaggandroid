package com.damagecontrolhq.internationalswagg.adapters;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.damagecontrolhq.internationalswagg.R;

/**
 * Created by dwilson on 1/30/17.
 */
public class ReservationParentViewHolder extends ParentViewHolder {
    TextView categoryItemCount, categoryTitle;

    public ReservationParentViewHolder(@NonNull View v) {
        super(v);

        categoryItemCount = (TextView) v.findViewById(R.id.category_item_count);
        categoryTitle = (TextView) v.findViewById(R.id.category_title);
    }
}

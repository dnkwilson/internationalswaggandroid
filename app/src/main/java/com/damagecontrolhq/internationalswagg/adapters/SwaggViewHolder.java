package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;

import com.damagecontrolhq.internationalswagg.ProfileActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.models.Chat;
import com.damagecontrolhq.internationalswagg.models.User;

/**
 * InternationalSwagg
 * Created by dwilson on 9/1/15.
 */
public class SwaggViewHolder extends RecyclerView.ViewHolder {
    private Context context;
    private View view;
    private User user;

    SwaggViewHolder(View v){
        super(v);
        this.context = v.getContext();
        this.view = v;
        this.user = SwaggApp.getInstance().getSession().getUser();
    }

    void buildBackground(Chat chat) {
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        if (chat.getUserId() == user.getID()){
            params.leftMargin = 100;
            changeBackground(R.drawable.bg_msg_you);
        }else{
            params.rightMargin = 100;
            changeBackground(R.drawable.bg_msg_from);
        }
        params.bottomMargin = 16;
        view.setLayoutParams(params);
    }

    private void changeBackground(int drawable){
        view.setBackground(ContextCompat.getDrawable(context, drawable));
    }

    void getUserProfile(int userId){
        Intent i = new Intent(context, ProfileActivity.class);
        i.putExtra(User.KEY_USER_ID, userId);
        context.startActivity(i);
    }
}

package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Song;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * InternationalSwagg
 * Created by dwilson on 9/22/15.
 */
public class SongAdapter extends BaseAdapter {
    private static final String TAG = "SongAdapter";

    private ArrayList<Song> songs;
    private Context context;

    public SongAdapter(Context context, ArrayList<Song> songs){
        this.songs = songs;
        this.context = context;
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.song, parent, false);

            holder = new ViewHolder();

            holder.artworkView = (ImageView)convertView.findViewById(R.id.song_artwork);
            holder.songView = (TextView)convertView.findViewById(R.id.song_title);
            holder.artistView = (TextView)convertView.findViewById(R.id.song_artist);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Song song = songs.get(position);
        holder.songView.setText(song.getTitle());
        holder.artistView.setText(song.getArtist());
        Picasso.with(context).load(ImageUtils.getImageUrl(song.getArtwork()))
                .into(holder.artworkView);

        return convertView;
    }

    public static class ViewHolder {
        ImageView artworkView;
        TextView songView, artistView;
    }
}

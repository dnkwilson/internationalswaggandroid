package com.damagecontrolhq.internationalswagg.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damagecontrolhq.internationalswagg.BoxTvActivity;
import com.damagecontrolhq.internationalswagg.LoginActivity;
import com.damagecontrolhq.internationalswagg.MainActivity;
import com.damagecontrolhq.internationalswagg.MusicPlayerActivity;
import com.damagecontrolhq.internationalswagg.ProfileActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.fragments.DashboardFragment;
import com.damagecontrolhq.internationalswagg.fragments.LoginFragment;
import com.damagecontrolhq.internationalswagg.fragments.ProfileFragment;
import com.damagecontrolhq.internationalswagg.models.DashboardItem;
import com.damagecontrolhq.internationalswagg.views.DashIconView;

import java.util.List;

/**
 * InternationalSwagg
 * Created by dwilson on 11/28/15.
 */
public class DashboardAdapter extends RecyclerView.Adapter<SwaggViewHolder> {
    private static final String TAG = DashboardAdapter.class.getName();
    private static final int TYPE_PREVIEW = 0;
    private static final int TYPE_MENU    = 1;

    private final Context mContext;
    private final List<DashboardItem> mItems;
    private CountdownViewHolder.ViewTimer timer;

    public DashboardAdapter(Context context, List<DashboardItem> items){
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public int getItemViewType(int position) {
        DashboardItem item = mItems.get(position);
        if (item.getType().equals(DashboardItem.TYPE_MENU)){
            return TYPE_MENU;
        }else{
            return TYPE_PREVIEW;
        }
    }

    @Override
    public SwaggViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case TYPE_PREVIEW:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swagg_countdown, parent, false);
                CountdownViewHolder viewHolder = new CountdownViewHolder(view);
                ViewGroup.LayoutParams layoutParams = viewHolder.itemView.getLayoutParams();
                if (layoutParams instanceof StaggeredGridLayoutManager.LayoutParams) {
                    StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) layoutParams;
                    params.setFullSpan(true) ;
                    viewHolder.itemView.setLayoutParams(params);
                }
                timer = viewHolder.getTimer();
                view.setTag(viewHolder);
                return viewHolder;
            default:
                view = new DashIconView(mContext);
                DashItemHolder holder = new DashItemHolder(view);
                view.setTag(holder);
                return holder;
        }
    }
    @Override
    public void onBindViewHolder(SwaggViewHolder holder, int position) {
        final DashboardItem item  = mItems.get(position);
        if (holder.getItemViewType() == TYPE_MENU){
            DashItemHolder dashItemHolder = (DashItemHolder) holder;
            dashItemHolder.fillView(item);
            dashItemHolder.icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(timer!=null) timer.cancel();
                    Intent i;
                    if (item.getLink().equals(LoginFragment.class.getName())) {
                        i = new Intent(v.getContext(), LoginActivity.class);
                        i.putExtra(SwaggApp.REQUESTED_PAGE, DashboardFragment.class.getName());
                    }else if(item.getLink().equals(ProfileFragment.class.getName())) {
                        i = new Intent(v.getContext(), ProfileActivity.class);
                    }else if(item.getLink().equals(MusicPlayerActivity.class.getName())){
                        i = new Intent(v.getContext(), MusicPlayerActivity.class);
                    }else if(item.getLink().equals(BoxTvActivity.class.getName())){
                        i = new Intent(v.getContext(), BoxTvActivity.class);
                    }else{
                        i = new Intent(v.getContext(), MainActivity.class);
                        i.putExtra(SwaggApp.REQUESTED_PAGE, item.getLink());
                    }
                    v.getContext().startActivity(i);

                }
            });
        }
    }

    @Override
    public void onViewDetachedFromWindow(SwaggViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (holder.getItemViewType() == TYPE_PREVIEW){
            CountdownViewHolder cdHolder = (CountdownViewHolder)holder;
            if (cdHolder.timerIsRunning){
                Log.d(TAG, "View detached. Time destroyed!");
                timer.cancel();
                cdHolder.timerIsRunning = false;
            }
        }
    }

    @Override
    public void onViewAttachedToWindow(SwaggViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        if (holder.getItemViewType() == TYPE_PREVIEW){
            CountdownViewHolder cdHolder = (CountdownViewHolder)holder;
            if (cdHolder.getTimer() == null || !cdHolder.timerIsRunning){
                Log.d(TAG, "View attached. Time started!");
                timer.start();
                cdHolder.timerIsRunning = true;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}

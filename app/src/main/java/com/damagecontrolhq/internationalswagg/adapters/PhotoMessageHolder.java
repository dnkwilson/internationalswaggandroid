package com.damagecontrolhq.internationalswagg.adapters;

import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Chat;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

/**
 * InternationalSwagg
 * Created by dwilson on 11/26/15.
 */
class PhotoMessageHolder extends SwaggViewHolder {
    private final TextView time;
    private final TextView username;
    private final TextView body;
    final ImageView photo;

    PhotoMessageHolder(View v) {
        super(v);
        photo = (ImageView)v.findViewById(R.id.photo_image);
        username = (TextView)v.findViewById(R.id.username);
        time = (TextView)v.findViewById(R.id.time);
        body = (TextView)v.findViewById(R.id.message);
    }

    void fillView(final Chat chat){
        Picasso.with(time.getContext()).load(ImageUtils.getImageUrl(chat.getMedia())).into(photo);
        username.setText(chat.getUsername());
        time.setText(chat.getFormattedTime());
        if (chat.getBody().length() > 0){
            body.setText(chat.getBody());
            body.setVisibility(View.VISIBLE);
            Linkify.addLinks(body, Linkify.ALL);
        }
        username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserProfile(chat.getUserId());
            }
        });

        buildBackground(chat);
    }
}

package com.damagecontrolhq.internationalswagg;

import android.support.v4.app.Fragment;

import com.damagecontrolhq.internationalswagg.fragments.ProfileFragment;

public class ProfileActivity extends BaseActivity {
    private static final String TAG = "ProfileActivity";
    @Override
    protected Fragment createFragment() {
        return new ProfileFragment();
    }
}

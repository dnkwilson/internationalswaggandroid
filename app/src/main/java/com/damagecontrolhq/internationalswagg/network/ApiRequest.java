package com.damagecontrolhq.internationalswagg.network;


import android.content.Context;

import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.interfaces.OnVolleyResponseListener;
import com.damagecontrolhq.internationalswagg.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.TimeZone;

/**
 * InternationalSwagg
 * Created by dwilson on 10/20/15.
 */
public class ApiRequest{
    private static final String TAG = "ApiRequest";

    private OnVolleyResponseListener listener = null;

    private final int type;
    private final String url;
    private JSONObject params = new JSONObject();
    private final Context context;
    private final SessionManager session;


    public ApiRequest(int type, String url, JSONObject params, Context context){
        this.type = type;
        this.url = url;
        this.params = params;
        this.context = context;
        this.session = SwaggApp.getInstance().getSession();
        callApi();
    }

    private void callApi() {
//        // We first check for cached request
//        Cache cache = SwaggApp.getInstance().getRequestQueue().getCache();
//        Cache.Entry entry = cache.get(url);
//        if (entry != null) {
//            // fetch the data from cache
//            try {
//                String data = new String(entry.data, "UTF-8");
//                try {
//                    if (listener != null)
//                        listener.onComplete(new JSONObject(data));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//
//        } else {
//            // Volley's json array request object
//            setParams();
//            JsonObjectRequest jsonReq = new JsonObjectRequest(type, url, params,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            if (listener != null)
//                                listener.onComplete(response);
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    NetworkResponse response = error.networkResponse;
//                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                        Toast.makeText(context, context.getString(R.string.error_network_timeout),
//                                Toast.LENGTH_LONG).show();
//                    }else{
//                        if(error.networkResponse != null && error.networkResponse.data != null){
//                            switch (response.statusCode){
//                                case 401:
//                                    String errorString = new String(response.data);
//                                    session.clearSession();
//                                    Toast.makeText(context, errorString, Toast.LENGTH_LONG);
//                                    break;
//                            }
//                        }
//                    }
//
//                    if (url.equals(DamageControlAPI.LOGIN)) {
//                        Log.e(TAG, "Error logging in...clearing session");
//                        SwaggApp.getInstance().getSession().clearSession();
//                    }
//                     error.printStackTrace();
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> headers = new HashMap<>();
//                    headers.put("Content-Type", "application/json");
//                    headers.put("Accept", "application/json");
//                    headers.put("Authorization", getAuthString());
//                    return headers;
//                }
//            };
//
//            int socketTimeout = 5000;//30 seconds - change to what you want
//            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//            jsonReq.setRetryPolicy(policy);
//            SwaggApp.getInstance().addToRequestQueue(jsonReq);
//        }
    }

    private void setParams(){
        try {
            params.put("timezone", TimeZone.getDefault().getID());
            params.put(User.KEY_PROVIDER, session.getLoginProvider());
            params.put(User.KEY_UID, session.getLoginUid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getAuthString() {
        String token = (session.isLoggedIn()) ? session.getUserToken() : session.getLoginToken();
        return "Token token=\"" + token + "\"";
    }

    public void setOnVolleyResponseListener(OnVolleyResponseListener listener){
        this.listener = listener;
    }


}

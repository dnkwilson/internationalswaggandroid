package com.damagecontrolhq.internationalswagg.interfaces;

import com.damagecontrolhq.internationalswagg.adapters.ReservationAdapter;

/**
 * Created by dwilson on 1/30/17.
 */

public interface RecyclerItemClickListener {
    void onItemClicked(ReservationAdapter.PackageViewHolder holder, int position);
}

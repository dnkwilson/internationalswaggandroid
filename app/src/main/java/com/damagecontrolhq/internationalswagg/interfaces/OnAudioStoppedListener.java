package com.damagecontrolhq.internationalswagg.interfaces;

/**
 * InternationalSwagg
 * Created by dwilson on 11/12/15.
 */
public interface OnAudioStoppedListener {
    void onAudioStoppedListener(boolean isPaused);
}

package com.damagecontrolhq.internationalswagg.interfaces;

import com.damagecontrolhq.internationalswagg.models.ReservationItem;

/**
 * Created by dwilson on 3/6/17.
 */

public interface OnReservationProductListener {
    void onProductChanged(ReservationItem oldItem, ReservationItem newItem);
}

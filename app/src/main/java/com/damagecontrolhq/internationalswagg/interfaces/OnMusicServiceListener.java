package com.damagecontrolhq.internationalswagg.interfaces;

/**
 * InternationalSwagg
 * Created by dwilson on 2/6/16.
 */
public interface OnMusicServiceListener {
    void onReady();
    void onSongCompleted();
    void updateUI(int index);
}

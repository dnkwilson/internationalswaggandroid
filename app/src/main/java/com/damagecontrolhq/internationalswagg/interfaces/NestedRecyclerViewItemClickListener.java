package com.damagecontrolhq.internationalswagg.interfaces;

import android.support.v7.widget.RecyclerView;

import com.damagecontrolhq.internationalswagg.models.ReservationItem;

/**
 * Created by dwilson on 2/6/17.
 */

public interface NestedRecyclerViewItemClickListener {
    void onItemClicked(RecyclerView.ViewHolder holder, int parentPosition, int childPosition, ReservationItem item);
}

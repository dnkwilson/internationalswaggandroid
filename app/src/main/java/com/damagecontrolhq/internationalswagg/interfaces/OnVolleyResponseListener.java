package com.damagecontrolhq.internationalswagg.interfaces;

import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 10/20/15.
 */
public interface OnVolleyResponseListener {
    void onComplete(JSONObject json);
    void onError(Throwable error);
}

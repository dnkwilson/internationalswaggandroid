package com.damagecontrolhq.internationalswagg.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.ReservationProductAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.GridItemDecoration;
import com.damagecontrolhq.internationalswagg.helpers.RecyclerItemClickListener;
import com.damagecontrolhq.internationalswagg.interfaces.OnReservationProductListener;
import com.damagecontrolhq.internationalswagg.models.Product;
import com.damagecontrolhq.internationalswagg.models.ProductList;
import com.damagecontrolhq.internationalswagg.models.ReservationItem;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.damagecontrolhq.internationalswagg.ReservationItemActivity.ARG_UPDATE_PRODUCT_REQUEST_CODE;

/**
 * Created by dwilson on 2/3/17.
 */

public class ReservationProductFragment extends DialogFragment implements
        RecyclerItemClickListener.OnItemClickListener {
    private static final String TAG = ReservationProductFragment.class.getName();
    public static final String PRODUCT_SEARCH = "product_search";
    public static final String ARG_PRODUCT = "product",
                                ARG_RESERVATION_ITEM = "reservation_item";

    private RecyclerView mRecyclerView;
    private Button mOk, mCancel;

    private ArrayList<ReservationItem> mReservationItems = new ArrayList<>();
    private ReservationProductAdapter mAdapter;
    private Product mProduct;
    private ReservationItem mReservationItem;
    private int selectedItemTotal = 0;

    private OnReservationProductListener listener;

    public static ReservationProductFragment newInstance(ReservationItem reservationItem) {
        Bundle args = new Bundle();
        args.putParcelable(PRODUCT_SEARCH, reservationItem);
        ReservationProductFragment fragment = new ReservationProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reservation_product, container, false);

        mRecyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
        mOk = (Button) v.findViewById(R.id.ok_btn);
        mCancel = (Button) v.findViewById(R.id.cancel_btn);


        Bundle args = getArguments();
        mReservationItem = args.getParcelable(PRODUCT_SEARCH);
        mProduct = mReservationItem.getProduct();

        int price = mProduct.getJmPrice();
        selectedItemTotal = price * mReservationItem.getQty();

        int columns = getResources().getInteger(R.integer.product_list_columns);

        loadProducts();

        mRecyclerView.addItemDecoration(new GridItemDecoration(
                getResources().getDimensionPixelSize(R.dimen.view_margin),
                columns));
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), this));
        GridLayoutManager mLayoutManager = new GridLayoutManager(this.getActivity(), columns);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra(ARG_PRODUCT, mReservationItems);
                intent.putExtra(ARG_RESERVATION_ITEM, mReservationItem);

                getTargetFragment().onActivityResult(
                        getTargetRequestCode(), ARG_UPDATE_PRODUCT_REQUEST_CODE, intent);
                getDialog().dismiss();
            }
        });

        return v;
    }

    private void loadProducts() {
        int categoryId = 0;
        if (!(mProduct.getCategoryName().equals("Beer") || mProduct.getCategoryName().equals("Chaser"))){
            categoryId = mProduct.getCategoryId();
        }

        SwaggApp.api(true).getProducts(categoryId, selectedItemTotal).enqueue(new Callback<ProductList>() {
            @Override
            public void onResponse(Call<ProductList> call, Response<ProductList> response) {
                ArrayList<Product> products = new ArrayList<>();
                if (response.isSuccessful()){
                    products = response.body().products;
                }

                for (int i = 0; i < products.size(); i++) {
                    ReservationItem item = new ReservationItem(products.get(i));
                    mReservationItems.add(item);
                }

                mAdapter = new ReservationProductAdapter(getContext(), mReservationItems);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<ProductList> call, Throwable t) {
                Log.d(TAG, "GET ProductList error: " + t.getMessage());
            }
        });
    }

    private void updateParentFragment(int position){
        Intent intent = new Intent();
        intent.putExtra(ARG_PRODUCT, mReservationItems.get(position));
        intent.putExtra(ARG_RESERVATION_ITEM, mReservationItem);

        onActivityResult(getTargetRequestCode(), ARG_UPDATE_PRODUCT_REQUEST_CODE, intent);
        getDialog().dismiss();
    }

    @Override
    public void onItemClick(View childView, int position) {
        ReservationItem item = mReservationItems.get(position);

        if (mReservationItem.getProduct().getJmPrice() == item.getProduct().getJmPrice()){
            item.setQty(mReservationItem.getQty());
        }

        listener.onProductChanged(mReservationItem, item);
        dismiss();
    }

    @Override
    public void onItemLongPress(View childView, int position) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.listener = (OnReservationProductListener)context;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(listener.toString() + " must implement OnCompleteListener");
        }
    }
}

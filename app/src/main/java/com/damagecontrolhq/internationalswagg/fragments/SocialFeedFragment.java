package com.damagecontrolhq.internationalswagg.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.SocialFeedAdapter;
import com.damagecontrolhq.internationalswagg.app.DamageControlAPI;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.InfiniteScrollListener;
import com.damagecontrolhq.internationalswagg.models.InstagramPost;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SocialFeedFragment extends Fragment {
    private static final String TAG = "SocialFeedFragment";

    private static final String PARAM_MAX_ID = "max_tag_id";
    private static final String PARAM_MIN_ID = "min_tag_id";
    private static final String PARAM_NEXT_MAX_ID = "next_max_tag_id";
    private static final String PARAM_PAGINATION = "pagination";

    private List<InstagramPost> mPosts = new ArrayList<>();
    private SocialFeedAdapter mAdapter;
    private String requestString;
    private GridView mGridView;
    private int columnWidth;
    private String maxTagId;


    private void getPosts(){
        requestString = String.format("%s?%s=%s",  DamageControlAPI.FEED, PARAM_MAX_ID, maxTagId);

        // We first check for cached request
//        Cache cache = SwaggApp.getInstance().getRequestQueue().getCache();
//        Cache.Entry entry = cache.get(requestString);
//        if (entry != null) {
//            // fetch the data from cache
//            try {
//                String data = new String(entry.data, "UTF-8");
//                try {
//                    Log.d(TAG, "Loading from cache...");
//                    Log.d(TAG, "Cached data: " + data);
//                    parseJSON(new JSONObject(data));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//
//        } else {
//            // Volley's json array request object
//            SwaggObjectRequest jsonReq = new SwaggObjectRequest(requestString,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            VolleyLog.d(TAG, "Response: " + response.toString());
//                            parseJSON(response);
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    VolleyLog.d(TAG, "Error: " + error.getMessage());
//                }
//            }, getActivity());
//
//            SwaggApp.getInstance().addToRequestQueue(jsonReq);
//        }
    }

    private void parseJSON(JSONObject response) {
        try {
            JSONArray feedArray = response.getJSONArray("data");
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject json = (JSONObject) feedArray.get(i);

                InstagramPost post = new InstagramPost(json);

                mPosts.add(post);
            }

            JSONObject pagination = response.getJSONObject(PARAM_PAGINATION);
            maxTagId = pagination.getString(PARAM_NEXT_MAX_ID);

            // notify data changes to list adapter
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initializeGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                SwaggApp.GRID_PADDING, r.getDisplayMetrics());

        columnWidth = (int) ((ImageUtils.getScreenWidth(getActivity()) -
                ((SwaggApp.NUM_OF_COLUMNS + 1) * padding)) / SwaggApp.NUM_OF_COLUMNS);

        mGridView.setNumColumns(SwaggApp.NUM_OF_COLUMNS);
        mGridView.setColumnWidth(columnWidth);
        mGridView.setStretchMode(GridView.NO_STRETCH);
        mGridView.setPadding((int) padding, (int) padding, (int) padding,
                (int) padding);
        mGridView.setHorizontalSpacing((int) padding);
        mGridView.setVerticalSpacing((int) padding);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_social_feed, container, false);

        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setTitle("Instagram Feed");
        mActionBar.setSubtitle(null);

        mGridView = (GridView)v.findViewById(R.id.social_feed);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InstagramPost post = mPosts.get(position);

                Uri url = Uri.parse(post.getLink());
                Intent insta = new Intent(Intent.ACTION_VIEW, url);
                insta.setPackage("com.instagram.android");

                if (AppUtils.isIntentAvailable(getActivity(), insta)) {
                    startActivity(insta);
                } else {
                    startActivity(new Intent(Intent.ACTION_VIEW, url));
                }
            }
        });

        initializeGridLayout();
        mAdapter = new SocialFeedAdapter(getActivity(), mPosts, columnWidth);
        mGridView.setAdapter(mAdapter);

        getPosts();

        mGridView.setOnScrollListener(new InfiniteScrollListener() {
            @Override
            public void onLoadMore() {
                getPosts();
            }
        });

        return v;
    }
}

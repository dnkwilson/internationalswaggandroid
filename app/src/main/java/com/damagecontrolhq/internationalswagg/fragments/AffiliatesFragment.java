package com.damagecontrolhq.internationalswagg.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.AffiliatesAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.models.Affiliate;
import com.damagecontrolhq.internationalswagg.models.AffiliateList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * InternationalSwagg
 * Created by dwilson on 1/11/16.
 */
public class AffiliatesFragment extends Fragment{
    public static final String TAG = AffiliatesFragment.class.getName();

    private AffiliatesAdapter mAdapter;
    private ArrayList<Affiliate> mAffiliates = new ArrayList<>();
    private RecyclerView mRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_recycler_list, container, false);

        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setTitle("Affiliates");
        mActionBar.setSubtitle(null);

        // Setup our view and list adapter. Ensure it scrolls to the bottom as data changes
        mRecyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new AffiliatesAdapter(mAffiliates);
        mRecyclerView.setAdapter(mAdapter);

        initList();

        return v;
    }

    private void initList() {
        SwaggApp.api(false).getAffiliates().enqueue(new Callback<AffiliateList>() {
            @Override
            public void onResponse(Call<AffiliateList> call, Response<AffiliateList> response) {
                mAffiliates = response.body().getAffiliates();
                mAdapter = new AffiliatesAdapter(mAffiliates);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<AffiliateList> call, Throwable t) {
                Log.e(TAG, "Error getting affiliates: "+ t.getMessage());
            }
        });
    }
}

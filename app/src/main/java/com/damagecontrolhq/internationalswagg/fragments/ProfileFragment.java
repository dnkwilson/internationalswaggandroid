package com.damagecontrolhq.internationalswagg.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.models.RoundImage;
import com.damagecontrolhq.internationalswagg.models.User;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * InternationalSwagg
 * Created by dwilson on 10/23/15.
 */
public class ProfileFragment extends Fragment
        implements ProfileEditDialogFragment.OnProfileUpdateListener, ProfileEditDialogFragment.OnDialogDismiss{
    private static final String TAG = "ProfileFragment";

    private static final int CHOOSE_IMAGE_REQUEST = 101;

    private ActionBar mActionBar;
    private Toolbar mToolbar;

    private ImageView avatar, editAvatar;
    private TextView username;
    private TextView email;
    private TextView city;
    private Button updateButton;
    private User user;
    private FrameLayout editHolder;
    private SessionManager session;
    private boolean isSelf = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        getActivity().invalidateOptionsMenu();
    }

    private void getUser(int userId, final View v) {
        SwaggApp.api(false).getUser(userId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String string = response.body().getAsJsonObject("public_user").toString();
                try {
                    JSONObject json = new JSONObject(string);
                    user = new User(json);
                    initView(v);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "Error getting user: " + t.getMessage());
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        session = SwaggApp.getInstance().getSession();

        Intent i = getActivity().getIntent();
        if (i.hasExtra(User.KEY_USER_ID)){
            int userId = i.getIntExtra(User.KEY_USER_ID, 0);
            if (session.getUser().getID() != userId) isSelf = false;
            if (isSelf){
                user = session.getUser();
                initView(v); } else { getUser(userId, v);}
        }else{
            user = session.getUser();
            initView(v);
        }

        return v;
    }

    private void initView(View v){
        // Setup toolbars
        mToolbar = (Toolbar)v.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(true);

        mActionBar.setTitle(user.getFirstName() + "'s Profile");

        avatar   = (ImageView)v.findViewById(R.id.imgAvatar);
        username = (TextView)v.findViewById(R.id.txtUserName);
        email    = (TextView)v.findViewById(R.id.txtEmail);
        city     = (TextView)v.findViewById(R.id.txtCity);
        updateButton = (Button)v.findViewById(R.id.btnProfileEdit);
        editAvatar = (ImageView)v.findViewById(R.id.edit_avatar);
        editHolder = (FrameLayout)v.findViewById(R.id.editHolder);

        setFields();
    }

    private void setFields() {
        Picasso.with(getContext()).load(ImageUtils.getImageUrl(user.getProfilePic())).into(avatar);
        username.setText(user.getUserName());
        city.setText(user.getCity());

        BitmapDrawable bitmap = (BitmapDrawable) ContextCompat.getDrawable(getActivity(),R.drawable.ic_edit_photo);
        editAvatar.setImageDrawable(new RoundImage(bitmap.getBitmap()));

        if(user.getCity() == null || user.getCity().equals("")) city.setVisibility(View.GONE);

        if (!isSelf) {
            email.setVisibility(View.GONE);
            updateButton.setVisibility(View.GONE);
            editAvatar.setVisibility(View.GONE);
        }else{
            email.setText(user.getEmail());
            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateButton.setEnabled(false);
                    Bundle args = new Bundle();
                    args.putInt(User.KEY_ID, user.getID());
                    args.putString(User.KEY_USERNAME, user.getUserName());
                    args.putString(User.KEY_PHONE, user.getPhone());
                    args.putString(User.KEY_CITY, user.getCity());

                    FragmentManager fm = getFragmentManager();
                    ProfileEditDialogFragment dialogFragment = new ProfileEditDialogFragment();
                    dialogFragment.setArguments(args);
                    dialogFragment.setTargetFragment(ProfileFragment.this, 0);
                    dialogFragment.show(fm, String.format("Edit %s's Profile", user.getFirstName()));
                }
            });

            editHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Open photo selector");
                    getPhotoFromGallery();
                }
            });
        }

        // Set header toolbar navigation
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void completed(User u) {
        session.createSession(u);
        user = session.getUser();
        setFields();
        updateButton.setEnabled(true);
    }

    @Override
    public void dismissed() {
        updateButton.setEnabled(false);
    }

    private void getPhotoFromGallery(){
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHOOSE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            Log.d(TAG, "Result data: " + data);
            String filePath = AppUtils.getFilePath(Uri.parse(data.getDataString()), getActivity());
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = false;
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inDither = true;
                Bitmap mBitmap = AppUtils.decodeBitmapFromFile(filePath, 720, 720);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);

                // bitmap object
                byte[] byteImage_photo = baos.toByteArray();

                //generate base64 string of image
                String encodedImage = Base64.encodeToString(byteImage_photo, Base64.DEFAULT);
                JSONObject json = new JSONObject();
                json.put("avatar", encodedImage);
                JSONObject reg = new JSONObject().put("registration", json);
                updateProfile(reg);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateProfile(final JSONObject json){
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; " +
                "charset=utf-8"), json.toString());
        SwaggApp.api(false).updateUser(body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String string = response.body().getAsJsonObject("user").toString();
                Log.d(TAG, "Profile update response: " + json.toString());
                try {
                    JSONObject json = new JSONObject(string);
                    user.setProfilePic(json.getString("profile_pic"));
                    setFields();
                } catch (JSONException e) {
                    Log.e(TAG, "Issue updating profile pic: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "Error getting user: " + t.getMessage());
            }
        });
    }


}

package com.damagecontrolhq.internationalswagg.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;

import static com.damagecontrolhq.internationalswagg.ReservationItemActivity.ARG_VERIFY_NAME_REQUEST_CODE;


/**
 * A simple {@link Fragment} subclass.
 */
public class VerifyNameDialogFragment extends DialogFragment {

    private EditText mFirstName, mLastName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_verify_name_dialog, container, false);

        mFirstName = (EditText) v.findViewById(R.id.edit_first_name_text);
        mLastName = (EditText) v.findViewById(R.id.edit_last_name_text);
        Button mOkBtn = (Button) v.findViewById(R.id.verify_name_btn);


        final SessionManager sessionManager = SwaggApp.getInstance().getSession();
        mFirstName.setText(sessionManager.getUser().getFirstName());
        mLastName.setText(sessionManager.getUser().getLastName());

        mOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.getUser().setFirstName(mFirstName.getText().toString());
                sessionManager.getUser().setLastName(mLastName.getText().toString());
                getTargetFragment().onActivityResult(
                        getTargetRequestCode(), ARG_VERIFY_NAME_REQUEST_CODE, new Intent());
                getDialog().dismiss();
            }
        });

        return v;
    }

}

package com.damagecontrolhq.internationalswagg.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.User;
import com.damagecontrolhq.internationalswagg.utils.ValidationUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 10/23/15.
 */
public class ProfileEditDialogFragment extends DialogFragment {
    private static final String TAG = "ProfileEditDialogFragment";

    private EditText userName;
    private EditText city;
    private EditText phone;

    private OnProfileUpdateListener mCallback = null;
    private OnDialogDismiss mDismissedCallback = null;

    public interface OnProfileUpdateListener {
        void completed(User user);
    }

    public interface OnDialogDismiss{
        void dismissed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            mCallback = (OnProfileUpdateListener)getTargetFragment();
            mDismissedCallback = (OnDialogDismiss)getTargetFragment();
        } catch(ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_dialog, container);

        String mUsername = getArguments().getString(User.KEY_USERNAME);
        String mCity = getArguments().getString(User.KEY_CITY);
        String mPhone = getArguments().getString(User.KEY_PHONE);

        userName = (EditText)v.findViewById(R.id.edit_username_text);
        city     = (EditText)v.findViewById(R.id.edit_city_text);
        phone    = (EditText)v.findViewById(R.id.edit_phone_text);
        userName.setText(mUsername);

        if (mCity == null || mCity.equals("null")) {
            city.clearComposingText();
        }else {
            city.setText(mCity);
        }

        if (mPhone == null || mPhone.equals("null")){
            phone.setText(mPhone);
        }else{
            phone.setText(mPhone);
        }

        getDialog().setTitle("Edit Profile");

        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                ValidationUtils.validLength(userName, 8);
            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ValidationUtils.isPhoneNumber(phone);
            }
        });

        city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ValidationUtils.validLength(city,3);
            }
        });

        Button update = (Button)v.findViewById(R.id.profile_update_btn);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()){
                    callApi();
                    dismiss();
                    mDismissedCallback.dismissed();
                }else {
                    Toast.makeText(getActivity(), "Form contains error(s)",
                            Toast.LENGTH_LONG).show();

                }
            }
        });

        return v;
    }


    private boolean checkValidation() {
        boolean ret = true;

        if (!ValidationUtils.validLength(userName, 8)) ret = false;
        if (!ValidationUtils.validLength(city, 3)) ret = false;
        if (!ValidationUtils.isPhoneNumber(phone)) ret = false;

        return ret;
    }

    private void callApi(){
//        ApiRequest apiRequest = new ApiRequest(Request.Method.PUT, DamageControlAPI.USERS,
//                setParams(), getActivity());
//        apiRequest.setOnVolleyResponseListener(new OnVolleyResponseListener() {
//            @Override
//            public void onComplete(JSONObject json) {
//                parseJSON(json);
//            }
//
//            @Override
//            public void onError(Throwable error) {
//
//            }
//        });
    }

    private JSONObject setParams() {
        JSONObject params = new JSONObject();
        try {
            params.put(User.KEY_USERNAME, userName.getText());
            params.put(User.KEY_CITY, city.getText());
            params.put(User.KEY_PHONE, phone.getText());
        }catch (JSONException e){
            e.printStackTrace();
        }
        return params;
    }


    private void parseJSON(JSONObject json) {
        try {
            User user = new User(json.getJSONObject("user"));
            mCallback = (OnProfileUpdateListener)getTargetFragment();
            mCallback.completed(user);
            mDismissedCallback.dismissed();
        } catch (JSONException e) {  e.printStackTrace(); }
    }

    @Override
    public void onDetach() {
        mCallback = null;
        mDismissedCallback.dismissed();
        super.onDetach();
    }
}

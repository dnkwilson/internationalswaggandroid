package com.damagecontrolhq.internationalswagg.fragments;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.LoginActivity;
import com.damagecontrolhq.internationalswagg.PhotoPickerActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.ChatListAdapter;
import com.damagecontrolhq.internationalswagg.app.DamageControlAPI;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.models.Chat;
import com.damagecontrolhq.internationalswagg.models.User;
import com.damagecontrolhq.internationalswagg.services.VoiceNoteService;
import com.damagecontrolhq.internationalswagg.ui.ViewProxy;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * InternationalSwagg
 * Created by dwilson on 10/12/15.
 */
public class ChatFragment extends Fragment{
    private static final String TAG = "ChatFragment";

    private static final int MAX_RECORDING_TIME = 60000;
    private static final int CHOOSE_IMAGE_REQUEST = 100;
    private static final int PHOTO_MESSAGE_REQUEST = 101;
    public static final String ATTACHED_PHOTO = "attached_photo";
    public static final String SHARED_PHOTO = "shared_photo";

    private static final String USER_ONLINE = "online";
    private static final String USER_OFFLINE = "offline";

    private RecyclerView mRecyclerView;
    private List<Chat> mChats = new ArrayList<>();
    private ChatListAdapter mChatListAdapter;

    private View mView;
    private EditText mInputText;
    private ImageButton mActionBtn;

    private String mFilePath, mFileName;
    private MediaRecorder mMediaRecorder;
    private SessionManager mSession;

    private boolean isRecording;

    private VoiceNoteService vnService;
    private Intent playIntent;
    private boolean vnBound = false;
    private boolean deleteMessage = false;

    private ActionBar mActionBar;

    private Vibrator mVibrator;

    private TextView recordTimeText;
    private View recordPanel;
    private View slideText;
    private ImageView recIndicator;
    private float startedDraggingX = -1;
    private float distCanMove = dp(80);

    private Socket mSocket;
    {
        try{
            mSocket = IO.socket(DamageControlAPI.CHAT_URL);
        }catch (URISyntaxException ignored){}
    }

    private Menu menu;

    //connect to the service
    private ServiceConnection musicConnection = new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            VoiceNoteService.VoiceNoteBinder binder = (VoiceNoteService.VoiceNoteBinder)service;

            vnService = binder.getService();

            vnBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            vnBound = false;
        }
    };

    private final Emitter.Listener onConnected = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateStatus(USER_ONLINE);
                }
            });
        }
    };


    private final Emitter.Listener onDisconnected = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateStatus(USER_OFFLINE);
                }
            });
        }
    };


    private final Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject)args[0];
                    Chat chat = new Chat(data);
                    mChats.add(chat);
                    mRecyclerView.scrollToPosition(mChats.size() - 1);
                    mChatListAdapter.notifyItemInserted(mChats.size() - 1);
                    mChatListAdapter.notifyDataSetChanged();
                }
            });
        }
    };

    private final Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject)args[0];
                    Log.i(TAG, "User is typing: " + data);
                }
            });
        }
    };

    private final Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "User stopped typing: " + args.toString());
                }
            });
        }
    };

    private final Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "User joined: " + args.toString());
                }
            });
        }
    };

    private final Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "User left: " + args.toString());
                }
            });
        }
    };

    private final Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "Error connecting to chat server: " + args.toString());
                    mSocket.connect();
                }
            });
        }
    };

    private final Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Setup View
                    setupView();

                    // Get messages from history
                    JSONObject json = (JSONObject)args[0];
                    JSONArray array;
                    try {
                        Log.d(TAG, "Messages = " + json.getJSONArray("messages").toString());
                        array = json.getJSONArray("messages");
                        for (int i = 0; i < array.length(); i++){
                            JSONObject message = new JSONObject(array.getString(i));
                            Chat chat = new Chat(message);
                            mChats.add(chat);
                            mChatListAdapter.notifyItemInserted(mChats.size() - 1);
                            mRecyclerView.scrollToPosition(mChats.size() - 1);
                            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                            mChatListAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "Error parsing message history: " + e.toString());
                    }
                }
            });
        }
    };

    private void startVoiceNoteService(){
        if(playIntent==null){
            Log.d(TAG, "Starting Voice Note Service");
            playIntent = new Intent(getActivity(), VoiceNoteService.class);
            getActivity().bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            getActivity().startService(playIntent);
        }
    }

    private void stopVoiceNoteService(){
        Log.d(TAG, "Destroying Voice Note Service");
        if (playIntent != null) getActivity().stopService(playIntent);
        vnService = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        getActivity().invalidateOptionsMenu();

        // Share photo
        Intent i = getActivity().getIntent();
        Uri imageUri = i.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) launchPhotoMessage(imageUri.toString());

        SwaggApp.getInstance().checkLogin();
        mSession = SwaggApp.getInstance().getSession();
        if (!mSession.isLoggedIn()){
            Intent intent = new Intent(getContext(), LoginActivity.class);
            intent.putExtra(SwaggApp.REQUESTED_PAGE, ChatFragment.class.getName());
            startActivity(intent);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_chat, container, false);

        recordPanel = mView.findViewById(R.id.record_panel);
        recordTimeText = (TextView)mView.findViewById(R.id.recording_time_text);
        recIndicator = (ImageView)mView.findViewById(R.id.recordingIndicator);
        slideText = mView.findViewById(R.id.slideText);
        TextView textView = (TextView)mView.findViewById(R.id.slideToCancelTextView);
        textView.setText("SlideToCancel");

        final String mUsername = mSession.getUser().getUserName();

        mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setTitle(mUsername );

        mVibrator = (Vibrator)getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        // Setup our input methods. Enter key on the keyboard or pushing the send button
        mInputText = (EditText)mView.findViewById(R.id.inputMsg);
        mInputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    prepareMessage();
                }
                return true;
            }
        });

        final Handler typingHandler = new Handler();
        mInputText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(TAG, "Text changed!");
                if (s.length() > 0) {
                    mSocket.emit("typing");
                    mActionBtn.setEnabled(true);
                    mActionBtn.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_send_white));
                } else {
                    mActionBtn.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_mic));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                typingHandler.removeCallbacksAndMessages(null);
                typingHandler.postDelayed(userStoppedTyping, 2000);
            }

            Runnable userStoppedTyping = new Runnable() {
                @Override
                public void run() {
                    updateStatus(null);
                }
            };
        });


        mView.findViewById(R.id.chat_audio_send_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSocket == null || !mSocket.connected()) {
                    Toast.makeText(getActivity(), "Network connection error. Please retry when you are connected",
                            Toast.LENGTH_SHORT).show();
                } else {
                    prepareMessage();
                }
            }
        });

        mActionBtn = (ImageButton)mView.findViewById(R.id.chat_audio_send_button);
        mActionBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                attemptRecording();
                return true;
            }
        });

        int showVoiceButton = audioCaptureSupported() ? View.VISIBLE : View.GONE;
        mActionBtn.setVisibility(showVoiceButton);

        mActionBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_UP:
                        mActionBtn.setLayoutParams(new LinearLayout.LayoutParams(getDp(60), getDp(60)));
                        // We're only interested in anything if our speak button is currently pressed.
                        if (isRecording) {
                            startedDraggingX = -1;
                            recordPanel.setVisibility(View.INVISIBLE);
                            stopRecording();
                            isRecording = false;
                            if( deleteMessage){
                                File file = new File(mFilePath);
                                if(file.exists()){
                                    file.delete();
                                    Toast.makeText(getActivity(), "Recording deleted!", Toast.LENGTH_SHORT).show();
                                }
                                deleteMessage = false;
                            }else{
                                Chat chat = new Chat();
                                User user = SwaggApp.getInstance().getSession().getUser();
                                chat.setUserId(user.getID());
                                chat.setUsername(user.getUserName());
                                chat.setTime(chat.currentTime());
                                chat.setVoiceMessage(mFileName, fileString(), (int) getDuration());
                                chat.setLocalMedia(mFilePath);
                                uploadMedia(chat);
                            }
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        float x = event.getX();
                        if (x < -distCanMove) {
                            stopRecording();
                            deleteMessage = true;
                        }
                        x = x + ViewProxy.getX(mActionBtn);
                        FrameLayout.LayoutParams fParams = (FrameLayout.LayoutParams) slideText
                                .getLayoutParams();
                        if (startedDraggingX != -1) {
                            float dist = (x - startedDraggingX);
                            fParams.leftMargin = dp(30) + (int) dist;
                            slideText.setLayoutParams(fParams);
                            float alpha = 1.0f + dist / distCanMove;
                            if (alpha > 1) {
                                alpha = 1;
                            } else if (alpha < 0) {
                                alpha = 0;
                            }
                            ViewProxy.setAlpha(slideText, alpha);
                        }
                        if (x <= ViewProxy.getX(slideText) + slideText.getWidth()
                                + dp(30)) {
                            if (startedDraggingX == -1) {
                                startedDraggingX = x;
                                distCanMove = (recordPanel.getMeasuredWidth()
                                        - slideText.getMeasuredWidth() - dp(48)) / 2.0f;
                                if (distCanMove <= 0) {
                                    distCanMove = dp(80);
                                } else if (distCanMove > dp(80)) {
                                    distCanMove = dp(80);
                                }
                            }
                        }
                        if (fParams.leftMargin > dp(30)) {
                            fParams.leftMargin = dp(30);
                            slideText.setLayoutParams(fParams);
                            ViewProxy.setAlpha(slideText, 1);
                            startedDraggingX = -1;
                        }
                        break;

                }
                return false;
            }
        });

        return mView;
    }

    void attemptRecording(){
        Log.d(TAG, "Started recording...");
        if (mSocket == null || !mSocket.connected()){
            Toast.makeText(getActivity(), "Network connection error. Please retry when you are connected",
                    Toast.LENGTH_SHORT).show();
        }else{
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) slideText
                    .getLayoutParams();
            params.leftMargin = dp(30);
            slideText.setLayoutParams(params);

            mActionBtn.setLayoutParams(new LinearLayout.LayoutParams(getDp(70), getDp(70)));
            ViewProxy.setAlpha(slideText, 1);
            startedDraggingX = -1;
            startRecordingWrapper();
        }
    }

    private void setupView(){
        // Setup our view and list adapter. Ensure it scrolls to the bottom as data changes
        mRecyclerView = (RecyclerView)mView.findViewById(R.id.chat_list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mLayoutManager.setStackFromEnd(true);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Tell our list adapter that we only want 50 messages at a time
        mChatListAdapter = new ChatListAdapter(getActivity(), mChats, vnService);
        mRecyclerView.setAdapter(mChatListAdapter);

    }

    private String fileString(){
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try{
            InputStream in = new FileInputStream(new File(mFilePath));
            byte[] buf = new byte[2048];
            int n;
            while ((n = in.read(buf)) >= 0) {
                out.write(buf, 0, n);
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        byte[] data = out.toByteArray();
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    private void prepareMessage() {
        String input = mInputText.getText().toString();
        if (!input.equals("")) {
            // Create our 'model', a Chat object
            Chat chat = new Chat(input, mSession.getUser());

            // Create a new, auto-generated child of that chat location, and save our chat data there
            mChats.add(chat);
            mRecyclerView.scrollToPosition(mChats.size() - 1);
            mChatListAdapter.notifyItemInserted(mChats.size() - 1);
            mChatListAdapter.notifyDataSetChanged();

            // Upload media
            if (chat.getCategory().equals(Chat.VOICE_MESSAGE)) {
                uploadMedia(chat);
            }

            sendMessage(chat);
        }
    }

    private void sendMessage(Chat chat){
        mSocket.emit("new message", chat.toJSON());
        mInputText.setText("");
    }

    private void uploadMedia(final Chat chat){
        Log.d(TAG, "Uploading media...");
        Log.d(TAG, "User: " + SwaggApp.getInstance().getSession().getUser().getID() +
        " | Chat user_id: " + chat.getUserId());

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; " +
                "charset=utf-8"), chat.toJSON().toString());
        SwaggApp.chatApi().chatUpload(body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "Upload completed!");

                    try {
                        String string = response.body().getAsJsonObject().toString();
                        JSONObject json = new JSONObject(string);

                        if (json.getBoolean("success")) {
                            Log.d(TAG, "Response: " + json);
                            chat.setMedia(json.getString("media"));
                            chat.setIsUploaded(true);

                            mChats.add(chat);
                            if(mRecyclerView == null) setupView();

                            mRecyclerView.scrollToPosition(mChats.size() - 1);
                            mChatListAdapter.notifyItemInserted(mChats.size() - 1);
                            mChatListAdapter.notifyDataSetChanged();

                            sendMessage(chat);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.e(TAG, "Error uploading media: " + response.errorBody());
                    chat.setMedia(null);
                    chat.setIsUploaded(false);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "Error uploading media: " + t.getMessage());
                chat.setMedia(null);
                chat.setIsUploaded(false);
            }
        });
    }

    private boolean audioCaptureSupported(){
        return getActivity().getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_MICROPHONE);
    }

    private void setFile(){
        // File path of recorded audio
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        Date now = new Date();
        File dir = new File(SwaggApp.getInstance().getVoiceNoteDir());

        if (!dir.exists()){
            dir.mkdirs();
        }

        mFileName= String.format("AUDIO_%s.3gp", formatter.format(now));
        File file = new File(dir, mFileName);
        mFilePath = file.getAbsolutePath();

        Log.d(TAG, "File created for recording...");
    }

    private void startRecording() throws IOException {
        setFile();

        // Set the audio format and encoder
        mMediaRecorder = new MediaRecorder();

        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        // Setup the output location
        mMediaRecorder.setOutputFile(mFilePath);

        // Start the recording
        mMediaRecorder.prepare();
        mMediaRecorder.start();

        // Recording indicator
        timer.start();
        vibrate();
        recordPanel.setVisibility(View.VISIBLE);
        Animation blink = AnimationUtils.loadAnimation(getActivity(), R.anim.blink);
        recIndicator.startAnimation(blink);

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Log.d(TAG, "Started recording...");
    }

    private void stopRecording(){
        if (timer != null) timer.cancel();
        if (recordTimeText.getText().toString().equals("00:00")) return;
        try{
            if (mMediaRecorder!= null){
                mMediaRecorder.stop();
                mMediaRecorder.reset();
                mMediaRecorder.release();
                mMediaRecorder = null;
            }
        }catch(RuntimeException e){
            Log.e(TAG, "Error stopping recording: " + e.getMessage());
        }

        recordTimeText.setText("00:00");
        vibrate();
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Log.d(TAG, "Stopped recording...");
    }

    private void vibrate(){
        if (mVibrator.hasVibrator()) mVibrator.vibrate(200);
    }

    private long getDuration() {
        MediaPlayer mp = new MediaPlayer();
        try {
            mp.setDataSource(mFilePath);
            mp.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long duration = mp.getDuration();
        mp.reset();
        mp.release();
        return duration;
    }

    private void getPhotoFromGallery(){
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i, CHOOSE_IMAGE_REQUEST);
    }

    private void launchPhotoMessage(String path){
        Intent i = new Intent(getActivity(), PhotoPickerActivity.class);
        i.putExtra(ATTACHED_PHOTO, path);
        startActivityForResult(i, PHOTO_MESSAGE_REQUEST);
    }

    private final CountDownTimer timer = new CountDownTimer(MAX_RECORDING_TIME, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            long elapsed = MAX_RECORDING_TIME - millisUntilFinished;
            recordTimeText.setText(AppUtils.milliSecondsToTimer(elapsed));
        }

        @Override
        public void onFinish() {
            Log.v(TAG, "Maximum Recording Duration Reached");
            mActionBtn.setPressed(false);
        }
    };

    public static int dp(float value) {
        return (int) Math.ceil(1 * value);
    }

    public int getDp(float value){
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (value * scale + 0.5f);
    }

    private void disconnectSocket(){
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT, onConnected);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnected);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("new message", onNewMessage);
        mSocket.off("user joined", onUserJoined);
        mSocket.off("user left", onUserLeft);
        mSocket.off("typing", onTyping);
        mSocket.off("stop typing", onStopTyping);
        mSocket.off("login", onLogin);
        updateStatus("offline");
    }

    private void connectSocket(){
        mSocket.on(Socket.EVENT_CONNECT, onConnected);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnected);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("new message", onNewMessage);
        mSocket.on("user joined", onUserJoined);
        mSocket.on("user left", onUserLeft);
        mSocket.on("typing", onTyping);
        mSocket.on("stop typing", onStopTyping);
        mSocket.on("login", onLogin);
        mSocket.connect();
        mSocket.emit("add user", SwaggApp.getInstance().getUser().toJSON());
        startVoiceNoteService();
        updateStatus(null);
    }

    private void updateStatus(String status) {
        String subtitle;
        if (status == null){
            if (mSocket.connected()){
                subtitle = USER_ONLINE;
            }else {
                subtitle = USER_OFFLINE;
            }
        }else {
            subtitle = status;
        }
        mActionBar.setSubtitle(subtitle);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "Chat room connected? " + mSocket.connected());
        if (!mSocket.connected()) connectSocket();
    }

    @Override
    public void onDestroy() {
        try{
            stopVoiceNoteService();
            getActivity().unbindService(musicConnection);
            disconnectSocket();
        }catch (IllegalArgumentException e){ Log.e(TAG, "Error closing chat room: " + e.toString()); }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHOOSE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            launchPhotoMessage(data.getDataString());
        }

        if (requestCode == PHOTO_MESSAGE_REQUEST && resultCode == Activity.RESULT_OK){
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inDither = true;

            mFilePath = data.getStringExtra(Chat.KEY_MEDIA_FILE);
            Bitmap mBitmap = AppUtils.decodeBitmapFromFile(mFilePath, 720, 720);

            File file = new File(mFilePath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if ((file.length()/1024) > 500){
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            }else{
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            }

            // bitmap object
            byte[] byteImage_photo = baos.toByteArray();

            //generate base64 string of image
            String encodedImage = Base64.encodeToString(byteImage_photo, Base64.DEFAULT);

            Chat chat = new Chat(encodedImage, data.getStringExtra(Chat.KEY_BODY));
            chat.setFileName(data.getStringExtra(Chat.KEY_FILENAME));
            chat.setLocalMedia(mFilePath);
            uploadMedia(chat);
        }
    }

    private void startRecordingWrapper(){
        try {
            startRecording();
            isRecording = true;
            recordPanel.setVisibility(View.VISIBLE);
        } catch (IOException e) {
            Log.e(TAG, "Error while recording: " + e.toString());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_chat, menu);
        this.menu = menu;
        MenuItem item = menu.findItem(R.id.action_notification_settings);
        updateMenuTitles(item);
    }

    private void updateMenuTitles(MenuItem item) {
        if (mSession.getPushNotifiable()) {
            item.setTitle("Disable Notifications");
        } else {
            item.setTitle("Enable Notifications");
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_attach_photo:
                if (mSocket == null || !mSocket.connected()){
                    Toast.makeText(getActivity(), "Network connection error. Please retry when you are connected",
                            Toast.LENGTH_SHORT).show();
                }else{
                    getPhotoFromGallery();
                }
                return true;
            case R.id.action_notification_settings:
                updateMenuTitles(item);
                return true;
            case R.id.action_invite_friends:
                AppUtils.inviteFriends(getActivity());
                return true;
            default:
                break;
        }

        return false;
    }
}

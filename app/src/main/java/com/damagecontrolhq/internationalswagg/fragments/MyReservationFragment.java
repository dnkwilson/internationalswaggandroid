package com.damagecontrolhq.internationalswagg.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.damagecontrolhq.internationalswagg.CheckoutActivity;
import com.damagecontrolhq.internationalswagg.LoginActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.SaleTransactionAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.models.SaleTransaction;
import com.damagecontrolhq.internationalswagg.models.SaleTransactionItem;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_PAYMENT_METHOD;
import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_PAYMENT_METHOD_TYPE;
import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_PAYMENT_NONCE;
import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_RESERVATION;
import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_RESERVATION_BALANCE;
import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_SALE_TRANSACTION_ID;
import static com.damagecontrolhq.internationalswagg.ReservationItemActivity.ARG_CHECKOUT_REQUEST_CODE;
import static com.damagecontrolhq.internationalswagg.helpers.SessionManager.BRAINTREE_TOKEN;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyReservationFragment extends Fragment {
    private static final String TAG = MyReservationFragment.class.getName();
    public static final String ARGS_SALE_TRANSACTION_ITEMS = "sale_transaction_items";

    private ListView mListView;
    private ImageView mImage;
    private SaleTransactionAdapter mAdapter;
    private ArrayList<SaleTransactionItem> mItems;
    private Button mStatusBtn;
    private BraintreeFragment mBraintreeFragment;

    private String mToken;
    private SaleTransaction mSale;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SwaggApp.getInstance().checkLogin();
        SessionManager mSession = SwaggApp.getInstance().getSession();
        if (!mSession.isLoggedIn()){
            Intent intent = new Intent(getContext(), LoginActivity.class);
            intent.putExtra(SwaggApp.REQUESTED_PAGE, MyReservationFragment.class.getName());
            startActivity(intent);
        }

        mToken = SwaggApp.getInstance().getSession().getPref(BRAINTREE_TOKEN);
        try {
            // mBraintreeFragment is ready to use!
            mBraintreeFragment = BraintreeFragment.newInstance(getActivity(), mToken);
        } catch (InvalidArgumentException e) {
            // There was an issue with your authorization string.
            Log.e(TAG, "Error building braintree fragment");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_my_reservation, container, false);

        mListView = (ListView) v.findViewById(R.id.reservation_list);
        mImage = (ImageView) v.findViewById(R.id.package_image);
        mStatusBtn = (Button) v.findViewById(R.id.reservation_status);

        mStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Get payment nounce");
                DropInRequest dropInRequest = new DropInRequest()
                        .clientToken(mToken)
                        .collectDeviceData(true);
                startActivityForResult(dropInRequest.getIntent(getActivity()), ARG_CHECKOUT_REQUEST_CODE);
            }
        });

        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setTitle("My Reservation");
        mActionBar.setSubtitle(null);

        mItems = getActivity().getIntent().getParcelableArrayListExtra(ARGS_SALE_TRANSACTION_ITEMS);
        getItems();

        return v;
    }

    public void getItems() {
        SwaggApp.api(true).myOrders().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Gson gson = new Gson();
                JsonElement key = response.body().getAsJsonArray("sale_transactions");
                Type listType = new TypeToken<ArrayList<SaleTransaction>>() {}.getType();
                ArrayList<SaleTransaction> sales = gson.fromJson(key,listType);

                mSale = sales.get(0);
                mItems = mSale.getSaleTransactionItems();

                mStatusBtn.setVisibility(View.VISIBLE);
                if (mSale.isPaidInFull()){
                    mStatusBtn.setEnabled(false);
                    mStatusBtn.setText(mSale.getFormattedStatus());
                    mStatusBtn.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.btn_green));
                }else{
                    mStatusBtn.setText("Pay Balance (" + mSale.getPrice() + ")");
                }

                String name = mSale.getPackageName().toLowerCase();
                int bgColor, textColor;
                String packageName = getActivity().getPackageName();
                bgColor = getResources().getIdentifier(name + "_bg", "color", packageName);
                textColor = getResources().getIdentifier(name, "color", packageName);

                Picasso.with(getActivity()).load(ImageUtils.getImageUrl(mSale.getPackageImage())).into(mImage);
                mImage.setBackgroundColor(ContextCompat.getColor(getActivity(), bgColor));

                SaleTransactionAdapter adapter = new SaleTransactionAdapter(getActivity(), mItems);
                mListView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ARG_CHECKOUT_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                    Intent intent = new Intent(getActivity(), CheckoutActivity.class);
                    intent.putExtra(ARG_PAYMENT_NONCE, result.getPaymentMethodNonce().getNonce());
                    intent.putExtra(ARG_PAYMENT_METHOD_TYPE, result.getPaymentMethodType());
                    intent.putExtra(ARG_PAYMENT_METHOD, result.getPaymentMethodType().getDrawable());
                    intent.putExtra(ARG_RESERVATION_BALANCE, true);
                    intent.putExtra(ARG_SALE_TRANSACTION_ID, mSale.getId());
                    intent.putExtra(ARG_RESERVATION, mSale.getReservation());
                    startActivity(intent);
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // the user canceled
                    Log.d(TAG, "Checkout cancelled!");
                } else {
                    // handle errors here, an exception may be available in
                    Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                    Log.e(TAG, "Error creating payment nonce: " + error.getMessage());
                }
        }
    }
}

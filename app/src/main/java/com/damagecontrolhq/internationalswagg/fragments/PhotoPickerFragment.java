package com.damagecontrolhq.internationalswagg.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Chat;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;
import com.damagecontrolhq.internationalswagg.utils.ExifUtil;

/**
 * InternationalSwagg
 * Created by dwilson on 11/24/15.
 */
public class PhotoPickerFragment extends Fragment {
    private final static String TAG = PhotoPickerFragment.class.getName();
    private EditText mCaption;
    private String mPhotoPath;
    private Bitmap mBitmap;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = getActivity().getIntent();
        mPhotoPath = getFilePath(Uri.parse(i.getStringExtra(ChatFragment.ATTACHED_PHOTO)));

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inDither = true;
        Bitmap temp = AppUtils.decodeBitmapFromFile(mPhotoPath, 720, 720);
        mBitmap = ExifUtil.rotateBitmap(mPhotoPath, temp);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo_picker, container, false);

        // Setup toolbars
        Toolbar mToolbar = (Toolbar)v.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(true);
        mToolbar.setBackgroundColor(Color.TRANSPARENT);
        mToolbar.setTitle(R.string.send_photo);

        // Set header toolbar navigation
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ImageView mPreviewImage = (ImageView) v.findViewById(R.id.preview_img);
        Button mSend = (Button) v.findViewById(R.id.send_btn);
        Button mCancel = (Button) v.findViewById(R.id.cancel_btn);
        mCaption = (EditText)v.findViewById(R.id.photo_caption);

        mPreviewImage.setImageBitmap(mBitmap);
        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                getActivity().setResult(Activity.RESULT_CANCELED, i);
                getActivity().finish();
            }
        });

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Send button pressed");
                Intent i = new Intent();
                i.putExtra(Chat.KEY_MEDIA_FILE, mPhotoPath);
                i.putExtra(Chat.KEY_BODY, mCaption.getText().toString());
                i.putExtra(Chat.KEY_FILENAME, parseFileName());
                getActivity().setResult(Activity.RESULT_OK, i);
                getActivity().finish();
            }
        });

        return v;
    }

    private String parseFileName(){
        String s[] = mPhotoPath.split("/");
        return s[s.length-1];
    }

    private String getFilePath(Uri selectedImage) {
        String picturePath = null;
        try{
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
            }
        }catch (Exception e){
            Log.e(TAG, "getFilePath: " + e.getMessage());
        }

        return picturePath;
    }

    @Override
    public void onPause() {
        super.onPause();
        mBitmap = null;
    }
}

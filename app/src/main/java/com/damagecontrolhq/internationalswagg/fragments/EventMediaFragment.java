package com.damagecontrolhq.internationalswagg.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.EventListAdapter;
import com.damagecontrolhq.internationalswagg.adapters.EventMediaListAdapter;
import com.damagecontrolhq.internationalswagg.models.EventMedia;

import java.util.ArrayList;

/**
 * InternationalSwagg
 * Created by dwilson on 12/17/15.
 */
public class EventMediaFragment extends Fragment {
    private static final String TAG = EventMediaFragment.class.getName();

    private RecyclerView mRecyclerView;
    private EventMediaListAdapter mAdapter;
    private ArrayList<EventMedia> mList = new ArrayList<>();
    private String mediaType, eventId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_event_media, container, false);

        mRecyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        Intent i = getActivity().getIntent();
        String eventName = i.getStringExtra(EventListAdapter.EVENT_NAME);
        mediaType = i.getStringExtra(EventListAdapter.EVENT_MEDIA_URL);
        eventId = i.getStringExtra(EventListAdapter.EVENT_ID);
        mList = i.getParcelableArrayListExtra(EventListAdapter.EVENT_MEDIA);

        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setTitle(eventName);

        mAdapter = new EventMediaListAdapter(mList);
        mRecyclerView.setAdapter(mAdapter);

        return v;
    }
}

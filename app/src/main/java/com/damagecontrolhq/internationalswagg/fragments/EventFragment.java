package com.damagecontrolhq.internationalswagg.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.EventListAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.SwaggDBHelper;
import com.damagecontrolhq.internationalswagg.models.Event;
import com.damagecontrolhq.internationalswagg.models.EventList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * InternationalSwagg
 * Created by dwilson on 10/15/15.
 */
@SuppressWarnings("ALL")
public class EventFragment extends Fragment {
    private static final String TAG = "EventFragment";

    private ArrayList<Event> mEvents = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private EventListAdapter mAdapter;
    private SwaggDBHelper db;

    private void setAdapter() {
        mAdapter = new EventListAdapter(getContext(), mEvents);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void getEvents() {
        SwaggApp.api(true).getEvents().enqueue(new Callback<EventList>() {
            @Override
            public void onResponse(Call<EventList> call, retrofit2.Response<EventList> response) {
                if (response.isSuccessful()){
                    mEvents = response.body().events;

                    for (int i = 0; i < mEvents.size(); i++) {
                        Event event = mEvents.get(i);
                        db.createEvent(event);
                    }
                    db.closeDB();
                    Log.d(TAG, "No. of events: " + mEvents.size());
                }
                setAdapter();
            }

            @Override
            public void onFailure(Call<EventList> call, Throwable t) {
                Log.e(TAG, "Failed to get Events list" + t.getMessage());
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        getActivity().invalidateOptionsMenu();

        db = new SwaggDBHelper(getContext().getApplicationContext());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_event, container, false);

        mRecyclerView = (RecyclerView)v.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setTitle("Events");
        mActionBar.setSubtitle(null);

        if (mEvents == null || mEvents.size() == 0){
            ArrayList<Event> events = db.getAllEvents();
            if (events.size() == 0){
                getEvents();
            }else{
                setAdapter();
                for (int i = 0; i < events.size(); i++) {
                    mEvents.add(events.get(i));
                    mAdapter.notifyDataSetChanged();
                }
            }
        }else{
            setAdapter();
        }

        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
//        SwaggApp.getInstance().cancelPendingRequests(DamageControlAPI.EVENTS);
    }

    @Override
    public void onDestroy() {
        super.onPause();
//        SwaggApp.getInstance().cancelPendingRequests(DamageControlAPI.EVENTS);
    }
}

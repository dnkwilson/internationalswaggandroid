package com.damagecontrolhq.internationalswagg.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damagecontrolhq.internationalswagg.BoxTvActivity;
import com.damagecontrolhq.internationalswagg.MusicPlayerActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.CountdownViewHolder;
import com.damagecontrolhq.internationalswagg.adapters.DashboardAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.models.DashboardItem;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * InternationalSwagg
 * Created by dwilson on 8/25/15.
 */
public class DashboardFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private static final String TAG = DashboardFragment.class.getSimpleName();

    private List<DashboardItem> mDashItems = new ArrayList<>();
    private DashboardAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    private void initList() {
        // We populate the planets
        mDashItems.add(new DashboardItem("countdown"));
        mDashItems.add(new DashboardItem("Box TV", R.drawable.ic_live_tv_black_48dp, BoxTvActivity.class.getName(), R.drawable.btn_secondary));
        mDashItems.add(new DashboardItem("Chat", R.drawable.ic_forum_white_48dp, ChatFragment.class.getName(), R.drawable.btn_primary));
        mDashItems.add(new DashboardItem("Events", R.drawable.ic_event_white_48dp, EventFragment.class.getName(), R.drawable.btn_green));
        mDashItems.add(new DashboardItem("Music", R.drawable.ic_headset_white_48dp, MusicPlayerActivity.class.getName(), R.drawable.btn_indigo));
        mDashItems.add(new DashboardItem("Affiliates", R.drawable.ic_pages_white_48dp, AffiliatesFragment.class.getName(), R.drawable.btn_black));
        if (SwaggApp.getInstance().getSession().isLoggedIn()){
            mDashItems.add(new DashboardItem("Profile", R.drawable.ic_person_white_48dp, ProfileFragment.class.getName(), R.drawable.btn_orange));
        }else{
            mDashItems.add(new DashboardItem("Login", R.drawable.ic_exit_to_app_white_48dp, LoginFragment.class.getName(), R.drawable.btn_orange));
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        setRetainInstance(true);

        mAdapter = new DashboardAdapter(getActivity(), mDashItems);
        initList();
        mAdapter.notifyDataSetChanged();

        SharedPreferences prefs = getActivity().getSharedPreferences(SwaggApp.PACKAGE_NAME,
                Context.MODE_PRIVATE);
        if (!prefs.getBoolean(SwaggApp.INVITED_FRIENDS, false)){
            prefs.edit().putBoolean(SwaggApp.INVITED_FRIENDS, true).apply();
            promptInvitation();
        }
    }

    private void promptInvitation(){
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Invite a friend");

        alert.setPositiveButton("Invite", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                AppUtils.inviteFriends(getActivity());
            }
        });

        alert.show();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);

        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setTitle("International Swagg");
        mActionBar.setSubtitle(null);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mSwipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.dashboard);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mRecyclerView.setAdapter(mAdapter);

        return v;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSwipeRefreshLayout.setRefreshing(false);
        mDashItems = null;
        CountdownViewHolder holder = (CountdownViewHolder)mRecyclerView.findViewHolderForAdapterPosition(0);
        if(holder != null && holder.timerIsRunning){
            holder.getTimer().cancel();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        CountdownViewHolder holder = (CountdownViewHolder)mRecyclerView.findViewHolderForAdapterPosition(0);
        if(holder != null && !holder.timerIsRunning){
            holder.getTimer().start();
        }
    }
}

package com.damagecontrolhq.internationalswagg.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.adapters.PostListAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.interfaces.RecyclerItemClickListener;
import com.damagecontrolhq.internationalswagg.models.Post;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * InternationalSwagg
 * Created by dwilson on 11/28/15.
 */
public class FeedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private List<Post> mPosts = new ArrayList<>();
    private PostListAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().invalidateOptionsMenu();
        setRetainInstance(true);

        getActivity().invalidateOptionsMenu();

        Intent i = getActivity().getIntent();
        ArrayList <Post> posts = i.getParcelableArrayListExtra("posts");
        if (posts != null) mPosts = posts;
        mAdapter = new PostListAdapter(getActivity(), mPosts);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_feed, container, false);

        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setTitle("The Feed");
        mActionBar.setSubtitle(null);

        RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mSwipeRefreshLayout = (SwipeRefreshLayout)v.findViewById(R.id.dashboard);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mRecyclerView.setAdapter(mAdapter);
        if (mPosts.size() == 0) getPosts();
        return v;
    }

    private void getPosts() {
        SwaggApp.api(false).getPosts().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                parseJSON(response.body());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    /**
     * Parsing json response and passing the data to feed view list adapter
     * */
    private void parseJSON(JsonObject response) {
        try {
            mPosts.clear();

            String string = response.getAsJsonArray("posts").toString();
            JSONArray posts = new JSONArray(string);
            for (int i = 0; i < posts.length(); i++) {
                JSONObject json = (JSONObject) posts.get(i);
                Post post = new Post(json);
                mPosts.add(post);
            }
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {  e.printStackTrace(); }
    }

    @Override
    public void onRefresh() {
        getPosts();
        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onResume() {
        super.onResume();
        getPosts();
        mAdapter.notifyDataSetChanged();
    }
}

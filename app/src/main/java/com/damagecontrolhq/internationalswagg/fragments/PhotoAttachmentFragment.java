package com.damagecontrolhq.internationalswagg.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Chat;
import com.damagecontrolhq.internationalswagg.models.Post;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

/**
 * InternationalSwagg
 * Created by dwilson on 11/27/15.
 */
public class PhotoAttachmentFragment extends Fragment {

    private ImageView mImageView;
    private ActionBar mActionBar;
    private String mName;
    private String mUrl, mTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = getActivity().getIntent();
        mUrl = i.getStringExtra(Chat.KEY_MEDIA_FILE);
        mTitle = i.getStringExtra(Post.KEY_TITLE);


//        String s[] = mUrl.split("/");
//        mName =  s[s.length-1];
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo_attachment, container, false);

        // Setup toolbars
        Toolbar mToolbar = (Toolbar)v.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(true);
        mToolbar.setBackgroundColor(Color.TRANSPARENT);

        if(mTitle != null) mActionBar.setTitle(mTitle);

        // Set header toolbar navigation
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        mImageView = (ImageView)v.findViewById(R.id.photo);
        Picasso.with(getContext()).load(ImageUtils.getImageUrl(mUrl)).into(mImageView);

        return v;
    }
}

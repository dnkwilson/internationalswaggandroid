package com.damagecontrolhq.internationalswagg.fragments;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.MainActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.DamageControlAPI;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.models.User;
import com.damagecontrolhq.internationalswagg.utils.ParseUtils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * InternationalSwagg
 * Created by dwilson on 10/23/15.
 */
//@RuntimePermissions
public class LoginFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{
    private static final String TAG = LoginFragment.class.getName();

    private static final int RC_SIGN_IN = 321;

    private CallbackManager mCallbackManager;
    private AccountManager mAccountManager;

    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSigninClicked;

    private Activity mActivity;
    private SessionManager mSession;
    //    private int mRequestMethod = Request.Method.POST;
    private String mRequestString;
    private String mRequestedPage;
    private String mLoginMessage;
    private Button mGoogleButton;

    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        mActivity = getActivity();
        mSession = SwaggApp.getInstance().getSession();

        Intent i = mActivity.getIntent();
        mRequestedPage = i.getStringExtra(SwaggApp.REQUESTED_PAGE);
        mLoginMessage = i.getStringExtra(SwaggApp.LOGIN_MESSAGE);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestServerAuthCode(getString(R.string.default_web_client_id), false)
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        if (mLoginMessage != null) {
            TextView messageView = (TextView) v.findViewById(R.id.login_message);
            messageView.setText(mLoginMessage);
            messageView.setVisibility(View.VISIBLE);
        }

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "Facebook UID = " + loginResult.getAccessToken().getUserId());
                mSession.createLogin(loginResult.getAccessToken().getUserId(),
                        loginResult.getAccessToken().getToken());
                mRequestString = DamageControlAPI.LOGIN;
                callAPI();
            }

            @Override
            public void onCancel() {
                mSession.setPref(User.KEY_PROVIDER, null);
                Log.e(TAG, "Facebook login cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                mSession.setPref(User.KEY_PROVIDER, null);
                Log.e(TAG, "Error logging into facebook: " + error.getMessage());
            }
        });

        Button loginButton = (Button) v.findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSession.setPref(User.KEY_PROVIDER, SessionManager.FB);
                LoginManager.getInstance().logInWithReadPermissions(LoginFragment.this, Arrays.asList(SwaggApp.FB_PERMISSIONS));
            }
        });

        mGoogleButton = (Button) v.findViewById(R.id.google_login_button);
        mGoogleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mGoogleApiClient.isConnected()){
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }
            }
        });

        return v;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
        }else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null && acct.getServerAuthCode() != null) {
                mSession.setPref(User.KEY_PROVIDER, SessionManager.GOOGLE);
                mSession.setLoginUid(acct.getId());
                mSession.setLoginToken(acct.getServerAuthCode());
                callAPI();
            }else{
                Toast.makeText(getContext(), "Error authenticating with Google", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Error authenticating with Google", Toast.LENGTH_SHORT).show();
        }
    }

    private void callAPI() {
        pDialog = new ProgressDialog(mActivity);
        pDialog.setMessage("Loading...");
        pDialog.show();

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; " +
                "charset=utf-8"), mSession.getLogin().toString());
        SwaggApp.api(false).login(body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                handleResponse(response.body());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "Error logging in: " + t.getMessage());
                pDialog.dismiss();
            }
        });
    }

    private void handleResponse(JsonObject response) {
        try {
            String string = response.getAsJsonObject().toString();
            JSONObject json = new JSONObject(string);

            if ((json.getJSONObject("meta").getBoolean("is_logged_in")) && (json.getJSONObject("meta").getBoolean("success"))) {
                if (json.getJSONObject("meta").getBoolean("success")) {
                    User mUser = new User(json.getJSONObject("user"));
                    mSession.createSession(mUser);
                    Log.d(TAG, "API KEY after response: " + mSession.getUserToken());

                    // Register with parse
                    SharedPreferences prefs = getActivity().getSharedPreferences(SwaggApp.PACKAGE_NAME, Context.MODE_PRIVATE);
                    if (prefs.getBoolean(SwaggApp.FIRST_RUN, true)) {
                        ParseUtils.registerParse();
                        prefs.edit().putBoolean("first_run", false).apply();
                    }

                    Intent intent = new Intent(mActivity, MainActivity.class);
                    // set the new task and clear flags
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra(SwaggApp.REQUESTED_PAGE, mRequestedPage);
                    startActivity(intent);
                } else {
                    mSession.clearSession();
                    Toast.makeText(mActivity, json.getJSONObject("meta").getString("info"), Toast.LENGTH_SHORT).show();
                }
            } else {
                mSession.clearSession();
                Toast.makeText(mActivity, json.getJSONObject("meta").getString("info"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing server hash: " + e.toString());
        }
        pDialog.dismiss();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Failed Google authentication");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Google Client Connected!");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Google Client Suspended!");
    }
}

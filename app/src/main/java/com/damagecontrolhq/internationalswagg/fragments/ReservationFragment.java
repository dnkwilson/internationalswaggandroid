package com.damagecontrolhq.internationalswagg.fragments;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.LoginActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.ReservationItemActivity;
import com.damagecontrolhq.internationalswagg.adapters.ReservationAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.interfaces.RecyclerItemClickListener;
import com.damagecontrolhq.internationalswagg.models.Reservation;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_RESERVATION;
import static com.damagecontrolhq.internationalswagg.helpers.SessionManager.BRAINTREE_TOKEN;

/**
 * Created by dwilson on 1/28/17.
 */
public class ReservationFragment extends Fragment implements RecyclerItemClickListener {
    private static final String TAG = ReservationFragment.class.getName();

    private ReservationAdapter mAdapter;
    private RecyclerView mRecyclerView;

    private ArrayList<Reservation> mList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reservation, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (mActionBar != null){
            mActionBar.setTitle("Reserve Table");
            mActionBar.setSubtitle(null);
        }

        mAdapter = new ReservationAdapter(getContext(), this, mList);
        mRecyclerView.setAdapter(mAdapter);

        getReservations();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mList.size() == 0){
            getReservations();
        }
    }

    private void getReservations() {
        SwaggApp.api(true).getPackages().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Gson gson = new Gson();
                JsonElement key = response.body().getAsJsonArray("packages");
                Type listType = new TypeToken<ArrayList<Reservation>>() {}.getType();
                mList = gson.fromJson(key,listType);
                mAdapter = new ReservationAdapter(getContext(), ReservationFragment.this, mList);
                Log.d("ReservationTest1", "ReservationList: " + mList.size());
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "GET Packages error: " + t.getMessage());
            }
        });
    }

    private void displayReservationItem(ReservationAdapter.PackageViewHolder holder, int position) {
        Bundle bundle = new Bundle();

        // Note that we need the API version check here because the actual transition classes (e.g. Fade)
        // are not in the support library and are only available in API 21+. The methods we are calling on the Fragment
        // ARE available in the support library (though they don't do anything on API < 21)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(getActivity(), holder.packageImage, "packageImage");
            bundle = options.toBundle();
        }

        Intent intent = new Intent(getActivity(), ReservationItemActivity.class);
        intent.putExtra(ARG_RESERVATION, mList.get(position));
        startActivity(intent, bundle);
    }

    @Override
    public void onItemClicked(final ReservationAdapter.PackageViewHolder holder, final int position) {

        if (!SwaggApp.getInstance().getSession().isLoggedIn()){
            Intent intent = new Intent(getContext(), LoginActivity.class);
            intent.putExtra(SwaggApp.REQUESTED_PAGE, ReservationItemActivity.class.getName());
            startActivity(intent);
        }else {

            final ProgressDialog progressDialog = ProgressDialog.show(getContext(), null, "Loading. Please wait...", true);
            //you usually don't want the user to stop the current process, and this will make sure of that
            progressDialog.setCancelable(false);
            progressDialog.show();

            SwaggApp.api(true).getToken().enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.isSuccessful()) {
                        JsonObject json = response.body();
                        Log.e(TAG, "Token String: " + json.toString());
                        String mToken = json.get("token").getAsString();
                        SessionManager sessionManager = SwaggApp.getInstance().getSession();
                        sessionManager.setPref(BRAINTREE_TOKEN, mToken);
                        displayReservationItem(holder, position);
                    } else {
                        SwaggApp.getInstance().getSession().setPref(BRAINTREE_TOKEN, null);
                        Toast.makeText(getContext(), "Error getting client token",
                                Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    SwaggApp.getInstance().getSession().setPref(BRAINTREE_TOKEN, null);
                    Log.e(TAG, "Error getting client token");
                    progressDialog.dismiss();
                }
            });
        }
    }
}

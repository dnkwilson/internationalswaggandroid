package com.damagecontrolhq.internationalswagg.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.damagecontrolhq.internationalswagg.LoginActivity;
import com.damagecontrolhq.internationalswagg.ProfileActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.DamageControlAPI;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.models.Comment;
import com.damagecontrolhq.internationalswagg.models.Post;
import com.damagecontrolhq.internationalswagg.views.CommentView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * InternationalSwagg
 * Created by dwilson on 10/24/15.
 */
public class CommentListFragment extends Fragment {
    private static final String TAG = "CommentListFragment";

    private CommentAdapter mAdapter;
    private ListView mListView;
    private EditText mCommentEntry;
    private ArrayList<Comment> mComments = new ArrayList<>();
    private Post mPost;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        getActivity().invalidateOptionsMenu();

        Intent intent = getActivity().getIntent();
        mPost = intent.getParcelableExtra("post");
        mComments = mPost.getComments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_comment_list, container, false);

        mListView = (ListView)v.findViewById(R.id.comment_list);
        mCommentEntry = (EditText)v.findViewById(R.id.comment_entry);
        if (!SwaggApp.getInstance().getSession().isLoggedIn()) mCommentEntry.setVisibility(View.GONE);

        mCommentEntry.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.i("event", "captured");
                    callApi();
                    return false;
                }

                return false;
            }
        });

        mAdapter = new CommentAdapter(getActivity(), mComments);
        mListView.setAdapter(mAdapter);
        mListView.smoothScrollToPosition(mAdapter.getCount() - 1);


        return v;
    }

    private void callApi() {
        String url = String.format("%s/%s/comments", DamageControlAPI.POSTS, mPost.getID());
//        ApiRequest apiRequest = new ApiRequest(Request.Method.POST, url, getParams(), getContext());
//        apiRequest.setOnVolleyResponseListener(new OnVolleyResponseListener() {
//            @Override
//            public void onComplete(JSONObject json) {
//                parse(json);
//                mCommentEntry.setText(null);
//            }
//
//            @Override
//            public void onError(Throwable error) {
//
//            }
//        });
        mListView.smoothScrollToPosition(mAdapter.getCount() - 1);
    }

    private JSONObject getParams() {
        JSONObject params = new JSONObject();
        try {
            JSONObject child = new JSONObject();
            child.put(Comment.KEY_USER_ID, new SessionManager(getActivity()).getUser().getID());
            child.put(Comment.KEY_COMMENT, mCommentEntry.getText());
            params.put("comment", child);

        }catch (JSONException e){ e.printStackTrace();}
        return params;
    }

    private void parse(JSONObject json) {
        if (json != null) {
            try{
                Comment comment = new Comment(json.getJSONObject("comment"));
                mPost.getComments().add(comment);
                mAdapter.notifyDataSetChanged();
            }catch (JSONException e){e.printStackTrace();}
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_login:
                Intent login = new Intent(getActivity(), LoginActivity.class);
                startActivity(login);
                return true;
            case R.id.action_profile:
                Intent profile = new Intent(getActivity(), ProfileActivity.class);
                startActivity(profile);
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class CommentAdapter extends ArrayAdapter<Comment> {

        final ArrayList<Comment> comments;
        final Context context;

        public CommentAdapter(Context c, ArrayList<Comment> comments) {
            super(c, 0, comments);
            this.comments = comments;
            this.context  = c;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CommentView commentView = (CommentView)convertView;
            if (null == commentView)
                commentView = new CommentView(getContext());
            commentView.setItem(getItem(position));

            return commentView;
        }

    }
}

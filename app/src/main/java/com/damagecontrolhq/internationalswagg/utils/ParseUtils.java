package com.damagecontrolhq.internationalswagg.utils;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.R;

/**
 * InternationalSwagg
 * Created by dwilson on 12/10/15.
 */
public class ParseUtils {
    private static String TAG = ParseUtils.class.getSimpleName();

    public static void verifyParseConfiguration(Context context) {
        if (TextUtils.isEmpty(context.getString(R.string.parse_app_id)) || TextUtils.isEmpty(context.getString(R.string.parse_client_key))) {
            Toast.makeText(context, "Please configure your Parse Application ID and Client Key in AppConfig.java", Toast.LENGTH_LONG).show();
            ((Activity) context).finish();
        }
    }

    public static void registerParse() {
//        ParsePush.subscribeInBackground(SwaggApp.getInstance().getString(R.string.parse_channel), new SaveCallback() {
//            @Override
//            public void done(ParseException e) {
//                Log.e(TAG, "Successfully subscribed to Parse!");
//            }
//        });
    }

    public static void subscribeWithEmail(String email) {
//        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
//
//        installation.put("email", email);
//
//        installation.saveInBackground();
    }
}

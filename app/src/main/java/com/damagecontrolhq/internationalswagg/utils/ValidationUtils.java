package com.damagecontrolhq.internationalswagg.utils;

import android.telephony.PhoneNumberUtils;
import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * InternationalSwagg
 * Created by dwilson on 10/23/15.
 */
public class ValidationUtils {
    // Regular Expression
    // you can change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REGEX = "\\d{3}-\\d{7}";

    // Error Messages
    private static final String REQUIRED_MSG = "required";
    private static final String EMAIL_MSG = "invalid email";
    private static final String PHONE_MSG = "###-#######";

    // call this method when you need to check email validation
    public static boolean isEmailAddress(EditText editText, boolean required) {
        return isValid(editText, required);
    }

    // call this method when you need to check phone number validation
    public static boolean isPhoneNumber(EditText editText) {
        boolean isValid;
        if (editText.length() < 10){
            isValid = (editText.getText().toString().replaceAll("[-+.^:,]", "").length() == 7);
        }else{
            isValid =  PhoneNumberUtils.isGlobalPhoneNumber(editText.getText().toString());
        }
        return isValid;
    }

    // return true if the input field is valid, based on the parameter passed
    private static boolean isValid(EditText editText, boolean required) {

        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
        editText.setError(null);

        // text required and editText is blank, so return false
        if ( required && !hasText(editText) ) return false;

        // pattern doesn't match so returning false
        if (required && !Pattern.matches(ValidationUtils.EMAIL_REGEX, text)) {
            editText.setError(ValidationUtils.EMAIL_MSG);
            return false;
        }

        return true;
    }

    // check the input field has any text or not
    // return true if it contains text otherwise false
    private static boolean hasText(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError(REQUIRED_MSG);
            return false;
        }

        return true;
    }

    public static boolean validLength(EditText editText, int minLength){
        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() < minLength) {
            editText.setError(REQUIRED_MSG);
            return false;
        }

        return true;
    }

}

package com.damagecontrolhq.internationalswagg;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;

import com.damagecontrolhq.internationalswagg.adapters.EventListAdapter;
import com.damagecontrolhq.internationalswagg.adapters.GalleryAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.models.ImageGallery;
import com.damagecontrolhq.internationalswagg.models.Photo;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * InternationalSwagg
 * Created by dwilson on 9/1/15.
 */
public class PhotoGalleryActivity extends AppCompatActivity {
    private static final String TAG = PhotoGalleryActivity.class.getName();

    private ArrayList<Photo> mPhotos = new ArrayList<>();
    private GridView mGridView;
    private GalleryAdapter mAdapter;
    private int galleryId;
    private int columnWidth;


    private void getImages(){
        Log.d(TAG, "Getting images");
        SwaggApp.api(true).getGallery(galleryId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject json = response.body();
                    Gson gson = new Gson();
                    JsonElement key = json.getAsJsonObject().get("gallery");
                    mPhotos = gson.fromJson(key, ImageGallery.class).getImages();
                }

                Log.d(TAG, "There are " + mPhotos.size() + " photos ");
                mAdapter = new GalleryAdapter(mPhotos, PhotoGalleryActivity.this, columnWidth);
                mGridView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "Failure getting photos: " + t.getMessage());
            }
        });
    }

    private void initializeGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                SwaggApp.GRID_PADDING, r.getDisplayMetrics());

        columnWidth = (int) ((ImageUtils.getScreenWidth(this) -
                ((SwaggApp.NUM_OF_COLUMNS + 1) * padding)) / SwaggApp.NUM_OF_COLUMNS);

        mGridView.setNumColumns(SwaggApp.NUM_OF_COLUMNS);
        mGridView.setColumnWidth(columnWidth);
        mGridView.setStretchMode(GridView.NO_STRETCH);
        mGridView.setPadding((int) padding, (int) padding, (int) padding,
                (int) padding);
        mGridView.setHorizontalSpacing((int) padding);
        mGridView.setVerticalSpacing((int) padding);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_gallery);

        // Get extras
        Intent intent = getIntent();
        galleryId     = intent.getIntExtra(EventListAdapter.GALLERY_ID, 0);
//        path          = intent.getStringExtra(EventListAdapter.EVENT_MEDIA_URL);
        String eventName = intent.getStringExtra(EventListAdapter.EVENT_NAME);

        // Setup toolbar
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setTitle(eventName);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //noinspection deprecation
            window.setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }

        // Setup View
        mGridView = (GridView)findViewById(R.id.grid_view);

        initializeGridLayout();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getImages();
    }
}

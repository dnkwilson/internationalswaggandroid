package com.damagecontrolhq.internationalswagg;

import android.support.v4.app.Fragment;

import com.damagecontrolhq.internationalswagg.fragments.PhotoAttachmentFragment;

/**
 * InternationalSwagg
 * Created by dwilson on 11/27/15.
 */
public class PhotoAttachmentActivity extends BaseActivity{
    @Override
    protected Fragment createFragment() {
        return new PhotoAttachmentFragment();
    }
}

package com.damagecontrolhq.internationalswagg;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.interfaces.OnMusicServiceListener;
import com.damagecontrolhq.internationalswagg.models.Song;
import com.damagecontrolhq.internationalswagg.services.MusicService;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * InternationalSwagg
 * Created by dwilson on 2/5/16.
 */
public class MusicPlayerActivity extends AppCompatActivity{
    private static final String TAG = MusicPlayerActivity.class.getName();

    public static final String SONG_LIST = "songs", SONG_INDEX = "song_index";
    private ImageButton btnPlay;
    private ImageButton btnNext;
    private ImageButton btnPrevious;
    private ImageButton btnRepeat;
    private ImageButton btnShuffle;
    private ImageView artwork;
    private SeekBar seekBar;
    private TextView currentPosition;
    private TextView duration;

    //service
    private MusicService service;
    private Intent playIntent;

    //binding
    private boolean musicBound = false;
    private boolean isPaused = true;
    private int playbackPosition;

    private Handler mHandler = new Handler();
    private ArrayList<Song> mSongs = new ArrayList<>();

    private int currentSongIndex = 0;
    private boolean isShuffle = false;
    private boolean isRepeat = false;
    private Toolbar mToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_music_player);

        // All player buttons
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        btnNext = (ImageButton) findViewById(R.id.btnNext);
        btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
//        btnPlaylist = (ImageButton) findViewById(R.id.btnPlaylist);
        btnRepeat = (ImageButton) findViewById(R.id.btnRepeat);
        btnShuffle = (ImageButton) findViewById(R.id.btnShuffle);
        seekBar = (SeekBar) findViewById(R.id.songProgressBar);
//        title = (TextView) findViewById(R.id.songTitle);
        currentPosition = (TextView) findViewById(R.id.songCurrentDurationLabel);
        duration = (TextView) findViewById(R.id.songTotalDurationLabel);
        artwork = (ImageView)findViewById(R.id.artwork);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent i = getIntent();
        mSongs   = i.getParcelableArrayListExtra(SONG_LIST);
        currentSongIndex = i.getIntExtra(SONG_INDEX, 0);
        if (mSongs == null || mSongs.size() == 0){
            mSongs = new ArrayList<>();
            getPlayList();
        }

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(!musicBound) return;

                if (isPaused) {
                    if(playbackPosition == 0){
                        playSong(currentSongIndex);
                        initPlayer();
                    }else{
                        service.resumeSong();
                        setIsPlaying();
                        updateProgressBar();
                    }
                }else{
                    service.pausePlayer();
                    setPaused();
                }
            }
        });


        /**
         * Next button click event
         * Plays next song by taking currentSongIndex + 1
         * */
        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // check if next song is there or not
                if(currentSongIndex < (mSongs.size() - 1)){
                    playSong(currentSongIndex + 1);
                    currentSongIndex = currentSongIndex + 1;
                }else{
                    // play first song
                    playSong(0);
                    currentSongIndex = 0;
                }
                initPlayer();

            }
        });

        /**
         * Back button click event
         * Plays previous song by currentSongIndex - 1
         * */
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(currentSongIndex > 0){
                    playSong(currentSongIndex - 1);
                    currentSongIndex = currentSongIndex - 1;
                }else{
                    // play last song
                    playSong(mSongs.size() - 1);
                    currentSongIndex = mSongs.size() - 1;
                }
                initPlayer();

            }
        });

        /**
         * Button Click event for Repeat button
         * Enables repeat flag to true
         * */
        btnRepeat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(isRepeat){
                    isRepeat = false;
                    Toast.makeText(getApplicationContext(), "Repeat is OFF", Toast.LENGTH_SHORT).show();
                    btnRepeat.setImageResource(R.drawable.ic_repeat);
                }else{
                    // make repeat to true
                    isRepeat = true;
                    Toast.makeText(getApplicationContext(), "Repeat is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    isShuffle = false;
                    btnRepeat.setImageResource(R.drawable.ic_repeat_active);
                    btnShuffle.setImageResource(R.drawable.ic_shuffle);
                }
            }
        });

        /**
         * Button Click event for Shuffle button
         * Enables shuffle flag to true
         * */
        btnShuffle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(isShuffle){
                    isShuffle = false;
                    Toast.makeText(getApplicationContext(), "Shuffle is OFF", Toast.LENGTH_SHORT).show();
                    btnShuffle.setImageResource(R.drawable.ic_shuffle_yellow);
                }else{
                    // make repeat to true
                    isShuffle= true;
                    Toast.makeText(getApplicationContext(), "Shuffle is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    isRepeat = false;
                    btnShuffle.setImageResource(R.drawable.ic_shuffle);
                    btnRepeat.setImageResource(R.drawable.ic_repeat);
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    service.seek(progress);
                    currentPosition.setText(AppUtils.milliSecondsToTimer(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // remove message Handler from updating progress bar
                mHandler.removeCallbacks(onEverySecond);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // remove message Handler from updating progress bar
                mHandler.removeCallbacks(onEverySecond);
                service.seek(service.getPosition());
                currentPosition.setText(AppUtils.milliSecondsToTimer(service.getPosition()));

                // update timer progress again
                updateProgressBar();
            }
        });

    }



    private void updateProgressBar(){
        mHandler.postDelayed(onEverySecond, 100);
    }

    //connect to the service
    private final ServiceConnection musicConnection = new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MusicBinder binder = (MusicService.MusicBinder)service;
            //get service
            MusicPlayerActivity.this.service = binder.getService();
            //pass list
            MusicPlayerActivity.this.service.setList(mSongs);
            musicBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound = false;
        }
    };

    private Runnable onEverySecond = new Runnable() {
        @Override
        public void run() {
            if (!isPaused){
                try{
                    if (service.isPlaying()) {
                        // Update seekbar position
                        playbackPosition = service.getPosition();
                        // Displaying time completed playing
                        currentPosition.setText(AppUtils.milliSecondsToTimer(playbackPosition));
                        seekBar.setProgress(playbackPosition);
                        // Running this thread after 100 milliseconds
                        seekBar.postDelayed(onEverySecond, 100);
                    }else{
                        seekBar.setProgress(0);
                        setPaused();
                    }
                }catch (IllegalStateException e){
                    Log.d(TAG, "Service error: " + e.toString());
                    service = null;
                    setPaused();
                }catch (NullPointerException e){
                    Log.e(TAG, "Error with progressbar: " + e.getMessage());
                }
            }
        }
    };

    void initPlayer(){
        // By default play first song
        Song song = mSongs.get(currentSongIndex);
        mToolbar.setTitle(song.getTitle());
        mToolbar.setSubtitle(song.getArtist());
        duration.setText(AppUtils.milliSecondsToTimer(song.getLength()));

        seekBar.setMax(song.getLength());

        Log.d(TAG, "ARTWORK URL: " + song.getArtwork());
        Picasso.with(this).load(ImageUtils.getImageUrl(song.getArtwork())).into(artwork);
    }

    private void setPaused(){
        isPaused = true;
        btnPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_lg_play));
    }

    private void setIsPlaying(){
        isPaused = false;
        btnPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pause_circle));
    }

    //user song select
    private void playSong(int index){
        service.setSong(index);
        service.playSong();
        service.setOnMusicServiceListener(new OnMusicServiceListener() {
            @Override
            public void onReady() {
                setIsPlaying();
                updateProgressBar();
            }

            @Override
            public void onSongCompleted() {
                currentSongIndex++;
                initPlayer();
                setIsPlaying();
                updateProgressBar();
            }

            @Override
            public void updateUI(int index) {
                currentSongIndex = index;
                initPlayer();
                if (isPaused) {
                    setPaused();
                } else {
                    setIsPlaying();
                }

                updateProgressBar();
            }
        });
    }

    /**
     * Receiving song index from playlist view
     * and play the song
     * */
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 100){
            currentSongIndex = data.getExtras().getInt(SONG_INDEX);
            if (mSongs == null || mSongs.size() == 0) mSongs = data.getExtras().getParcelableArrayList(SONG_LIST);
            // play selected song
            playSong(currentSongIndex);
            initPlayer();
        }

    }

    @Override
    public void onDestroy(){
        stopService(playIntent);
        this.unbindService(musicConnection);
        service = null;
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (playIntent == null){
            playIntent = new Intent(this, MusicService.class);
            this.bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            this.startService(playIntent);
        }
    }

    void getPlayList(){
        SwaggApp.api(false).getMusic().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String string = response.body().toString();
                try {
                    JSONObject json = new JSONObject(string);
                    JSONArray array = json.getJSONArray("songs");
                    for (int i = 0; i < array.length(); i++){
                        JSONObject data = (JSONObject)array.get(i);
                        Song song = new Song(data);
                        mSongs.add(song);
                    }
                }catch (JSONException e){
                    Log.e(TAG, "Error parsing server hash: " + e.toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "Error parsing server hash: " + t.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_music, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_playlist:
                Intent i = new Intent(this, PlaylistActivity.class);
                i.putExtra(SONG_INDEX, currentSongIndex);
                i.putParcelableArrayListExtra(SONG_LIST, mSongs);
                startActivityForResult(i, 100);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
package com.damagecontrolhq.internationalswagg;

import android.support.v4.app.Fragment;

import com.damagecontrolhq.internationalswagg.fragments.PhotoPickerFragment;

public class PhotoPickerActivity extends BaseActivity {


    @Override
    protected Fragment createFragment() {
        return new PhotoPickerFragment();
    }
}

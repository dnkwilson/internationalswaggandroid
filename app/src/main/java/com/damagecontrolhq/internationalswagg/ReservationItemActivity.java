package com.damagecontrolhq.internationalswagg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.damagecontrolhq.internationalswagg.adapters.ReservationChildViewHolder;
import com.damagecontrolhq.internationalswagg.adapters.ReservationItemAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.fragments.ReservationProductFragment;
import com.damagecontrolhq.internationalswagg.interfaces.NestedRecyclerViewItemClickListener;
import com.damagecontrolhq.internationalswagg.interfaces.OnReservationProductListener;
import com.damagecontrolhq.internationalswagg.models.Category;
import com.damagecontrolhq.internationalswagg.models.Reservation;
import com.damagecontrolhq.internationalswagg.models.ReservationItem;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_PAYMENT_METHOD;
import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_PAYMENT_METHOD_TYPE;
import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_PAYMENT_NONCE;
import static com.damagecontrolhq.internationalswagg.CheckoutActivity.ARG_RESERVATION;
import static com.damagecontrolhq.internationalswagg.fragments.ReservationProductFragment.ARG_PRODUCT;
import static com.damagecontrolhq.internationalswagg.helpers.SessionManager.BRAINTREE_TOKEN;

public class ReservationItemActivity extends AppCompatActivity implements
        NestedRecyclerViewItemClickListener, OnReservationProductListener {
    private static final String TAG = ReservationItemActivity.class.getName();
    private static final String ARG_PARENT_POS = "parent_position";
    private static final String ARG_CHILD_POS = "child_position";

    public static final int ARG_UPDATE_PRODUCT_REQUEST_CODE = 900,
            ARG_CHECKOUT_REQUEST_CODE = 901,
            ARG_VERIFY_NAME_REQUEST_CODE = 902;

    private ImageView mPhoto;
    private TextView mPrice;
    private RecyclerView mRecyclerView;
    private Button mReserveBtn;
    private BraintreeFragment mBraintreeFragment;

    private Reservation reservation;
    private String mToken;
    private HashMap<String, Integer> itemToSwapPosition = new HashMap<>();
    private ReservationChildViewHolder mViewHolder;

    private ReservationItemAdapter mAdapter;
    private ArrayList<ReservationItem> mReservationItems = new ArrayList<>();
    private ArrayList<Category> mCategories = new ArrayList<>();

    private void initPayment(){
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken(mToken)
                .collectDeviceData(true);
        startActivityForResult(dropInRequest.getIntent(this), ARG_CHECKOUT_REQUEST_CODE);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_item);

        mToken = SwaggApp.getInstance().getSession().getPref(BRAINTREE_TOKEN);
        try {
            // mBraintreeFragment is ready to use!
            mBraintreeFragment = BraintreeFragment.newInstance(this, mToken);
            createView();
        } catch (InvalidArgumentException e) {
            // There was an issue with your authorization string.
            Log.e(TAG, "Error building braintree fragment");
        }
    }

    private void createView() {
        // Inflate the layout for this fragment
        mPhoto = (ImageView) findViewById(R.id.package_image);
        mPrice = (TextView) findViewById(R.id.package_price);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mReserveBtn = (Button) findViewById(R.id.package_reserve);

        mReserveBtn.setEnabled(mToken != null);

        reservation = getIntent().getExtras().getParcelable(ARG_RESERVATION);

        Picasso.with(this).load(ImageUtils.getImageUrl(reservation.getPhoto())).into(mPhoto);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(reservation.getName() + " Package");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mPrice.setText(AppUtils.formatPrice(reservation.getPrice().getFormattedPrice()));

        String name = reservation.getName().toLowerCase();
        int bgColor, textColor;
        String packageName = getPackageName();
        bgColor = getResources().getIdentifier(name + "_bg", "color", packageName);
        textColor = getResources().getIdentifier(name, "color", packageName);

        mPhoto.setBackgroundColor(ContextCompat.getColor(this, bgColor));
        mPrice.setTextColor(ContextCompat.getColor(this, textColor));

        mReservationItems = reservation.getReservationItems();

        mCategories = reservation.getCategories();

        mReserveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initPayment();
            }
        });

        mAdapter = new ReservationItemAdapter(this, mCategories, this);
        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @UiThread
            @Override
            public void onParentExpanded(int parentPosition) {
                Category expandedItem = mCategories.get(parentPosition);
            }

            @UiThread
            @Override
            public void onParentCollapsed(int parentPosition) {
                Category expandedItem = mCategories.get(parentPosition);
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(this,
                mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(mDividerItemDecoration);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mAdapter.onSaveInstanceState(outState);
    }

    @Override
    public void onItemClicked(RecyclerView.ViewHolder holder, int parentPosition, int childPosition,
                              ReservationItem item) {
        mViewHolder = (ReservationChildViewHolder) holder;
        itemToSwapPosition.put(ARG_PARENT_POS, parentPosition);
        itemToSwapPosition.put(ARG_CHILD_POS, childPosition);
        ReservationProductFragment fragment = ReservationProductFragment.newInstance(item);
        FragmentManager fm = getSupportFragmentManager();
        fragment.show(fm, fragment.getClass().getName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ARG_CHECKOUT_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);

                    Intent intent = new Intent(this, CheckoutActivity.class);
                    intent.putExtra(ARG_PAYMENT_NONCE, result.getPaymentMethodNonce().getNonce());
                    intent.putExtra(ARG_PAYMENT_METHOD_TYPE, result.getPaymentMethodType());
                    intent.putExtra(ARG_PAYMENT_METHOD, result.getPaymentMethodType().getDrawable());
                    intent.putExtra(ARG_RESERVATION, reservation);
                    startActivity(intent);
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // the user canceled
                    Log.d(TAG, "Checkout cancelled!");
                } else {
                    // handle errors here, an exception may be available in
                    Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                    Log.e(TAG, "Error creating payment nonce: " + error.getMessage());
                }
        }
    }

    @Override
    public void onProductChanged(ReservationItem oldItem, ReservationItem newItem) {
        Log.d(TAG, "Target Fragment Reached!!!");
        int itemPosition = mReservationItems.indexOf(oldItem);

        mReservationItems.remove(oldItem);
        mReservationItems.add(itemPosition, newItem);

        boolean existingProduct = false;
        for (int i = 0; i < mCategories.size(); i++){
            Category category = mCategories.get(i);
            if (category.getChildList().contains(oldItem)){
                List<ReservationItem> items = category.getChildList();

                for (int j = 0; j < items.size(); j++){
                    ReservationItem item = items.get(j);
                    if (item.getProduct().getId() == newItem.getProduct().getId()){
                        existingProduct = true;
                    }
                }

                if (existingProduct){
                    ReservationItem item = items.get(i);
                    item.setQty(item.getQty() + oldItem.getQty());
                    items.remove(oldItem);
                }else{
                    int pos = items.indexOf(oldItem);
                    items.remove(oldItem);
                    items.add(pos, newItem);
                }
            }
        }

        if (existingProduct){
            mAdapter.notifyChildRemoved(itemToSwapPosition.get(ARG_PARENT_POS),
                    itemToSwapPosition.get(ARG_CHILD_POS));
        }else{
            mAdapter.notifyChildChanged(itemToSwapPosition.get(ARG_PARENT_POS),
                    itemToSwapPosition.get(ARG_CHILD_POS));
        }

        mAdapter.notifyDataSetChanged();
        mViewHolder.productTitle.setText(newItem.getProduct().getName());
        mViewHolder.productQty.setText(String.valueOf(newItem.getQty()));
    }
}


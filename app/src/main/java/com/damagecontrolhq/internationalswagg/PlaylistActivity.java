package com.damagecontrolhq.internationalswagg;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.damagecontrolhq.internationalswagg.adapters.SongAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.models.Song;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaylistActivity extends AppCompatActivity {
    private static final String TAG = PlaylistActivity.class.getName();
    private ArrayList<Song> mSongs = new ArrayList<>();
    private SongAdapter mAdapter;
    private int mIndex;
    private ListView mListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_playlist);

        Intent i = getIntent();
        mSongs = i.getParcelableArrayListExtra(MusicPlayerActivity.SONG_LIST);
        mIndex = i.getIntExtra(MusicPlayerActivity.SONG_INDEX, 0);
        if(mSongs == null || mSongs.size() == 0){
            getPlayList();
        }

        // selecting single ListView item
        mListView = (ListView) findViewById(R.id.playlist);

        // Adding menuItems to ListView
        mAdapter = new SongAdapter(this, mSongs);
        mListView.setAdapter(mAdapter);

        // listening to single list item click
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // Starting new intent
                Intent i = new Intent(PlaylistActivity.this,
                        MusicPlayerActivity.class);
                // Sending songIndex to PlayerActivity
                i.putExtra(MusicPlayerActivity.SONG_LIST, mSongs);
                i.putExtra(MusicPlayerActivity.SONG_INDEX, position);
                setResult(100, i);
                // Closing PlayListView
                finish();
            }
        });
    }


    void getPlayList(){
        SwaggApp.api(false).getMusic().enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String string = response.body().toString();
                try {
                    JSONObject json = new JSONObject(string);
                    JSONArray array = json.getJSONArray("songs");
                    for (int i = 0; i < array.length(); i++){
                        JSONObject data = (JSONObject)array.get(i);
                        Song song = new Song(data);
                        mSongs.add(song);
                    }
                    mAdapter.notifyDataSetChanged();
                }catch (JSONException e){
                    Log.e(TAG, "Error parsing server hash: " + e.toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "Error parsing server hash: " + t.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_music, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_playlist:
                // Tell the media scanner about the new file so that it is
                // immediately available to the user.
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package com.damagecontrolhq.internationalswagg;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.dropin.utils.PaymentMethodType;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.fragments.MyReservationFragment;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.models.Checkout;
import com.damagecontrolhq.internationalswagg.models.Reservation;
import com.damagecontrolhq.internationalswagg.models.ReservationItem;
import com.damagecontrolhq.internationalswagg.models.SaleTransaction;
import com.damagecontrolhq.internationalswagg.models.User;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.damagecontrolhq.internationalswagg.app.SwaggApp.REQUESTED_PAGE;
import static com.damagecontrolhq.internationalswagg.fragments.MyReservationFragment.ARGS_SALE_TRANSACTION_ITEMS;
import static com.damagecontrolhq.internationalswagg.helpers.SessionManager.RESERVATION_ID;

public class CheckoutActivity extends AppCompatActivity {
    public static final String ARG_PAYMENT_NONCE = "payment_nonce",
                                ARG_PAYMENT_METHOD = "payment_method",
                                ARG_PAYMENT_METHOD_TYPE = "payment_method_type",
                                ARG_RESERVATION_BALANCE = "reservation_balance",
                                ARG_SALE_TRANSACTION_ID = "sale_transaction_id",
                                ARG_RESERVATION = "reservation";
    private static final String TAG = CheckoutActivity.class.getName();

    private LinearLayout mVerifyNameContainer;
    private EditText mFirstName, mLastName;
    private RadioGroup mRadioGroup;
    private RadioButton mRadioFull, mRadioRSVP;
    private TextView mFullPrice, mRsvpPrice, mServiceFee, mTotalPrice;
    private ImageView mPaymentMethod, mPackageImage;
    private CheckBox mNameVerificationCb, mTermsCb;
    private Button mConfirmBtn;

    private SessionManager mSession = SwaggApp.getInstance().getSession();
    private Reservation mReservation;
    private String mPaymentNonce;
    private PaymentMethodType mPaymentMethodType;
    private boolean payFull, isBalancePayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        mVerifyNameContainer = (LinearLayout) findViewById(R.id.name_verification_container);
        mFirstName = (EditText) findViewById(R.id.edit_first_name_text);
        mLastName = (EditText) findViewById(R.id.edit_last_name_text);
        mFullPrice = (TextView) findViewById(R.id.tv_full_price);
        mRsvpPrice = (TextView) findViewById(R.id.tv_rsvp_price);
        mServiceFee = (TextView) findViewById(R.id.tv_fee_price);
        mTotalPrice = (TextView) findViewById(R.id.tv_total_price);
        mConfirmBtn = (Button) findViewById(R.id.checkout_btn);
        mPaymentMethod = (ImageView) findViewById(R.id.image_payment_method);
        mPackageImage = (ImageView) findViewById(R.id.package_image);

        mRadioGroup = (RadioGroup) findViewById(R.id.radio_price);
        mRadioFull = (RadioButton) findViewById(R.id.radio_full);
        mRadioRSVP = (RadioButton) findViewById(R.id.radio_rsvp);
        mNameVerificationCb = (CheckBox) findViewById(R.id.verify_name_checkbox);
//        mTermsCb = (CheckBox) findViewById(R.id.accept_terms_checkbox);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Checkout");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Bundle args = getIntent().getExtras();
        mReservation = args.getParcelable(ARG_RESERVATION);
        mPaymentNonce = args.getString(ARG_PAYMENT_NONCE);
        mPaymentMethodType = (PaymentMethodType) args.get(ARG_PAYMENT_METHOD_TYPE);
        isBalancePayment = args.getBoolean(ARG_RESERVATION_BALANCE, false);

        if(mPaymentMethodType != PaymentMethodType.PAY_WITH_VENMO &&
                mPaymentMethodType != PaymentMethodType.PAYPAL){
            mVerifyNameContainer.setVisibility(View.VISIBLE);
        }else{
            mNameVerificationCb.setChecked(true);
        }

        if (isBalancePayment){
            mRadioRSVP.setChecked(true);
            mRadioRSVP.setText("Reservation Balance");
            mRadioFull.setVisibility(View.GONE);
            findViewById(R.id.tv_full_price).setVisibility(View.GONE);
        }

        payFull = mRadioFull.isChecked();


        updateScreen();

        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.radio_full){
                    payFull = true;
                }else if(i == R.id.radio_rsvp){
                    payFull = false;
                }

                updateScreen();
            }
        });

        mNameVerificationCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    mVerifyNameContainer.setBackgroundColor(ContextCompat.getColor(CheckoutActivity.this, R.color.accent_light));
                }
            }
        });


        mConfirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mNameVerificationCb.isChecked() && !TextUtils.isEmpty(mFirstName.getText())
                        && !TextUtils.isEmpty(mLastName.getText())){
                    // Process payment
                    processPayment();
                }else{
                    mVerifyNameContainer.setBackgroundColor(ContextCompat.getColor(CheckoutActivity.this, R.color.bt_error_red));
                }
            }
        });
    }

    private void updateScreen() {
        User user = mSession.getUser();
        Double fullPrice = mReservation.getPrice().getFormattedPrice();
        Double rsvpPrice = fullPrice / 2;
        Double rate = 0.03;
        Double serviceFee = payFull ? fullPrice *rate : rsvpPrice * rate;
        Double total = payFull ? fullPrice + serviceFee : rsvpPrice + serviceFee;

        Picasso.with(this).load(ImageUtils.getImageUrl(mReservation.getPhoto())).into(mPackageImage);
        mFirstName.setText(user.getFirstName());
        mLastName.setText(user.getLastName());
        mRsvpPrice.setText(AppUtils.formatPrice(rsvpPrice));
        mFullPrice.setText(AppUtils.formatPrice(fullPrice));
        mServiceFee.setText(AppUtils.formatPrice(serviceFee));
        mTotalPrice.setText(AppUtils.formatPrice(total));
        mPaymentMethod.setImageDrawable(ContextCompat.getDrawable(this, mPaymentMethodType.getDrawable()));

        String name = mReservation.getName().toLowerCase();
        int bgColor;
        String packageName = getPackageName();
        bgColor = getResources().getIdentifier(name + "_bg", "color", packageName);

        LinearLayout packageCard = (LinearLayout) findViewById(R.id.package_card);
        packageCard.setBackgroundColor(ContextCompat.getColor(this, bgColor));
    }

    private String getLocale(){
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getNetworkCountryIso().toLowerCase();
    }

    void processPayment(){
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Processing payment...", true);
        //you usually don't want the user to stop the current process, and this will make sure of that
        progressDialog.setCancelable(false);
        progressDialog.show();

        ArrayList<HashMap<String, Integer>> items = new ArrayList<>();
        if (mReservation.getReservationItems() != null){
            for (int i = 0; i < mReservation.getReservationItems().size(); i++) {
                ReservationItem item = mReservation.getReservationItems().get(i);
                HashMap<String, Integer> param = new HashMap<>();
                param.put("product_id", item.getProduct().getId());
                param.put("qty", item.getQty());
                items.add(param);
            }
        }

        User user = SwaggApp.getInstance().getSession().getUser();
        Checkout checkout =  new Checkout(user.getID(), user.getFirstName(), user.getLastName(),
                mPaymentNonce, getLocale(), mReservation.getId(), items, payFull);

        SwaggApp.api(true).checkout(checkout).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()){
                    JsonObject json = response.body();
                    Gson gson = new Gson();
                    JsonElement key = json.getAsJsonObject().get("sale_transaction");
                    SaleTransaction transaction = gson.fromJson(key, SaleTransaction.class);
                    mSession.setPref(RESERVATION_ID, transaction.getId());
                    Intent i = new Intent(CheckoutActivity.this, MainActivity.class);
                    i.putExtra(REQUESTED_PAGE, MyReservationFragment.class.getName());
                    i.putExtra(ARGS_SALE_TRANSACTION_ITEMS, transaction.getSaleTransactionItems());
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }else{
                    onBackPressed();
                    Toast.makeText(CheckoutActivity.this, "Error creating transaction", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "Error processing payment" + t.getMessage());
                Toast.makeText(CheckoutActivity.this, "Error processing transaction", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                onBackPressed();
            }
        });
    }
}

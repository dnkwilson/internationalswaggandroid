package com.damagecontrolhq.internationalswagg.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.damagecontrolhq.internationalswagg.models.Affiliate;
import com.damagecontrolhq.internationalswagg.models.Chat;
import com.damagecontrolhq.internationalswagg.models.Event;
import com.damagecontrolhq.internationalswagg.models.EventMedia;
import com.damagecontrolhq.internationalswagg.models.Post;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * InternationalSwagg
 * Created by dwilson on 1/7/16.
 */
public class SwaggDBHelper extends SQLiteOpenHelper {
    private static final String TAG = SwaggDBHelper.class.getName();

    public static final String DATABASE_NAME = "SwaggDB.db";
    private static final int DATABASE_VERSION = 1;

    public static final String ID = "ID";
    public static final String CREATED_AT = "CREATED_AT";
    public static final String EVENT_MEDIA_ID = "event_media_id";
    public static final String POST_ID = "post_id";
    public static final String EVENT_ID = "event_id";

    public static final String TABLE_CHATS = "chats";
    public static final String TABLE_EVENTS = "events";
    public static final String TABLE_POSTS = "posts";
    public static final String TABLE_EVENT_MEDIAS = "event_media";
    public static final String TABLE_AFFILIATES = "affiliates";

    private static final String CREATE_TABLE_CHATS = "CREATE TABLE " + TABLE_CHATS + "(" + ID +
            " INTEGER PRIMARY KEY," + Chat.KEY_BODY  + " TEXT," + Chat.KEY_USER_ID + " INTEGER,"
            + Chat.KEY_USERNAME + " TEXT," + Chat.KEY_MEDIA + " TEXT," + Chat.KEY_DURATION +
            " TEXT," + CREATED_AT + " DATETIME," + Chat.KEY_CATEGORY + " TEXT," + Chat.KEY_LOCAL_URL
            + " TEXT" + ")";

    private static final String CREATE_TABLE_EVENTS = "CREATE TABLE " + TABLE_EVENTS + "(" + ID +
            " INTEGER PRIMARY KEY," + Event.KEY_BODY  + " TEXT," + Event.KEY_NAME + " TEXT,"
            + Event.KEY_PHOTO + " TEXT," + Event.KEY_DATE + " TEXT," + Event.KEY_VIDEOS +
            " TEXT," + EVENT_ID + " INTEGER," + CREATED_AT + " DATETIME" + ")";

    private static final String CREATE_TABLE_EVENT_MEDIAS = "CREATE TABLE " + TABLE_EVENT_MEDIAS + "("
            + ID + " INTEGER PRIMARY KEY," + EventMedia.KEY_NAME  + " TEXT," + EventMedia.KEY_INFO
            + " TEXT," + EventMedia.KEY_MEDIA_TYPE + " TEXT," + EventMedia.KEY_PREVIEW_IMAGE + " TEXT,"
            + EventMedia.KEY_EVENT_ID + " INTEGER," + EVENT_MEDIA_ID + " INTEGER," +
            CREATED_AT + " DATETIME" + ")";

    private static final String CREATE_TABLE_POSTS = "CREATE TABLE " + TABLE_POSTS + "(" + ID +
            " INTEGER PRIMARY KEY," + Post.KEY_BODY  + " TEXT," + Post.KEY_CATEGORY + " TEXT,"
            + Post.KEY_COMMENTS + " TEXT," + Post.KEY_DATE + " DATETIME," + Post.KEY_IMAGE +
            " TEXT," + Post.KEY_TITLE + " TEXT," + Post.KEY_VIDEO + " TEXT," + POST_ID
            + " INTEGER," + Post.KEY_VOTES + " TEXT" + ")";

    private static final String CREATE_TABLE_AFFILIATES = "CREATE TABLE " + TABLE_AFFILIATES + "(" + ID +
            " INTEGER PRIMARY KEY," + Affiliate.KEY_NAME  + " TEXT," + Affiliate.KEY_INFO + " TEXT,"
            + Affiliate.KEY_PHONE + " TEXT," + Affiliate.KEY_IG + " DATETIME," + Affiliate.KEY_TW +
            " TEXT," + Affiliate.KEY_FB + " TEXT," + Affiliate.KEY_EMAIL + " TEXT, " + Affiliate.KEY_LOGO
            + " TEXT" + ")";

    public SwaggDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public boolean createChat(Chat chat){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Chat.KEY_USER_ID, chat.getUserId());
        values.put(Chat.KEY_USERNAME, chat.getUsername());
        values.put(Chat.KEY_BODY, chat.getBody());
        values.put(Chat.KEY_MEDIA, chat.getMedia());
        values.put(Chat.KEY_DURATION, chat.getDuration());
        values.put(Chat.KEY_CATEGORY, chat.getCategory());
        values.put(Chat.KEY_LOCAL_URL, chat.getLocalMedia());
        values.put(CREATED_AT, getDateTime());

        db.insert("chats", null, values);

        return true;
    }

    public boolean createEvent(Event event){
        ContentValues values = new ContentValues();
        values.put(EVENT_ID, event.getId());
        values.put(Event.KEY_NAME, event.getName());
        values.put(Event.KEY_PHOTO, event.getPhoto());
        values.put(Event.KEY_BODY, event.getBody());
        values.put(Event.KEY_DATE, event.dateToSting());
        values.put(CREATED_AT, getDateTime());

        if ( event.getGalleries().size() > 0) createEventMedia(event.getGalleries());
        if ( event.getVideos().size() > 0) createEventMedia(event.getVideos());

        return true;
    }

    public boolean createEventMedia(ArrayList<EventMedia> medias){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (EventMedia media : medias) {
            values.put(EVENT_MEDIA_ID, media.getId());
            values.put(EventMedia.KEY_NAME, media.getName());
            values.put(EventMedia.KEY_INFO, media.getInfo());
            values.put(EventMedia.KEY_EVENT_ID, media.getEventId());
            values.put(EventMedia.KEY_PREVIEW_IMAGE, media.getPreview());
            values.put(EventMedia.KEY_MEDIA_TYPE, media.getType());
            values.put(CREATED_AT, getDateTime());
        }

        // insert row
        db.insert(TABLE_EVENT_MEDIAS, null, values);

        return true;
    }

    public boolean createAffiliate(long event_id, ArrayList<Affiliate> affiliates){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (Affiliate affiliate : affiliates) {
            values.put(Affiliate.KEY_NAME, affiliate.getName());
            values.put(Affiliate.KEY_INFO, affiliate.getInfo());
            values.put(Affiliate.KEY_PHONE, affiliate.getPhone());
            values.put(Affiliate.KEY_IG, affiliate.getInstagram());
            values.put(Affiliate.KEY_TW, affiliate.getTwitter());
            values.put(Affiliate.KEY_FB, affiliate.getFacebook());
            values.put(Affiliate.KEY_EMAIL, affiliate.getEmail());
            values.put(Affiliate.KEY_LOGO, affiliate.getLogo());
        }

        // insert row
        db.insert(TABLE_AFFILIATES, null, values);

        return true;
    }


    public Event getEvent(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "select * from " + TABLE_EVENTS + " where id="+id+"";
        Log.i(TAG, query);

        Cursor c =  db.rawQuery(query, null);
        if (c != null)
            c.moveToFirst();

        Event e = new Event();
        e.setId(c.getInt(c.getColumnIndex(EVENT_ID)));
        e.setBody(c.getString(c.getColumnIndex(Event.KEY_BODY)));
        e.setName(c.getString(c.getColumnIndex(Event.KEY_NAME)));
        e.setPhoto(c.getString(c.getColumnIndex(Event.KEY_PHOTO)));
        e.setDate(c.getString(c.getColumnIndex(Event.KEY_DATE)));

        e.setGalleries(getAllEventMedias(c.getInt(c.getColumnIndex(ID)),
                EventMedia.IMAGE_MEDIA_TYPE));
        e.setVideos(getAllEventMedias(c.getInt(c.getColumnIndex(ID)),
                EventMedia.VIDEO_MEDIA_TYPE));
        return e;
    }

    public EventMedia getEventMedia(int id){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "select * from " + TABLE_EVENT_MEDIAS + " where id="+id+"";
        Log.i(TAG, query);

        Cursor c =  db.rawQuery(query, null );
        if (c != null)
            c.moveToFirst();

        EventMedia media = new EventMedia();
        media.setId(c.getInt(c.getColumnIndex(EVENT_MEDIA_ID)));
        media.setEventId(String.valueOf(c.getInt(c.getColumnIndex(EventMedia.KEY_EVENT_ID))));
        media.setName(c.getString(c.getColumnIndex(EventMedia.KEY_NAME)));
        media.setInfo(c.getString(c.getColumnIndex(EventMedia.KEY_INFO)));
        media.setPreview(c.getString(c.getColumnIndex(EventMedia.KEY_PREVIEW_IMAGE)));
        media.setType(c.getString(c.getColumnIndex(EventMedia.KEY_MEDIA_TYPE)));

        return media;
    }

    public Integer deleteObject(Integer id, String str){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(str, "id = ? ", new String[] { Integer.toString(id) });
    }


    public ArrayList<Event> getAllEvents(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_EVENTS;
        Log.i(TAG, query);

        Cursor c = db.rawQuery(query, null);

        ArrayList<Event> events = new ArrayList<>();

        if (c.moveToFirst()){
            do {
                Event event = new Event();
                event.setId(c.getInt(c.getColumnIndex(EVENT_ID)));
                event.setBody(c.getString(c.getColumnIndex(Event.KEY_BODY)));
                event.setPhoto(c.getString(c.getColumnIndex(Event.KEY_PHOTO)));
                event.setName(c.getString(c.getColumnIndex(Event.KEY_NAME)));
                event.setDate(c.getString(c.getColumnIndex(Event.KEY_DATE)));
                event.setGalleries(getAllEventMedias(c.getInt(c.getColumnIndex(EVENT_ID)),
                        EventMedia.IMAGE_MEDIA_TYPE));
                event.setVideos(getAllEventMedias(c.getInt(c.getColumnIndex(EVENT_ID)),
                        EventMedia.VIDEO_MEDIA_TYPE));
                events.add(event);
            } while (c.moveToNext());
        }

        return events;
    }

    public ArrayList<EventMedia> getAllEventMedias(long event_id, String type){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT  * FROM " + TABLE_EVENT_MEDIAS + " WHERE " + EventMedia.KEY_EVENT_ID +
                " = " + event_id + " AND " + EventMedia.KEY_MEDIA_TYPE + " = "
                + "'" + type + "'";
        Log.e(TAG, query);

        Cursor c = db.rawQuery(query, null);

        ArrayList<EventMedia> eventMedia = new ArrayList<>();

        if (c.moveToFirst()){
            do {
                EventMedia media = new EventMedia();
                media.setId(c.getInt(c.getColumnIndex(EVENT_MEDIA_ID)));
                media.setType(type);
                media.setInfo(c.getString(c.getColumnIndex(EventMedia.KEY_INFO)));
                media.setPreview(c.getString(c.getColumnIndex(EventMedia.KEY_PREVIEW_IMAGE)));
                media.setName(c.getString(c.getColumnIndex(EventMedia.KEY_NAME)));
                media.setEventId(String.valueOf(c.getInt(c.getColumnIndex(EVENT_ID))));
                eventMedia.add(media);
            } while (c.moveToNext());
        }

        return eventMedia;
    }

    public ArrayList<Chat> getAllChats(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT  * FROM " + TABLE_CHATS;

        Log.e(TAG, query);

        Cursor c = db.rawQuery(query, null);

        ArrayList<Chat> chats = new ArrayList<>();

        if (c.moveToFirst()){
            do {
                Chat chat = new Chat();
                chat.setBody(c.getString(c.getColumnIndex(Chat.KEY_BODY)));
                chat.setLocalMedia(c.getString(c.getColumnIndex(Chat.KEY_LOCAL_URL)));
                chat.setTime(c.getString(c.getColumnIndex(CREATED_AT)));
                chat.setMedia(c.getString(c.getColumnIndex(Chat.KEY_MEDIA)));
                chat.setUserId(c.getInt(c.getColumnIndex(Chat.KEY_USER_ID)));
                chat.setUsername(c.getString(c.getColumnIndex(Chat.KEY_USERNAME)));
                chat.setDuration(c.getInt(c.getColumnIndex(Chat.KEY_DURATION)));
                chat.setCategory(c.getString(c.getColumnIndex(Chat.KEY_CATEGORY)));
                chats.add(chat);
            } while (c.moveToNext());
        }

        return chats;
    }

    public ArrayList<Affiliate> getAllAffiliates(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_AFFILIATES;
        Log.i(TAG, query);

        Cursor c = db.rawQuery(query, null);

        ArrayList<Affiliate> affiliates = new ArrayList<>();

        if (c.moveToFirst()){
            do {
                Affiliate affiliate = new Affiliate();
                affiliate.setName(c.getString(c.getColumnIndex(Affiliate.KEY_NAME)));
                affiliate.setInfo(c.getString(c.getColumnIndex(Affiliate.KEY_INFO)));
                affiliate.setPhone(c.getString(c.getColumnIndex(Affiliate.KEY_PHONE)));
                affiliate.setInstagram(c.getString(c.getColumnIndex(Affiliate.KEY_IG)));
                affiliate.setTwitter(c.getString(c.getColumnIndex(Affiliate.KEY_TW)));
                affiliate.setFacebook(c.getString(c.getColumnIndex(Affiliate.KEY_FB)));
                affiliate.setEmail(c.getString(c.getColumnIndex(Affiliate.KEY_EMAIL)));
                affiliate.setLogo(c.getString(c.getColumnIndex(Affiliate.KEY_LOGO)));
                affiliates.add(affiliate);
            } while (c.moveToNext());
        }

        return affiliates;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CHATS);
        db.execSQL(CREATE_TABLE_POSTS);
        db.execSQL(CREATE_TABLE_EVENTS);
        db.execSQL(CREATE_TABLE_EVENT_MEDIAS);
        db.execSQL(CREATE_TABLE_AFFILIATES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POSTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHATS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENT_MEDIAS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AFFILIATES);

        // create new tables
        onCreate(db);
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
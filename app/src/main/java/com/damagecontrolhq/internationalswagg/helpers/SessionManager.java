package com.damagecontrolhq.internationalswagg.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.TimeZone;

/**
 * InternationalSwagg
 * Created by dwilson on 9/30/15.
 */
public class SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    public static final String  FB = "facebook",
                                GOOGLE = "google",
                                BRAINTREE_TOKEN = "braintree_token",
                                RESERVATION_ID = "reservation_id";
    private static final String LOGIN_KEY = "login_key";

    // Shared Preferences
    private SharedPreferences pref;

    private Editor editor;

    // Shared preferences file name
    private static final String PREF_NAME = "InternationalSwagg";

    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    public static final String  KEY_ALLOW_PUSH = "allow_push",
                                KEY_FIREBASE_TOKEN = "firebase_token";

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        int PRIVATE_MODE = 0;
        this.pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.editor = pref.edit();
    }

    /**
     * Create user session
     * */
    public void createSession(User user){
        // Store user info
        editor.putInt(User.KEY_ID, user.getID());
        editor.putString(User.KEY_EMAIL, user.getEmail());
        editor.putString(User.KEY_UID, user.getUID());
        editor.putString(User.KEY_FIRST_NAME, user.getFirstName());
        editor.putString(User.KEY_LAST_NAME, user.getLastName());
        editor.putString(User.KEY_GENDER, user.getGender());
        editor.putString(User.KEY_PHONE, user.getPhone());
        editor.putString(User.KEY_CITY, user.getCity());
        editor.putString(User.KEY_HOMETOWN, user.getHomeTown());
        editor.putString(User.KEY_COUNTRY, user.getCountry());
        editor.putString(User.KEY_DOB, user.getDOB());
        editor.putString(User.KEY_USERNAME, user.getUserName());
        editor.putString(User.KEY_PROVIDER, user.getProvider());
        editor.putString(User.KEY_TOKEN, user.getToken());
        editor.putString(User.KEY_PROFILE_PIC, user.getProfilePic());
        editor.putString("time_zone", TimeZone.getDefault().getID());

//        setParseId(true);

        // commit changes
        editor.apply();
    }

    public void setFirebaseToken(String token){
        editor.putString(KEY_FIREBASE_TOKEN, token);
        editor.putBoolean(KEY_ALLOW_PUSH, !token.isEmpty());
        editor.apply();
    }

    public void setPushNotifiable(boolean notify){
        editor.putBoolean(KEY_ALLOW_PUSH, notify).apply();
    }

    public boolean getPushNotifiable(){
        return pref.getBoolean(KEY_ALLOW_PUSH, true);
    }

    public String getFirebaseToken(){
        return pref.getString(KEY_FIREBASE_TOKEN, null);
    }

    public void createLogin(String userId, String token){
        editor.putString(User.KEY_UID, userId);
        editor.putString(LOGIN_KEY, token);
        editor.putString(User.KEY_PROVIDER, SessionManager.FB);
        editor.apply();
    }

    public JSONObject getLogin(){
        JSONObject json = new JSONObject();
        try {
            json.put(User.KEY_UID, getLoginUid());
            json.put(User.KEY_PROVIDER, getPref(User.KEY_PROVIDER));
            json.put(KEY_FIREBASE_TOKEN, SwaggApp.getInstance().getSession().getFirebaseToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * Clear user session
     * */
    public void clearSession(){
        // Clearing all data from Shared Preferences
        editor.putString(LOGIN_KEY, null);
        editor.putString(User.KEY_TOKEN, null);
        editor.putString(User.KEY_PROVIDER, null);
        editor.putString(User.KEY_UID, null);
        editor.putInt(RESERVATION_ID, 0);
        editor.putString(BRAINTREE_TOKEN, null);
        editor.apply();
    }

    public String getPref(String key){
        return pref.getString(key, null);
    }

    public void setPref(String key, String value){
        editor.putString(key, value).apply();
    }

    public int getPref(String key, int value){
        return pref.getInt(key, value);
    }

    public void setPref(String key, int value){
        editor.putInt(key, value).apply();
    }

    public String getLoginProvider(){
        return pref.getString(User.KEY_PROVIDER, null);
    }

    public void setLoginProvider(){
        editor.putString(User.KEY_PROVIDER, SessionManager.GOOGLE).apply();
    }

    public String getLoginUid(){
        return pref.getString(User.KEY_UID, null);
    }

    public void setLoginUid(String provider){
        editor.putString(User.KEY_UID, provider).apply();
    }

    public String getLoginToken(){
        return pref.getString(LOGIN_KEY, null);
    }

    public void setLoginToken(String provider){
        editor.putString(LOGIN_KEY, provider).apply();
    }

    public String getUserToken(){
        return pref.getString(User.KEY_TOKEN, null);
    }

    public boolean isLoggedIn(){
        return getUserToken() != null;
    }

    public User getUser() {
        return new User(pref);
    }
}
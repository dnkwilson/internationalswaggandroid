package com.damagecontrolhq.internationalswagg.helpers;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}

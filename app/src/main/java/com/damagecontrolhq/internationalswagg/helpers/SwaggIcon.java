package com.damagecontrolhq.internationalswagg.helpers;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

/**
 * InternationalSwagg
 * Created by dwilson on 10/1/15.
 */
public class SwaggIcon {
    private Drawable drawable;
    public SwaggIcon(){
        this.drawable = new IconicsDrawable(SwaggApp.getInstance())
                        .icon(FontAwesome.Icon.valueOf("faw_play_circle"))
                        .color(ContextCompat.getColor(SwaggApp.getInstance(), com.damagecontrolhq.internationalswagg.R.color.accent_dark))
                        .sizeDp(24);
    }

    public Drawable getDrawable() {
        return drawable;
    }
}

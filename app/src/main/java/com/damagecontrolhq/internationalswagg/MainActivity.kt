package com.damagecontrolhq.internationalswagg

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.view.LayoutInflaterCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.basecamp.turbolinks.TurbolinksAdapter
import com.basecamp.turbolinks.TurbolinksSession
import com.damagecontrolhq.internationalswagg.fragments.ChatFragment
import com.damagecontrolhq.internationalswagg.fragments.DashboardFragment
import com.damagecontrolhq.internationalswagg.helpers.SessionManager
import com.mikepenz.iconics.context.IconicsLayoutInflater
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), TurbolinksAdapter, NavigationView.OnNavigationItemSelectedListener {
    private var mSession: SessionManager? = null
    private val mAvatar: ImageView? = null
    private var mCoordinator: CoordinatorLayout? = null

    companion object {
        private val TAG = "MainActivity"

        private val INTENT_URL = "intentUrl"
        private val REQUEST_PERMISSIONS = 1225
        private val PERMISSIONS_NEED = arrayOf(android.Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    private fun getURL() : String {
        return if (intent.getStringExtra(MainActivity.INTENT_URL) != null)  {
            intent.getStringExtra(MainActivity.INTENT_URL)
        }else {
            BuildConfig.HOST
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        LayoutInflaterCompat.setFactory(layoutInflater, IconicsLayoutInflater(delegate))
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        val mToolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(mToolbar)
        supportInvalidateOptionsMenu()

        mCoordinator = findViewById(R.id.coordinatorLayout)

        val mDrawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        mDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()
//        supportActionBar?.setDisplayHomeAsUpEnabled(false)
//        supportActionBar?.setHomeButtonEnabled(false)

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)


        TurbolinksSession
                .getDefault(this)
                .activity(this)
                .adapter(this)
                .view(container)
                .visit(getURL())

//        if (savedInstanceState == null) {
//            val fragmentManager = supportFragmentManager
//            val intent = intent
//            var requestedPage: String? = intent.getStringExtra(SwaggApp.REQUESTED_PAGE)
//            val checkLogin = intent.getBooleanExtra(SwaggApp.CHECK_LOGIN, false)
//
//            mSession = SwaggApp.getInstance().session
//
//            if (checkLogin) {
//                // Make sure we have a mUsername
//                SwaggApp.getInstance().checkLogin()
//                mSession = SwaggApp.getInstance().session
//            }
//
//            // Toggle menu
//            if (mSession!!.getPref(RESERVATION_ID, 0) != 0) {
//                navigationView.menu.clear()
//                navigationView.inflateMenu(R.menu.menu_drawer_reservation)
//            } else {
//                navigationView.menu.clear()
//                navigationView.inflateMenu(R.menu.menu_drawer)
//            }
//
//            val action = intent.action
//            val type = intent.type
//            if (Intent.ACTION_SEND == action && type != null) requestedPage = ChatFragment::class.java.name
//
//            if (requestedPage != null) {
//                if (requestedPage == ChatFragment::class.java.name) {
//                    openChatRoom()
//                } else if (requestedPage == ReservationItemActivity::class.java.toString()) {
//                    val i = Intent(this, ReservationItemActivity::class.java)
//                    startActivity(i)
//                    finish()
//                } else {
//                    val fragment = Fragment.instantiate(this, requestedPage)
//                    fragmentManager.beginTransaction().replace(R.id.container, fragment,
//                            fragment.javaClass.toString()).commit()
//                }
//            } else {
//                fragmentManager.beginTransaction().replace(R.id.container, DashboardFragment(),
//                        DashboardFragment::class.java.toString()).commit()
//            }
//        }
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            val fragment = supportFragmentManager.findFragmentById(R.id.container)
            if (fragment is DashboardFragment) {
                finish()
            } else {
                super.onBackPressed()
            }
        }
    }

    private fun isCurrentFragment(frag: String): Boolean {
        val fragment = Fragment.instantiate(this, frag)
        val currentFragment = supportFragmentManager.findFragmentById(R.id.container)
        return currentFragment != null && fragment.javaClass.toString() == currentFragment.tag
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // update the main content by replacing fragments
//        var fragment: Fragment? = null
        when (item.itemId) {
            //            case R.id.nav_connect:
            //                fragment =  new SocialFeedFragment();
            //                setTitle("Instagram Feed");
            //                break;
//            R.id.nav_reserve -> fragment = ReservationFragment()
//            R.id.nav_my_reservation -> fragment = MyReservationFragment()
            R.id.nav_music -> {
                val i = Intent(this, MusicPlayerActivity::class.java)
                startActivity(i)
            }
            R.id.nav_box_tv -> startActivity(Intent(this, BoxTvActivity::class.java))
//            R.id.nav_chat -> fragment = ChatFragment()
//            R.id.nav_events -> {
//                fragment = EventFragment()
//                title = "Events"
//            }
//            R.id.nav_cares -> {
//                fragment = CharityFragment()
//                title = "DC Cares"
//            }
            //            case R.id.nav_feed:
            //                fragment = new FeedFragment();
            //                setTitle("The Feed");
            //                break;
//            R.id.nav_affiliates -> {
//                fragment = AffiliatesFragment()
//                title = "Affiliates"
//            }
            else -> startActivity(Intent(this, BoxTvActivity::class.java))
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)

//        if (fragment != null) {
//            if (isCurrentFragment(fragment.javaClass.name)) {
//                drawer.closeDrawers()
//                return false
//            }
//
//            val fragmentManager = supportFragmentManager
//            if (fragment is ChatFragment) {
//                openChatRoom()
//            } else {
//                fragmentManager.beginTransaction().replace(R.id.container, fragment, fragment.javaClass.toString()).commit()
//            }
//        }

        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun openChatRoom() {
        val pm = packageManager
        val audioPermissions = pm.checkPermission(android.Manifest.permission.RECORD_AUDIO,
                packageName)
        val storagePermissions = pm.checkPermission(
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE, packageName)
        val hasRecordAudioPermissions = audioPermissions == PackageManager.PERMISSION_GRANTED
        val hasStoragePermissions = storagePermissions == PackageManager.PERMISSION_GRANTED

        if (hasRecordAudioPermissions && hasStoragePermissions) {
            supportFragmentManager.beginTransaction().replace(R.id.container,
                    ChatFragment(), ChatFragment::class.java.toString()).commit()
        }

        if (!hasRecordAudioPermissions) {
            showPermissionRationale(R.string.permission_chat_denied, PERMISSIONS_NEED,
                    REQUEST_PERMISSIONS)
        }
        if (!hasStoragePermissions) {
            showPermissionRationale(R.string.permission_chat_denied, PERMISSIONS_NEED,
                    REQUEST_PERMISSIONS)
        }
    }

    private fun showPermissionRationale(messageResId: Int, permission: Array<String>, result: Int) {
        val snackbar = Snackbar.make(mCoordinator!!, messageResId, Snackbar.LENGTH_INDEFINITE)
                .setAction("SHOW") { ActivityCompat.requestPermissions(this@MainActivity, permission, result) }
        // Changing message text color
        snackbar.setActionTextColor(Color.WHITE)

        // Changing action button text color
        val sbView = snackbar.view
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.accent))
        val textView = sbView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(this, R.color.black))
        snackbar.show()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSIONS) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                supportFragmentManager.beginTransaction().replace(R.id.container,
                        ChatFragment(), ChatFragment::class.java.toString()).commit()
            } else {
                showPermissionRationale(R.string.permission_chat_denied, PERMISSIONS_NEED, REQUEST_PERMISSIONS)
            }
        }

    }

    override fun onRestart() {
        super.onRestart()

        // Since the webView is shared between activities, we need to tell Turbolinks
        // to load the location from the previous activity upon restarting
        TurbolinksSession.getDefault(this)
                .activity(this)
                .adapter(this)
                .restoreWithCachedSnapshot(true)
                .view(container)
                .visit(BuildConfig.BASE_URL)
    }

    override fun onPageFinished() {
    }

    override fun pageInvalidated() {
    }

    override fun onReceivedError(errorCode: Int) {
    }

    override fun visitCompleted() {
    }

    override fun requestFailedWithStatusCode(statusCode: Int) {
    }

    // The starting point for any href clicked inside a Turbolinks enabled site. In a simple case
    // you can just open another activity, or in more complex cases, this would be a good spot for
    // routing logic to take you to the right place within your app.
    override fun visitProposedToLocationWithAction(location: String?, action: String?) {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra(MainActivity.INTENT_URL, location)
        this.startActivity(intent)
    }

    // -----------------------------------------------------------------------
    // Private
    // -----------------------------------------------------------------------

    // Simply forwards to an error page, but you could alternatively show your own native screen
    // or do whatever other kind of error handling you want.
    private fun handleError(code: Int) {
        if (code == 404) {
            TurbolinksSession.getDefault(this)
                    .activity(this)
                    .adapter(this)
                    .restoreWithCachedSnapshot(false)
                    .view(container)
                    .visit("${BuildConfig.BASE_URL}/error")
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onSaveInstanceState(outState: Bundle) {}
}

package com.damagecontrolhq.internationalswagg;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.damagecontrolhq.internationalswagg.adapters.PostListAdapter;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.interfaces.RecyclerItemClickListener;
import com.damagecontrolhq.internationalswagg.models.Post;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BoxTvActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private List<Post> mPosts = new ArrayList<>();
    private PostListAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    private void getPosts() {
        SwaggApp.api(true).getPosts("video").enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                parseJSON(response.body());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    /**
     * Parsing json response and passing the data to feed view list adapter
     * */
    private void parseJSON(JsonObject response) {
        try {
            mPosts.clear();

            String string = response.getAsJsonArray("posts").toString();
            JSONArray posts = new JSONArray(string);
            for (int i = 0; i < posts.length(); i++) {
                JSONObject json = (JSONObject) posts.get(i);
                Post post = new Post(json);
                mPosts.add(post);
            }
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {  e.printStackTrace(); }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_tv);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("BOX TV");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        getPosts();
        mAdapter = new PostListAdapter(this, mPosts);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onRefresh() {
        getPosts();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPosts();
        mAdapter.notifyDataSetChanged();
    }
}

package com.damagecontrolhq.internationalswagg.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dwilson on 2/11/17.
 */
public class ProductList {
    @SerializedName(value="products")
    public ArrayList<Product> products;

    public ProductList(){}
}

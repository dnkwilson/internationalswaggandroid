package com.damagecontrolhq.internationalswagg.models;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 9/12/15.
 */
@SuppressWarnings("ImplicitArrayToString")
public class InstagramPost {
    private static final String TAG = "InstagramPost";

    private static final String KEY_ID = "id";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_VIDEO = "video";
    private static final String KEY_LIKES = "likes";
    private static final String KEY_LINK = "link";
    private static final String KEY_COMMENTS = "comments";
    private static final String KEY_TYPE = "type";
    private static final String KEY_CAPTION = "caption";
    private static final String KEY_CREATED_AT = "created_at";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PROFILE_PIC = "profile_pic";
    private static final String KEY_IS_VIDEO = "is_video";

    private String mId;
    private String mCaption;
    private String mImage;
    private String mVideo;
    private String mLink;
    private String mComments;
    private String mLikes;
    private String mType;
    private String mCreatedAt;
    private String mUsername;
    private String mProfilePic;
    private boolean mIsVideo;

    public InstagramPost(JSONObject json){
        try{
            this.mId  = json.getString(KEY_ID);
//            this.mCaption = json.getString(KEY_CAPTION);
//            this.mComments  = json.getString(KEY_COMMENTS);
//            this.mLikes = json.getString(KEY_LIKES);
//            this.mType  = json.getString(KEY_TYPE);
//            this.mCreatedAt = json.getString(KEY_CREATED_AT);
//            this.mUsername  = json.getString(KEY_USERNAME);
//            this.mProfilePic = json.getString(KEY_PROFILE_PIC);
//            this.mVideo = json.getString(KEY_VIDEO);
            this.mImage = json.getString(KEY_IMAGE);
            this.mLink  = json.getString(KEY_LINK);
            this.mIsVideo = json.getBoolean(KEY_IS_VIDEO);
        }catch (JSONException e){
            Log.e(TAG, "Error creating Instagram object: " + e.toString());
        }
    }

    public String getId() {
        return mId;
    }

    public String getImage() {
        return mImage;
    }

    public String getVideo() {
        return mVideo;
    }

    public boolean getIsVideo() {
        return mIsVideo;
    }

    public String getLink() {
        return mLink;
    }
}

package com.damagecontrolhq.internationalswagg.models;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dwilson on 1/31/17.
 */

public class Category  implements Parent<ReservationItem>{

    private int id;
    private String name;
    private ArrayList<ReservationItem> reservationItems;

    public Category(ReservationItem item){
        this.id = item.getCategoryId();
        this.name = item.getProduct().getCategoryName();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addReservationItem(ReservationItem item){
        if (item.getCategoryId() == this.id){
            if (reservationItems == null) reservationItems = new ArrayList<>();
            reservationItems.add(item);
        }
    }

    public int getItemCount(){
        int total = 0;

        for (int i = 0; i < reservationItems.size(); i++) {
            total += reservationItems.get(i).getQty();
        }

        return total;
    }

    @Override
    public List<ReservationItem> getChildList() {
        return (reservationItems == null) ? new ArrayList<ReservationItem>() : reservationItems;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}

package com.damagecontrolhq.internationalswagg.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 1/11/16.
 */
public class Affiliate {
    public static final String TAG = Affiliate.class.getName();

    public static final String KEY_NAME = "name";
    public static final String KEY_INFO = "info";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_IG = "instagram";
    public static final String KEY_TW = "twitter";
    public static final String KEY_FB = "facebook";
    public static final String KEY_FB_ID = "facebook_id";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_LOGO = "logo";

    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("info")
    private String info;
    @Expose
    @SerializedName("phone")
    private String phone;
    @Expose
    @SerializedName("instagram")
    private String instagram;
    @Expose
    @SerializedName("twitter")
    private String twitter;
    @Expose
    @SerializedName("facebook")
    private String facebook;
    @Expose
    @SerializedName("facebook_id")
    private String facebookId;
    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("logo")
    private String logo;

    public Affiliate(){}

    public Affiliate(JSONObject json){
        this.name = json.optString(KEY_NAME);
        this.info = json.optString(KEY_INFO);
        this.phone = json.optString(KEY_PHONE);
        this.instagram = json.optString(KEY_IG);
        this.twitter = json.optString(KEY_TW);
        this.facebook = json.optString(KEY_FB);
        this.facebookId = json.optString(KEY_FB_ID);
        this.email = json.optString(KEY_EMAIL);
        this.logo = json.optString(KEY_LOGO);
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public String getPhone() {
        return phone;
    }

    public String getInstagram() {
        return instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public String getEmail() {
        return email;
    }

    public String getLogo() {
        return logo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}

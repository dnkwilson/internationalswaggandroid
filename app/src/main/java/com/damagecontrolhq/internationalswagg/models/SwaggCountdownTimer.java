package com.damagecontrolhq.internationalswagg.models;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * InternationalSwagg
 * Created by dwilson on 8/25/15.
 */
public class SwaggCountdownTimer {
    private static final String TAG = "SwaggCountdownTimer";
    private long intervalMillis;


    public static Calendar nextEaster() {
        Calendar today = new GregorianCalendar();
        int currentYear = today.get(Calendar.YEAR);
        int year = (today.get(Calendar.MONTH) >= 0) ? currentYear : currentYear + 1;
        int Y = year;
        int a = Y % 19;
        int b = Y / 100;
        int c = Y % 100;
        int d = b / 4;
        int e = b % 4;
        int f = (b + 8) / 25;
        int g = (b - f + 1) / 3;
        int h = (19 * a + b - d - g + 15) % 30;
        int i = c / 4;
        int k = c % 4;
        int L = (32 + 2 * e + 2 * i - h - k) % 7;
        int m = (a + 11 * h + 22 * L) / 451;
        int month = (h + L - 7 * m + 114) / 31;
        int day = ((h + L - 7 * m + 114) % 31) + 1;
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, day - 2);
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Log.d(TAG, "Good Friday is " + calendar.getTime());
        return calendar;
    }

//    public SwaggCountdownTimer(int second, int minute, int hour, int monthDay, int month, int year) {
    public SwaggCountdownTimer() {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");

//        GregorianCalendar futureTime = new GregorianCalendar(timeZone);
        GregorianCalendar futureTime = (GregorianCalendar) nextEaster();
//        futureTime.setTimeZone(timeZone);

        // Set date to future time
//        futureTime.set(year, month, monthDay, hour, minute, second);
        long futureMillis = futureTime.getTimeInMillis();

        GregorianCalendar rightNow = new GregorianCalendar();

        // Set date to current time
        long nowMillis = rightNow.getTimeInMillis();

        SimpleDateFormat future = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.US);
        future.setTimeZone(TimeZone.getTimeZone("Jamaica"));
        future.setCalendar(futureTime);

        SimpleDateFormat now = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.US);
        now.setTimeZone(TimeZone.getDefault());
        now.setCalendar(rightNow);

        Log.d(TAG, "Current date: " + now.format(rightNow.getTime()));
        Log.d(TAG, "End date: " + future.format(futureTime.getTime()));

        // Subtract current milliseconds time from future milliseconds time to retrieve interval
        intervalMillis = futureMillis - nowMillis;
    }

    public long getIntervalMillis() {
        return intervalMillis;
    }
}

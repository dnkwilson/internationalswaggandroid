package com.damagecontrolhq.internationalswagg.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dwilson on 1/28/17.
 */

public class Reservation implements Parcelable {
    public static final String  KEY_ID = "id",
                                KEY_NAME = "name",
                                KEY_PHOTO = "photo",
                                KEY_PRICE = "price",
                                KEY_RESERVATION_ITEMS = "package_items";
    @Expose
    @SerializedName("id")
    private int mId;
    @Expose
    @SerializedName("name")
    private String mName;
    @Expose
    @SerializedName("photo")
    private String mPhoto;
    @Expose
    @SerializedName("package_items")
    private ArrayList<ReservationItem> mReservationItems;
    @Expose
    @SerializedName("price")
    private Price mPrice;

    public Reservation(){}

    public int getId() {
        return mId;
    }

    public Price getPrice() {
        return mPrice;
    }

    public ArrayList<ReservationItem> getReservationItems() {
        return mReservationItems;
    }

    public ArrayList<Product> getProducts() {
        ArrayList<Product> products = new ArrayList<>();
        for (int i = 0; i < mReservationItems.size(); i++) {
            ReservationItem item = mReservationItems.get(i);
            products.add(item.getProduct());
        }
        return products;
    }

    public ArrayList<Category> getCategories() {
        ArrayList<Category> categories = new ArrayList<>();
        for (int i = 0; i < mReservationItems.size(); i++) {
            ReservationItem reservationItem = mReservationItems.get(i);

            Category category = null;
            boolean categoryExists = false;
            for (int j = 0; j < categories.size(); j++) {
                if (categories.get(j).getId() == reservationItem.getCategoryId()) {
                    categoryExists = true;
                    category = categories.get(j);
                }
            }

            if (!categoryExists){
                category = new Category(reservationItem);
                categories.add(category);
            }

            category.addReservationItem(reservationItem);

        }
        return categories;
    }

    public String getPhoto() {
        return mPhoto;
    }

    public String getName() {
        return mName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeString(this.mName);
        dest.writeString(this.mPhoto);
        dest.writeTypedList(this.mReservationItems);
        dest.writeParcelable(this.mPrice, flags);
    }

    protected Reservation(Parcel in) {
        this.mId = in.readInt();
        this.mName = in.readString();
        this.mPhoto = in.readString();
        this.mReservationItems = in.createTypedArrayList(ReservationItem.CREATOR);
        this.mPrice = in.readParcelable(Price.class.getClassLoader());
    }

    public static final Parcelable.Creator<Reservation> CREATOR = new Parcelable.Creator<Reservation>() {
        @Override
        public Reservation createFromParcel(Parcel source) {
            return new Reservation(source);
        }

        @Override
        public Reservation[] newArray(int size) {
            return new Reservation[size];
        }
    };
}

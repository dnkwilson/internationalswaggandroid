package com.damagecontrolhq.internationalswagg.models;

import android.content.SharedPreferences;
import android.util.Log;

import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * InternationalSwagg
 * Created by dwilson on 9/17/15.
 */
public class User {
    private static final String TAG = "User";

    public static final String KEY_EMAIL      = "email";
    public static final String KEY_ID         = "id";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME  = "last_name";
    public static final String KEY_PROFILE_PIC = "profile_pic";
    public static final String KEY_GENDER      = "gender";
    public static final String KEY_DOB        = "dob";
    public static final String KEY_USERNAME   = "username";
    public static final String KEY_PHONE      = "phone";
    public static final String KEY_HOMETOWN   = "hometown";
    public static final String KEY_CITY       = "city";
    public static final String KEY_COUNTRY    = "country";
    public static final String KEY_UID        = "uid";
    public static final String KEY_PROVIDER   = "provider";
    public static final String KEY_TOKEN      = "token";
    public static final String KEY_USER_ID    = "user_id";

    private int mID;
    private String mEmail;
    private String mFirstName;
    private String mLastName;
    private String mGender;
    private String mDOB;
    private String mUserName;
    private String mHomeTown;
    private String mCity;
    private String mCountry;
    private String mProvider;
    private String mUID;
    private String mToken;
    private String mProfilePic;
    private String mPhone;

    public User(String first_name, String last_name, String gender, String username, String email,
                String token){
        this.mFirstName = first_name;
        this.mLastName  = last_name;
        this.mGender    = gender;
        this.mUserName  = username;
        this.mGender    = gender;
        this.mEmail     = email;
        this.mToken     = token;
    }

    public User(JSONObject json){
        try {
            this.mID        = json.getInt(KEY_ID);
            this.mFirstName = json.getString(KEY_FIRST_NAME);
            this.mLastName  = json.getString(KEY_LAST_NAME);
            this.mGender    = json.getString(KEY_GENDER);
            this.mUserName  = json.getString(KEY_USERNAME);
            this.mHomeTown  = json.getString(KEY_HOMETOWN);
            this.mPhone     = json.getString(KEY_PHONE);
            this.mCity      = json.getString(KEY_CITY);
            this.mProfilePic  = json.getString(KEY_PROFILE_PIC);
            User currentUser = SwaggApp.getInstance().getSession().getUser();
            if((json.getInt(KEY_ID) == currentUser.getID()) || currentUser.getToken() == null){
                this.mEmail     = json.getString(KEY_EMAIL);
                this.mUID       = json.getString(KEY_UID);
                this.mToken = json.getString(KEY_TOKEN);
                this.mProvider  = json.getString(KEY_PROVIDER);
            }
        }catch (JSONException e){
            Log.e(TAG, "Error creating user from server hash: " + e.toString());
        }
    }

    public User(Person person, String email){
        this.mEmail     = email;
        this.mFirstName = person.getName().getGivenName();
        this.mLastName  = person.getName().getFamilyName();
        this.mGender    = parseGender(person.getGender());
        this.mUserName  = person.getNickname();
        this.mHomeTown  = parseHometown(person.getPlacesLived());
        this.mCity      = person.getCurrentLocation();
        this.mUID       = person.getId();
        this.mDOB        = person.getBirthday();
        this.mProfilePic = person.getImage().getUrl();
    }

    public User(SharedPreferences pref){
        this.mID        = pref.getInt(KEY_ID, 0);
        this.mEmail     = pref.getString(User.KEY_EMAIL, "");
        this.mUID       = pref.getString(User.KEY_UID, "");
        this.mFirstName = pref.getString(User.KEY_FIRST_NAME, "");
        this.mLastName  = pref.getString(User.KEY_LAST_NAME, "");
        this.mUserName  = pref.getString(User.KEY_USERNAME, "");
        this.mGender    = pref.getString(User.KEY_GENDER, "");
        this.mPhone     = pref.getString(User.KEY_PHONE, "");
        this.mCity      = pref.getString(User.KEY_CITY, "");
        this.mHomeTown  = pref.getString(User.KEY_HOMETOWN, "");
        this.mCountry   = pref.getString(User.KEY_COUNTRY, "");
        this.mDOB       = pref.getString(User.KEY_DOB, "");
        this.mProvider  = pref.getString(User.KEY_PROVIDER, "");
        this.mToken = pref.getString(User.KEY_TOKEN, null);
        this.mProfilePic    = pref.getString(User.KEY_PROFILE_PIC, "");
    }

    private String parseHometown(List<Person.PlacesLived> placesLived) {
        String hometown = null;
        if(placesLived != null){
            for (int i = 0; i < placesLived.size(); i++) {
                if (placesLived.get(i).isPrimary())
                    hometown = placesLived.get(i).getValue();
            }
        }
        return hometown;
    }

    private String parseGender(int gender) {
        switch (gender){
            case Person.Gender.FEMALE:
                return "Female";
            case Person.Gender.MALE:
                return "Male";
            default:
                return "Unknown";
        }
    }

    public JSONObject registrationParams(){
        JSONObject json    = new JSONObject();
        JSONObject request = new JSONObject();
        try {
            json.put(KEY_ID, this.mID);
            json.put(KEY_EMAIL, this.mEmail);
            json.put(KEY_FIRST_NAME, this.mFirstName);
            json.put(KEY_LAST_NAME, this.mLastName);
            json.put(KEY_GENDER, this.mGender);
            json.put(KEY_PHONE, this.mPhone);
            json.put(KEY_DOB, this.mDOB);
            json.put(KEY_USERNAME, this.mUserName);
            json.put(KEY_HOMETOWN, this.mHomeTown);
            json.put(KEY_CITY, this.mCity);
            json.put(KEY_COUNTRY, this.mCountry);
            json.put(KEY_UID, this.mUID);
            json.put(KEY_TOKEN, this.mToken);
            json.put(KEY_PROFILE_PIC, this.mProfilePic);
            request.put("registration", json);
        } catch (JSONException e) {
            Log.e(TAG, "Error creating JSON: " + e.toString());
        }
        return json;
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put(KEY_USERNAME, getUserName());
            json.put(KEY_USER_ID, SwaggApp.getInstance().getSession().getUser().getID());
            json.put(KEY_FIRST_NAME, getFirstName());
            json.put(KEY_LAST_NAME, getLastName());
            json.put(KEY_PROFILE_PIC, getProfilePic());
            json.put(KEY_EMAIL, getEmail());
            json.put(KEY_GENDER, getGender());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }


    public int getID() {
        return mID;
    }

    public String getProvider() {
        return mProvider;
    }

    public void setProvider(String mProvider) {
        this.mProvider = mProvider;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getDOB() {
        return mDOB;
    }

    public void setDOB(String mDOB) {
        this.mDOB = mDOB;
    }

    public String getProfilePic() {
        return mProfilePic;
    }

    public void setProfilePic(String mProfilePic) {
        this.mProfilePic = mProfilePic;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String mGender) {
        this.mGender = mGender;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getHomeTown() {
        return mHomeTown;
    }

    public void setHomeTown(String mHomeTown) {
        this.mHomeTown = mHomeTown;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String mCity) {
        this.mCity = mCity;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public String getUID() {
        return mUID;
    }

    public void setUID(String mUID) {
        this.mUID = mUID;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String mAuthToken) {
        this.mToken = mAuthToken;
    }
}

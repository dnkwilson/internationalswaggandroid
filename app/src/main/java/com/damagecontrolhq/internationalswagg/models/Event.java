package com.damagecontrolhq.internationalswagg.models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * InternationalSwagg
 * Created by dwilson on 8/27/15.
 */
@SuppressWarnings("ImplicitArrayToString")
public class Event implements Parcelable{
    private static final String TAG = Event.class.getName();
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_BODY = "description";
    public static final String KEY_DATE = "date";
    public static final String KEY_PHOTO = "flyer";
    public static final String KEY_PHOTO_URL = "url";
    public static final String KEY_GALLERIES = "galleries";
    public static final String KEY_VIDEOS = "videos";

    @Expose
    @SerializedName("id")
    private int mId;

    @Expose
    @SerializedName("name")
    private String mTitle;

    @Expose
    @SerializedName("date")
    private Date mDate;

    @Expose
    @SerializedName("description")
    private String mBody;

    @Expose
    @SerializedName("flyer")
    private String mPhoto;

    @Expose
    @SerializedName("galleries")
    private ArrayList<EventMedia> mGalleries = new ArrayList<>();

    @Expose
    @SerializedName("videos")
    private ArrayList<EventMedia> mVideos = new ArrayList<>();

    public Event(){}

    public Event(JSONObject json) throws JSONException {
        mId    = json.getInt(KEY_ID);
        mTitle = json.getString(KEY_NAME);
        mBody  = json.getString(KEY_BODY);
        mDate  = formatDate(json.getString(KEY_DATE));
        mPhoto = json.getJSONObject(KEY_PHOTO)
                .getString(KEY_PHOTO_URL);
        mVideos = loadMedia(json.getJSONArray(KEY_VIDEOS));
        mGalleries = loadMedia(json.getJSONArray(KEY_GALLERIES));
    }

    private Event(Bundle bundle){
        this.mId    = bundle.getInt(KEY_ID);
        this.mTitle = bundle.getString(KEY_NAME);
        this.mBody  = bundle.getString(KEY_BODY);
        this.mDate  = formatDate(bundle.getString(KEY_DATE));
        this.mPhoto = bundle.getString(KEY_PHOTO_URL);
        bundle.setClassLoader(EventMedia.class.getClassLoader());
        this.mGalleries = bundle.getParcelableArrayList(KEY_GALLERIES);
        this.mVideos = bundle.getParcelableArrayList(KEY_VIDEOS);
    }

    private Date formatDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();

        try {
            convertedDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public String dateToSting(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(mDate);
    }

    public JSONObject toJSON(){
        JSONObject json = new JSONObject();
        try {
            json.put(KEY_ID, mId);
            json.put(KEY_NAME, mTitle);
            json.put(KEY_BODY, mBody);
            json.put(KEY_PHOTO, mPhoto);
            json.put(KEY_VIDEOS, mVideos);
            json.put(KEY_GALLERIES, mGalleries);
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing event to json: " + e.toString());
        }
        return json;
    }

    private ArrayList<EventMedia> loadMedia(JSONArray jsonArray) {
        EventMedia media = new EventMedia();

        if (jsonArray.length() == 0) return new ArrayList<>();

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = (JSONObject) jsonArray.get(i);
                media = new EventMedia(json);

                Log.d(TAG, "EventMedia: " + media.toString());
                if (media.getType().equals(EventMedia.IMAGE_MEDIA_TYPE)){
                    mGalleries.add(media);
                }else{
                    mVideos.add(media);
                }
            }
        }catch (JSONException e){
            Log.d(TAG, "Error loading media: " + e.getStackTrace());
            e.printStackTrace();
        }

        if (media.getType().equals(EventMedia.IMAGE_MEDIA_TYPE)){
           return mGalleries;
        }else{
            return mVideos;
        }
    }


    public String getName() {
        return mTitle;
    }

    public void setName(String name) {
        this.mTitle = name;
    }

    public ArrayList<EventMedia> getVideos() {
        return mVideos;
    }

    public void setVideos(ArrayList<EventMedia> mVideos) {
        this.mVideos = mVideos;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(String date) {
        this.mDate = formatDate(date);
    }

    public ArrayList<EventMedia> getGalleries() {
        return mGalleries;
    }

    public void setGalleries(ArrayList<EventMedia> mGalleries) {
        this.mGalleries = mGalleries;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getPhoto() {
        return mPhoto;
    }

    public void setPhoto(String mPhoto) {
        this.mPhoto = mPhoto;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String mBody) {
        this.mBody = mBody;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // create a bundle for the key value pairs
        Bundle bundle = new Bundle();

        // insert the key value pairs to the bundle
        bundle.putInt(KEY_ID, mId);
        bundle.putString(KEY_BODY, mBody);
        bundle.putString(KEY_DATE, dateToSting());
        bundle.putString(KEY_NAME, mTitle);
        bundle.putString(KEY_PHOTO_URL, mPhoto);
        bundle.putParcelableArrayList(KEY_GALLERIES, mGalleries);
        bundle.putParcelableArrayList(KEY_VIDEOS, mVideos);

        // write the key value pairs to the parcel
        dest.writeBundle(bundle);
    }


    /**
     *Creator required for class implementing the parcelable interface.
     */
    public static final Parcelable.Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel source) {
            // read the bundle containing key value pairs from the parcel
            Bundle bundle = source.readBundle();

            // instantiate a photo using values from the bundle
            return new Event(bundle);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[0];
        }
    };
}

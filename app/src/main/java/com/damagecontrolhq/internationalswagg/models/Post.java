package com.damagecontrolhq.internationalswagg.models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.damagecontrolhq.internationalswagg.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * InternationalSwagg
 * Created by dwilson on 10/15/15.
 */
public class Post implements Parcelable {
    private static final String TAG = "Post";

    public static final String KEY_ID       = "id";
    public static final String KEY_TITLE    = "title";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_BODY     = "body";
    public static final String KEY_IMAGE    = "image";
    public static final String KEY_VIDEO    = "video";
    public static final String KEY_COMMENTS = "comments";
    public static final String KEY_VOTES    = "votes";
    public static final String KEY_DATE     = "created_at";


    private int mID;
    private String mTitle;
    private String mCategory;
    private String mBody;
    private String mImage;
    private String mVideo;
    private ArrayList<Vote> mVotes = new ArrayList<>();
    private ArrayList<Comment> mComments = new ArrayList<>();
    private long mDate;


    public Post(JSONObject json) {
        try {
            mID    = json.getInt(KEY_ID);
            mTitle = json.getString(KEY_TITLE);
            mBody  = json.getString(KEY_BODY);
            mCategory = json.getString(KEY_CATEGORY);
            mImage = json.getString(KEY_IMAGE);
            mVideo = json.getString(KEY_VIDEO);
            mDate  = AppUtils.getDateInMillis(json.getString(KEY_DATE));
            mVotes = loadVotes(json.getJSONArray(KEY_VOTES));
            mComments = loadComments(json.getJSONArray(KEY_COMMENTS));
        }catch (JSONException e){
            Log.d(TAG, "Error creating post " + e.toString());
            e.printStackTrace();
        }
    }

    private Post(Bundle bundle){
        bundle.setClassLoader(Post.class.getClassLoader());
        this.mID    = bundle.getInt(KEY_ID);
        this.mTitle = bundle.getString(KEY_TITLE);
        this.mBody  = bundle.getString(KEY_BODY);
        this.mDate  = bundle.getLong(KEY_DATE);
        this.mImage = bundle.getString(KEY_IMAGE);
        this.mVideo = bundle.getString(KEY_VIDEO);
        bundle.setClassLoader(Vote.class.getClassLoader());
        this.mVotes = bundle.getParcelableArrayList(KEY_VOTES);
        bundle.setClassLoader (Comment.class.getClassLoader());
        this.mComments = bundle.getParcelableArrayList(KEY_COMMENTS);
        bundle.setClassLoader(Post.class.getClassLoader());
        this.mCategory = bundle.getString(KEY_CATEGORY);
    }

    private ArrayList<Comment> loadComments(JSONArray jsonArray) {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = (JSONObject) jsonArray.get(i);
                Comment comment = new Comment(json);
                Log.d(TAG, "Comment: " + comment.toString());
                mComments.add(comment);
            }
        }catch (JSONException e){
            Log.d(TAG, "Error loading votes: " + e.toString());
            e.printStackTrace();
        }
        return mComments;
    }

    private ArrayList<Vote> loadVotes(JSONArray jsonArray) {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = (JSONObject) jsonArray.get(i);
                Vote vote = new Vote(json);
                Log.d(TAG, "Vote: " + vote.toString());
                mVotes.add(vote);
            }
        }catch (JSONException e){
            Log.d(TAG, "Error loading votes: " + e.toString());
            e.printStackTrace();
        }
        return mVotes;
    }

    public boolean userHasVoted(int userId){
        ArrayList<Vote> votes = mVotes;
        boolean voted = false;
        for(Vote v : votes){
            if(v.getUserId() != 0 && v.getUserId() == userId){
                voted = true;
            }
        }
        return voted;
    }

    public Vote userVote(int userId){
        ArrayList<Vote> votes = mVotes;
        Vote userVote = null;
        for(Vote v : votes) {
            if (v.getUserId() != 0 && v.getUserId() == userId) {
                userVote = v;
            }
        }
        return userVote;
    }

    public int getID() {
        return mID;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getBody() {
        return mBody;
    }

    public String getImage() {
        return mImage;
    }

    public String getVideo() {
        return mVideo;
    }

    public long getDate() {
        return mDate;
    }

    public ArrayList<Comment> getComments() {
        return mComments;
    }

    public ArrayList<Vote> getVotes() {
        return mVotes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mID);
        dest.writeString(this.mTitle);
        dest.writeString(this.mCategory);
        dest.writeString(this.mBody);
        dest.writeString(this.mImage);
        dest.writeString(this.mVideo);
        dest.writeTypedList(this.mVotes);
        dest.writeTypedList(this.mComments);
        dest.writeLong(this.mDate);
    }

    protected Post(Parcel in) {
        this.mID = in.readInt();
        this.mTitle = in.readString();
        this.mCategory = in.readString();
        this.mBody = in.readString();
        this.mImage = in.readString();
        this.mVideo = in.readString();
        this.mVotes = in.createTypedArrayList(Vote.CREATOR);
        this.mComments = in.createTypedArrayList(Comment.CREATOR);
        this.mDate = in.readLong();
    }

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };
}

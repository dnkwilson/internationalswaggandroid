package com.damagecontrolhq.internationalswagg.models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 9/1/15.
 */
public class Photo implements Parcelable{
    private static final String ID   = "id";
    public static final String NAME = "name";
    public static final String URL  = "url";
    public static final String THUMB  = "thumb";
    public static final String GALLERY_ID = "gallery_id";

    @Expose
    @SerializedName("id")
    private String mId;
    @Expose
    @SerializedName("gallery_id")
    private int mGalleryId;
    @Expose
    @SerializedName("thumb")
    private String mThumb;
    @Expose
    @SerializedName("name")
    private String mName;
    @Expose
    @SerializedName("url")
    private String mUrl;

    public String getThumb() {
        return mThumb;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public Photo(JSONObject json) throws JSONException {
        mId      = json.getString(ID);
        mGalleryId = json.getInt(GALLERY_ID);
        mName    = json.getString(NAME);
        mUrl     = json.getString(URL);
        mThumb   = json.getString(THUMB);
    }

    public JSONObject toJSON() throws JSONException{
        JSONObject json = new JSONObject();
        json.put(ID, mId);
        json.put(GALLERY_ID, mGalleryId);
        json.put(NAME, mName);
        json.put(URL, mUrl);
        json.put(THUMB, mThumb);
        return json;
    }

    private Photo(Bundle bundle){
        this.mId      = bundle.getString(ID);
        this.mGalleryId = bundle.getInt(GALLERY_ID);
        this.mName    = bundle.getString(NAME);
        this.mUrl     = bundle.getString(URL);
        this.mThumb   = bundle.getString(THUMB);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // create a bundle for the key value pairs
        Bundle bundle = new Bundle();

        // insert the key value pairs to the bundle
        bundle.putString(ID, mId);
        bundle.putInt(GALLERY_ID, mGalleryId);
        bundle.putString(NAME, mName);
        bundle.putString(URL, mUrl);
        bundle.putString(THUMB, mThumb);

        // write the key value pairs to the parcel
        dest.writeBundle(bundle);
    }

    /**
     *Creator required for class implementing the parcelable interface.
     */
    public static final Parcelable.Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            // read the bundle containing key value pairs from the parcel
            Bundle bundle = source.readBundle();

            // instantiate a photo using values from the bundle
            return new Photo(bundle);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[0];
        }
    };
}

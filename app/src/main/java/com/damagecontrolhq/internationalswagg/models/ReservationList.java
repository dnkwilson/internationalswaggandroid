package com.damagecontrolhq.internationalswagg.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dwilson on 2/9/17.
 */

public class ReservationList {
    @SerializedName(value="packages")
    public ArrayList<Reservation> reservations;

    public ReservationList(){}
}

package com.damagecontrolhq.internationalswagg.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dwilson on 2/23/17.
 */

public class ImageGallery {
    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("media_type")
    private String media_type;

    @Expose
    @SerializedName("event_id")
    private int event_id;

    @Expose
    @SerializedName("preview_image")
    private String preview_image;

    @Expose
    @SerializedName("info")
    private String info;

    @Expose
    @SerializedName("images")
    private ArrayList<Photo> images;

    public ImageGallery(){}

    public ArrayList<Photo> getImages() {
        return images;
    }
}

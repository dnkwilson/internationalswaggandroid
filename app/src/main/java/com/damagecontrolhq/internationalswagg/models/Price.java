package com.damagecontrolhq.internationalswagg.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dwilson on 1/28/17.
 */

public class Price implements Parcelable {

    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("jm")
    private String jm;
    @Expose
    @SerializedName("us")
    private String us;
    @Expose
    @SerializedName("gb")
    private String gb;
    @Expose
    @SerializedName("eu")
    private String eu;
    @Expose
    @SerializedName("ca")
    private String ca;

    public Price(){}

    public String getJm() {
        return jm;
    }

    public String getUs() {
        return us;
    }

    public String getGb() {
        return gb;
    }

    public String getEu() {
        return eu;
    }

    public String getCa() {
        return ca;
    }

    public Double getFormattedPrice(){
        return Double.parseDouble(us);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.jm);
        dest.writeString(this.us);
        dest.writeString(this.gb);
        dest.writeString(this.eu);
        dest.writeString(this.ca);
    }

    protected Price(Parcel in) {
        this.id = in.readInt();
        this.jm = in.readString();
        this.us = in.readString();
        this.gb = in.readString();
        this.eu = in.readString();
        this.ca = in.readString();
    }

    public static final Parcelable.Creator<Price> CREATOR = new Parcelable.Creator<Price>() {
        @Override
        public Price createFromParcel(Parcel source) {
            return new Price(source);
        }

        @Override
        public Price[] newArray(int size) {
            return new Price[size];
        }
    };
}

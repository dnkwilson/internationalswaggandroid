package com.damagecontrolhq.internationalswagg.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dwilson on 3/4/17.
 */

public class SaleTransactionList {
    @SerializedName(value="sale_transactions")
    public ArrayList<SaleTransaction> sales;

}

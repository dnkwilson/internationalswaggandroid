package com.damagecontrolhq.internationalswagg.models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.damagecontrolhq.internationalswagg.app.SwaggApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * InternationalSwagg
 * Created by dwilson on 9/29/15.
 */
public class Chat implements Parcelable {
    public static final String KEY_ID         = "id";
    public static final String KEY_AVATAR     = "avatar";
    public static final String KEY_AUDIO      = "audio_url";
    public static final String KEY_USER_ID    = "user_id";
    public static final String KEY_DURATION   = "duration";
    public static final String KEY_USERNAME   = "username";
    public static final String KEY_BODY       = "body";
    public static final String KEY_MEDIA      = "media";
    public static final String KEY_MEDIA_FILE = "media_file";
    public static final String KEY_FILENAME   = "filename";
    public static final String KEY_CATEGORY   = "category";
    public static final String KEY_TIME       = "time";
    public static final String KEY_LOCAL_URL  = "local_url";
    public static final String TEXT_MESSAGE   = "text_message";
    public static final String VOICE_MESSAGE  = "voice_message";
    public static final String PHOTO_MESSAGE  = "photo_message";

    private String username, body, time, category, media, fileName;
    private int userId, duration;
    private String localMedia;
    private boolean isUploaded = false;

    public Chat(){

    }

    public Chat(String body, String media, User user){
        this.category = Chat.PHOTO_MESSAGE;
        this.username = user.getUserName();
        this.userId   = user.getID();
        this.time     = currentTime();
        buildBody(body, media);
    }

    public Chat(String media, String body) {
        this.category = PHOTO_MESSAGE;
        this.media = media;
        this.body = body;
        this.userId = SwaggApp.getInstance().getSession().getUser().getID();
        this.username = SwaggApp.getInstance().getSession().getUser().getUserName();
        this.time = currentTime();
    }

    private void buildBody(String body, String media) {
        switch (category){
            case PHOTO_MESSAGE:
                this.body = body;
                this.media = media;
                break;
            case VOICE_MESSAGE:
                this.body = null;
                this.media = media;
                break;
            default:
                this.body = body;
                this.media = null;
                break;
        }
    }

    public Chat(String body, User user) {
        this.username = user.getUserName();
        this.userId   = user.getID();
        this.body     = body;
        this.time     = currentTime();
        this.category = TEXT_MESSAGE;
    }

    public Chat(JSONObject json, User user){
        try{
            this.body = json.getString(KEY_AUDIO);
            this.duration = json.getInt(KEY_DURATION);
            this.username = user.getUserName();
            this.userId = user.getID();
            this.time = currentTime();
            this.category = VOICE_MESSAGE;
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    private Chat(Bundle bundle){
        this.category = bundle.getString(KEY_CATEGORY);
        this.media = bundle.getString(KEY_MEDIA);
        this.body = bundle.getString(KEY_BODY);
        this.userId = bundle.getInt(KEY_USER_ID);
        this.username = bundle.getString(KEY_USERNAME);
        this.time = bundle.getString(KEY_TIME);
        this.duration = bundle.getInt(KEY_DURATION);
    }

    public Chat(JSONObject json){
        try{
            this.username = json.getString(KEY_USERNAME);
            this.userId = json.getInt(KEY_USER_ID);
            this.time = json.getString(KEY_TIME);
            this.category = json.getString(KEY_CATEGORY);
            if(!this.category.equals(VOICE_MESSAGE)) this.body = json.getString(KEY_BODY);
            if (category.equals(VOICE_MESSAGE))
                this.setVoiceMessage(json.getString(KEY_FILENAME),
                        json.getString(KEY_MEDIA), json.getInt(KEY_DURATION));

            if (category.equals(PHOTO_MESSAGE))
                this.setPhotoMessage(json.getString(KEY_FILENAME),
                        json.getString("media"));

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put(KEY_BODY, getBody());
            json.put(KEY_CATEGORY, getCategory());
            json.put(KEY_USER_ID, SwaggApp.getInstance().getSession().getUser().getID());
            json.put(KEY_USERNAME, SwaggApp.getInstance().getSession().getUser().getUserName());
            json.put(KEY_TIME, currentTime());

            // Build media hash
            if(getCategory() != TEXT_MESSAGE){
                JSONObject mediaHash = new JSONObject();
                mediaHash.put(KEY_MEDIA_FILE, getMedia());
                if (getCategory().equals(VOICE_MESSAGE)) mediaHash.put(KEY_DURATION, getDuration());
                mediaHash.put(KEY_FILENAME, getFileName());
                json.put(KEY_MEDIA, mediaHash);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }

    public void setVoiceMessage(String fileName, String file, int duration){
        setFileName(fileName);
        setMedia(file);
        setDuration(duration);
        setCategory(VOICE_MESSAGE);
    }

    public void setPhotoMessage(String fileName, String file){
        setFileName(fileName);
        setMedia(file);
        setCategory(PHOTO_MESSAGE);
    }

    public String getUsername() {
        return username;
    }

    public String getBody() {
        return body;
    }

    public int getUserId() {
        return userId;
    }

    public String getTime() {
        return time;
    }

    public String getCategory() {
        return category;
    }

    public int getDuration() {
        return duration;
    }

    public String getMedia() {
        return media;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(boolean isUploaded) {
        this.isUploaded = isUploaded;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFileName() {
        if (fileName == null){
            String str[] = getMedia().split("/");
            String file[] = str[str.length-1].split("\\.");
            fileName = file[0];
        }
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setLocalMedia(String localMedia) {
        this.localMedia = localMedia;
    }

    public String getLocalMedia() {
        return localMedia;
    }

    public String currentTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getFormattedTime(){
        Calendar now = Calendar.getInstance();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getDefault());

        SimpleDateFormat longTime = new SimpleDateFormat("MMM d, h:mm a", Locale.getDefault());
        longTime.setTimeZone(TimeZone.getDefault());

        String formattedDate = null;
        try {
            Calendar chatDate = Calendar.getInstance();
            chatDate.setTime(dateFormat.parse(getTime()));
            if (now.get(Calendar.DATE) == chatDate.get(Calendar.DATE)){
                SimpleDateFormat shortTime = new SimpleDateFormat("h:mm a", Locale.getDefault());
                formattedDate = shortTime.format(chatDate.getTime());
            }else{
                formattedDate = longTime.format(chatDate.getTime());
            }
        } catch (ParseException e) {
            Log.e(Chat.class.getName(), "Error parsing date: " + e.toString());
        }

        return formattedDate;
    }

    public boolean isFromMe(){
        return userId == SwaggApp.getInstance().getSession().getUser().getID();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // create a bundle for the key value pairs
        Bundle bundle = new Bundle();

        // insert the key value pairs to the bundle
        bundle.putInt(KEY_USER_ID, userId);
        bundle.putString(KEY_USERNAME, username);
        bundle.putString(KEY_BODY, body);
        bundle.putString(KEY_TIME, time);
        bundle.putString(KEY_CATEGORY, category);
        bundle.putInt(KEY_DURATION, duration);
        bundle.putString(KEY_MEDIA, media);

        // write the key value pairs to the parcel
        dest.writeBundle(bundle);
    }

    public static final Parcelable.Creator<Chat> CREATOR = new Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel source) {
            // read the bundle containing key value pairs from the parcel
            Bundle bundle = source.readBundle();

            // instantiate a photo using values from the bundle
            return new Chat(bundle);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[0];
        }
    };

    public void setUsername(String userName) {
        this.username = userName;
    }
}

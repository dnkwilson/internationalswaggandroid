package com.damagecontrolhq.internationalswagg.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by dwilson on 1/28/17.
 */

public class Product implements Parcelable{
    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("category_id")
    private int categoryId;

    @Expose
    @SerializedName("category_name")
    private String categoryName;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("price")
    private Price price;

    public Product(){}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Price getPrice() {
        return price;
    }

    public int getJmPrice(){
        BigDecimal p = new BigDecimal(price.getJm());
        return p.intValue();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.categoryId);
        dest.writeString(this.categoryName);
        dest.writeString(this.name);
        dest.writeParcelable(this.price, flags);
    }

    protected Product(Parcel in) {
        this.id = in.readInt();
        this.categoryId = in.readInt();
        this.categoryName = in.readString();
        this.name = in.readString();
        this.price = in.readParcelable(Price.class.getClassLoader());
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}

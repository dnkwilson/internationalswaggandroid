package com.damagecontrolhq.internationalswagg.models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.damagecontrolhq.internationalswagg.app.DamageControlAPI;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 10/20/15.
 */
@SuppressWarnings("ALL")
public class Comment implements Parcelable {
    private static final String TAG = "Comment";

    private static final String KEY_ID = "id";
    private static final String KEY_POST_ID = "post_id";
    public static final String KEY_USER_ID = "user_id";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_AVATAR  = "avatar";
    public static final String KEY_COMMENT = "comment";
    private static final String KEY_DATE    = "created_at";

    private int mId;
    private int mPostId;
    private int mUserId;
    private String mComment;
    private String mUserName;
    private String mAvatar;
    private long mDate;

    private Comment(Bundle bundle){
        this.mId     = bundle.getInt(KEY_ID);
        this.mPostId = bundle.getInt(KEY_POST_ID);
        this.mUserId = bundle.getInt(KEY_USER_ID);
        this.mAvatar = bundle.getString(KEY_AVATAR);
        this.mUserName = bundle.getString(KEY_USERNAME);
        this.mComment = bundle.getString(KEY_COMMENT);
        this.mDate   = bundle.getLong(KEY_DATE);
    }

    Comment(Parcel in) {
        mId = in.readInt();
        mUserId = in.readInt();
        mAvatar = in.readString();
        mUserName = in.readString();
        mPostId = in.readInt();
        mComment = in.readString();
        mDate = in.readLong();
    }

    public Comment(JSONObject json) {
        try {
            mId    = json.getInt(KEY_ID);
            mPostId = json.getInt(KEY_POST_ID);
            mAvatar = json.getString(KEY_AVATAR);
            mUserName = json.getString(KEY_USERNAME);
            mUserId  = json.getInt(KEY_USER_ID);
            mComment = json.getString(KEY_COMMENT);
            mDate = AppUtils.getDateInMillis(json.getString(KEY_DATE));
        }catch (JSONException e){
            Log.d(TAG, "Error creating comment " + e.getStackTrace());
            e.printStackTrace();
        }
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setPostId(int mPostId) {
        this.mPostId = mPostId;
    }

    public void setUserId(int mUserId) {
        this.mUserId = mUserId;
    }

    public void setComment(String mComment) {
        this.mComment = mComment;
    }

    public void setDate(long mDate) {
        this.mDate = mDate;
    }

    public int getId() {
        return mId;
    }

    public long getDate() {
        return mDate;
    }

    public String getComment() {
        return mComment;
    }

    public int getUserId() {
        return mUserId;
    }

    public int getPostId() {
        return mPostId;
    }

    public String getAvatar() {
        return DamageControlAPI.BASE_URL + mAvatar;
    }

    public String getUserName() {
        return mUserName;
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            // read the bundle containing key value pairs from the parcel
            Bundle bundle = source.readBundle();

            // instantiate a photo using values from the bundle
            return new Comment(bundle);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // create a bundle for the key value pairs
        Bundle bundle = new Bundle();

        // insert the key value pairs to the bundle
        bundle.putInt(KEY_ID, mId);
        bundle.putInt(KEY_USER_ID, mUserId);
        bundle.putInt(KEY_POST_ID, mPostId);
        bundle.putString(KEY_COMMENT, mComment);
        bundle.putString(KEY_AVATAR, mAvatar);
        bundle.putString(KEY_USERNAME, mUserName);
        bundle.putLong(KEY_DATE, mDate);

        // write the key value pairs to the parcel
        dest.writeBundle(bundle);
    }
}

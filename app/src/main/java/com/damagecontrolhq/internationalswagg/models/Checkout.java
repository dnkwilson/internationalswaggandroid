package com.damagecontrolhq.internationalswagg.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dwilson on 2/9/17.
 */

public class Checkout {
    @Expose
    @SerializedName("user_id")
    private int userId;
    @Expose
    @SerializedName("package_id")
    private int packageId;
    @Expose
    @SerializedName("products")
    private ArrayList<HashMap<String, Integer>> products;
    @Expose
    @SerializedName("first_name")
    private String firstName;
    @Expose
    @SerializedName("last_name")
    private String lastName;
    @Expose
    @SerializedName("payment_nonce")
    private String paymentNonce;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("pay_full")
    private boolean payFull;

    public Checkout(){}

    public Checkout(int userId, String firstName, String lastName, String paymentNonce,
                    String currency, int packageId, ArrayList<HashMap<String,
                    Integer>> products, boolean payFull){
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.paymentNonce = paymentNonce;
        this.currency = currency;
        this.packageId = packageId;
        this.products = products;
        this.payFull = payFull;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int memberId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPaymentNonce() {
        return paymentNonce;
    }

    public void setPaymentNonce(String paymentNonce) {
        this.paymentNonce = paymentNonce;
    }
}

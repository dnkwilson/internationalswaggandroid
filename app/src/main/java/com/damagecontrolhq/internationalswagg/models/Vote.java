package com.damagecontrolhq.internationalswagg.models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 10/20/15.
 */
public class Vote implements Parcelable{
    private static final String TAG = "Video";

    private static final String KEY_ID = "id";
    private static final String KEY_POST_ID = "voteable_id";
    private static final String KEY_USER_ID = "voter_id";

    private int mID;
    private int mPostId;
    private int mUserId;

    public Vote(JSONObject json) {
        try {
            mID    = json.getInt(KEY_ID);
            mPostId = json.getInt(KEY_POST_ID);
            mUserId  = json.getInt(KEY_USER_ID);
        }catch (JSONException e){
            Log.d(TAG, "Error creating vote " + e.toString());
            e.printStackTrace();
        }
    }

    private Vote(Bundle bundle){
        this.mID    = bundle.getInt(KEY_ID);
        this.mPostId = bundle.getInt(KEY_POST_ID);
        this.mUserId  = bundle.getInt(KEY_USER_ID);
    }

    Vote(Parcel in) {
        mID = in.readInt();
        mPostId = in.readInt();
        mUserId = in.readInt();
    }

    public static final Parcelable.Creator<Vote> CREATOR = new Parcelable.Creator<Vote>() {
        @Override
        public Vote createFromParcel(Parcel source) {
            // read the bundle containing key value pairs from the parcel
            Bundle bundle = source.readBundle();

            // instantiate a photo using values from the bundle
            return new Vote(bundle);
        }

        @Override
        public Vote[] newArray(int size) {
            return new Vote[size];
        }
    };

    public int getID() {
        return mID;
    }

    public int getPostId() {
        return mPostId;
    }

    public int getUserId() {
        return mUserId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // create a bundle for the key value pairs
        Bundle bundle = new Bundle();

        // insert the key value pairs to the bundle
        bundle.putInt(KEY_ID, mID);
        bundle.putInt(KEY_POST_ID, mPostId);
        bundle.putInt(KEY_USER_ID, mUserId);

        // write the key value pairs to the parcel
        dest.writeBundle(bundle);
    }

}

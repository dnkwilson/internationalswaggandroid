package com.damagecontrolhq.internationalswagg.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.damagecontrolhq.internationalswagg.utils.AppUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dwilson on 2/12/17.
 */

public class SaleTransaction implements Parcelable {
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("user_id")
    private int userId;
    @Expose
    @SerializedName("transaction_id")
    private String transactionId;
    @Expose
    @SerializedName("package")
    private Reservation reservation;
    @Expose
    @SerializedName("amount_paid")
    private String amountPaid;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("sale_transaction_items")
    private ArrayList<SaleTransactionItem> saleTransactionItems;

    public SaleTransaction() {
    }

    public int getId() {
        return id;
    }

    public ArrayList<SaleTransactionItem> getSaleTransactionItems() {
        return saleTransactionItems;
    }

    public String getPackageName() {
        return reservation.getName();
    }

    public String getPackageImage() {
        return reservation.getPhoto();
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public boolean isPaidInFull() {
        Double amount = Double.parseDouble(amountPaid);
        return amount >= reservation.getPrice().getFormattedPrice();
    }

    public String getPrice() {
        double packagePrice = reservation.getPrice().getFormattedPrice();
        double price = isPaidInFull() ? packagePrice : packagePrice/2;
        return AppUtils.formatPrice(price);
    }

    public Reservation getReservation() {
        return reservation;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.userId);
        dest.writeString(this.transactionId);
        dest.writeParcelable(this.reservation, flags);
        dest.writeString(this.amountPaid);
        dest.writeTypedList(this.saleTransactionItems);
    }

    protected SaleTransaction(Parcel in) {
        this.id = in.readInt();
        this.userId = in.readInt();
        this.transactionId = in.readString();
        this.reservation = in.readParcelable(Reservation.class.getClassLoader());
        this.amountPaid = in.readString();
        this.saleTransactionItems = in.createTypedArrayList(SaleTransactionItem.CREATOR);
    }

    public static final Creator<SaleTransaction> CREATOR = new Creator<SaleTransaction>() {
        @Override
        public SaleTransaction createFromParcel(Parcel source) {
            return new SaleTransaction(source);
        }

        @Override
        public SaleTransaction[] newArray(int size) {
            return new SaleTransaction[size];
        }
    };

    public String getFormattedStatus() {
        switch (status){
            case "settled":
               return "CONFIRMED";
            case "submitted_for_settlement":
                return "PROCESSING";
            default:
                return "PENDING";
        }
    }
}

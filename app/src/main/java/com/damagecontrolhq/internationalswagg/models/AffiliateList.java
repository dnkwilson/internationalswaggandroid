package com.damagecontrolhq.internationalswagg.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dwilson on 2/24/17.
 */

public class AffiliateList {

    @Expose
    @SerializedName("affiliates")
    private ArrayList<Affiliate> affiliates;

    public AffiliateList(){}

    public ArrayList<Affiliate> getAffiliates() {
        return affiliates;
    }
}

package com.damagecontrolhq.internationalswagg.models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 9/22/15.
 */
public class Song implements Parcelable{
    private static final String TAG = "Song";
    private static final String KEY_ATTRIBUTES = "attributes";
    private static final String KEY_ID = "id";
    private static final String KEY_ARTIST = "artist";
    private static final String KEY_TITLE = "title";
    private static final String KEY_URL    = "url";
    private static final String KEY_ARTWORK = "artwork";
    private static final String KEY_DOWNLOADS = "download_count";
    private static final String KEY_LENGTH    = "length";
    private static final String KEY_PLAYS    = "play_count";
    private static final String KEY_DOWNLOADABLE = "downloadable";

    private String mID;
    private String mArtist;
    private String mTitle;
    private String mUrl;
    private String mArtwork;
    private int mLength;
    private int mPlayCount;
    private int mDownloadCount;
    private boolean mDownloadable;

    public Song(JSONObject json) {
        try{
            this.mID = json.getString(KEY_ID);
            this.mArtist = json.getString(KEY_ARTIST);
            this.mTitle = json.getString(KEY_TITLE);
            this.mArtwork = json.getString(KEY_ARTWORK);
            this.mUrl = json.getString(KEY_URL);
            this.mDownloadCount = json.getInt(KEY_DOWNLOADS);
            this.mPlayCount = json.getInt(KEY_PLAYS);
            this.mLength = json.getInt(KEY_LENGTH);
            this.mDownloadable = json.getBoolean(KEY_DOWNLOADABLE);
        }catch (JSONException e){
            Log.e(TAG, "Error parsing JSON: " + e.toString());
        }
    }

    public Song(String thisId, String thisTitle, String thisArtist) {
        this.mID = thisId;
        this.mTitle = thisTitle;
        this.mArtist = thisArtist;
    }

    private Song(Bundle bundle){
        this.mID      = bundle.getString(KEY_ID);
        this.mTitle   = bundle.getString(KEY_TITLE);
        this.mArtist  = bundle.getString(KEY_ARTIST);
        this.mArtwork = bundle.getString(KEY_ARTWORK);
        this.mUrl     = bundle.getString(KEY_URL);
        this.mLength  = bundle.getInt(KEY_LENGTH);
    }

    public String getID() {
        return mID;
    }

    public String getArtist() {
        return mArtist;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getArtwork() {
        return mArtwork;
    }

    public int getLength() {
        return mLength;
    }

    public int getPlayCount() {
        return mPlayCount;
    }

    public int getDownloadCount() {
        return mDownloadCount;
    }

    public boolean isDownloadable() {
        return mDownloadable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // create a bundle for the key value pairs
        Bundle bundle = new Bundle();

        // insert the key value pairs to the bundle
        bundle.putString(KEY_ID, mID);
        bundle.putString(KEY_TITLE, mTitle);
        bundle.putString(KEY_ARTIST, mArtist);
        bundle.putString(KEY_ARTWORK, mArtwork);
        bundle.putString(KEY_URL, mUrl);
        bundle.putInt(KEY_LENGTH, mLength);

        // write the key value pairs to the parcel
        dest.writeBundle(bundle);
    }

    public static final Parcelable.Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel source) {
            // read the bundle containing key value pairs from the parcel
            Bundle bundle = source.readBundle();

            // instantiate a photo using values from the bundle
            return new Song(bundle);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[0];
        }
    };
}

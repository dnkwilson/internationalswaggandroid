package com.damagecontrolhq.internationalswagg.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dwilson on 2/12/17.
 */

public class SaleTransactionItem implements Parcelable {
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("sale_transaction_id")
    private int saleTransactionId;
    @Expose
    @SerializedName("qty")
    private int qty;
    @Expose
    @SerializedName("product")
    private Product product;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.saleTransactionId);
        dest.writeInt(this.qty);
        dest.writeParcelable(this.product, flags);
    }

    public SaleTransactionItem() {
    }

    public int getQty() {
        return qty;
    }

    public Product getProduct() {
        return product;
    }

    protected SaleTransactionItem(Parcel in) {
        this.id = in.readInt();
        this.saleTransactionId = in.readInt();
        this.qty = in.readInt();
        this.product = in.readParcelable(Product.class.getClassLoader());
    }

    public static final Parcelable.Creator<SaleTransactionItem> CREATOR = new Parcelable.Creator<SaleTransactionItem>() {
        @Override
        public SaleTransactionItem createFromParcel(Parcel source) {
            return new SaleTransactionItem(source);
        }

        @Override
        public SaleTransactionItem[] newArray(int size) {
            return new SaleTransactionItem[size];
        }
    };
}

package com.damagecontrolhq.internationalswagg.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dwilson on 1/29/17.
 */
public class ReservationItem implements Parcelable {
    @Expose
    @SerializedName("id")
    private int mId;

    @Expose
    @SerializedName("package_id")
    private int mReservationId;

    @Expose
    @SerializedName("qty")
    private int mQty;

    @Expose
    @SerializedName("product")
    private Product mProduct;

    public ReservationItem(){}

    public ReservationItem(Product product){
        this.mProduct = product;
        this.mQty = 1;
    }

    public Product getProduct() {
        return mProduct;
    }

    public int getId() {
        return mId;
    }

    public int getReservationId() {
        return mReservationId;
    }

    public int getQty() {
        return mQty;
    }

    public int getCategoryId(){
        return mProduct.getCategoryId();
    }

    public void setQty(int mQty) {
        this.mQty = mQty;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeInt(this.mReservationId);
        dest.writeInt(this.mQty);
        dest.writeParcelable(this.mProduct, flags);
    }

    protected ReservationItem(Parcel in) {
        this.mId = in.readInt();
        this.mReservationId = in.readInt();
        this.mQty = in.readInt();
        this.mProduct = in.readParcelable(Product.class.getClassLoader());
    }

    public static final Parcelable.Creator<ReservationItem> CREATOR = new Parcelable.Creator<ReservationItem>() {
        @Override
        public ReservationItem createFromParcel(Parcel source) {
            return new ReservationItem(source);
        }

        @Override
        public ReservationItem[] newArray(int size) {
            return new ReservationItem[size];
        }
    };
}

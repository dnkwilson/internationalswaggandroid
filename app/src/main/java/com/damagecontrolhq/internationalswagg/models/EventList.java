package com.damagecontrolhq.internationalswagg.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dwilson on 2/23/17.
 */

public class EventList {
    @SerializedName(value="events")
    public ArrayList<Event> events;

    public EventList(){}
}

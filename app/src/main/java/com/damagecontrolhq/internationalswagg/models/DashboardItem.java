package com.damagecontrolhq.internationalswagg.models;

/**
 * InternationalSwagg
 * Created by dwilson on 11/28/15.
 */
public class DashboardItem {
    public static final String TYPE_MENU    = "menu_item";
    private static final String TYPE_PREVIEW = "preview_item";

    private final String mName;
    private final String mType;
    private String mLink;
    private int mResource, mColor;

    public DashboardItem(String name, int resource, String link, int color){
        this.mType = TYPE_MENU;
        this.mName = name;
        this.mResource = resource;
        this.mLink = link;
        this.mColor = color;
    }

    public DashboardItem(int resource, String link, int color){
        this.mType = TYPE_MENU;
        this.mName = null;
        this.mResource = resource;
        this.mLink = link;
        this.mColor = color;
    }

    public DashboardItem(String name){
        this.mType = TYPE_PREVIEW;
        this.mName = name;
    }

    public int getResource() {
        return mResource;
    }

    public void setResource(int mResource) {
        this.mResource = mResource;
    }

    public String getType() {
        return mType;
    }

    public String getName() {
        return mName;
    }

    public String getLink() {
        return mLink;
    }

    public int getColor() {
        return mColor;
    }
}

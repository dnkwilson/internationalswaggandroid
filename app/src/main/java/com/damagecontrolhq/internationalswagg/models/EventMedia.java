package com.damagecontrolhq.internationalswagg.models;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 12/17/15.
 */
public class EventMedia implements Parcelable {
    private static final String TAG = EventMedia.class.getName();

    public static final String KEY_ID = "id",
                               KEY_NAME = "name",
                               KEY_INFO = "info",
                               KEY_PREVIEW_IMAGE = "preview_image",
                               KEY_MEDIA_TYPE = "media_type",
                               KEY_EVENT_ID = "event_id",
                               IMAGE_MEDIA_TYPE = "image_media",
                               VIDEO_MEDIA_TYPE = "video_media";
    @Expose
    @SerializedName("name")
    private String mName;

    @Expose
    @SerializedName("info")
    private String mInfo;

    @Expose
    @SerializedName("media_type")
    private String mType;

    @Expose
    @SerializedName("preview_image")
    private String mPreview;
    @Expose

    @SerializedName("event_id")
    private String mEventId;

    @Expose
    @SerializedName("id")
    private int mId;

    public EventMedia(){

    }

    public EventMedia(JSONObject json){
        try {
            this.mId = json.getInt(KEY_ID);
            this.mName = json.getString(KEY_NAME);
            this.mInfo = json.getString(KEY_INFO);
            this.mPreview = json.getString(KEY_PREVIEW_IMAGE);
            this.mType = json.getString(KEY_MEDIA_TYPE);
            this.mEventId = json.getString(KEY_EVENT_ID);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getInfo() {
        return mType.equals(IMAGE_MEDIA_TYPE) ? mInfo + " photos" : mInfo;
    }

    public void setInfo(String mInfo) {
        this.mInfo = mInfo;
    }

    public String getPreview() {
        return mPreview;
    }

    public void setPreview(String mPreview) {
        this.mPreview = mPreview;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public String getEventId() {
        return mEventId;
    }

    public void setEventId(String mEventId) {
        this.mEventId = mEventId;
    }

    private EventMedia(Bundle bundle){
        this.mId    = bundle.getInt(KEY_ID);
        this.mEventId = bundle.getString(KEY_EVENT_ID);
        this.mName  = bundle.getString(KEY_NAME);
        this.mInfo  = bundle.getString(KEY_INFO);
        this.mPreview = bundle.getString(KEY_PREVIEW_IMAGE);
        this.mType = bundle.getString(KEY_MEDIA_TYPE);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // create a bundle for the key value pairs
        Bundle bundle = new Bundle();

        // insert the key value pairs to the bundle
        bundle.putInt(KEY_ID, mId);
        bundle.putString(KEY_EVENT_ID, mEventId);
        bundle.putString(KEY_INFO, mInfo);
        bundle.putString(KEY_NAME, mName);
        bundle.putString(KEY_MEDIA_TYPE, mType);
        bundle.putString(KEY_PREVIEW_IMAGE, mPreview);

        // write the key value pairs to the parcel
        dest.writeBundle(bundle);
    }


    /**
     *Creator required for class implementing the parcelable interface.
     */
    public static final Parcelable.Creator<EventMedia> CREATOR = new Creator<EventMedia>() {
        @Override
        public EventMedia createFromParcel(Parcel source) {
            // read the bundle containing key value pairs from the parcel
            Bundle bundle = source.readBundle();

            // instantiate a photo using values from the bundle
            return new EventMedia(bundle);
        }

        @Override
        public EventMedia[] newArray(int size) {
            return new EventMedia[0];
        }
    };
}

package com.damagecontrolhq.internationalswagg;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.damagecontrolhq.internationalswagg.adapters.GalleryAdapter;
import com.damagecontrolhq.internationalswagg.adapters.PhotoAdapter;
import com.damagecontrolhq.internationalswagg.models.Photo;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * InternationalSwagg
 * Created by dwilson on 9/2/15.
 */
@RuntimePermissions
@SuppressWarnings("ALL")
public class PhotoActivity extends AppCompatActivity {
    private static final String TAG = "PhotoActivity";

    private Boolean showToolbar = true;
    private ViewPager mViewPager;
    private String photoName;
    private Toolbar mToolbar;

    private void sharePhoto() {
        File imageFileToShare = savePhoto();

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");

        Uri imageUri = Uri.fromFile(imageFileToShare);
        share.putExtra(Intent.EXTRA_TEXT, "#DamageControl #InternationalSwagg #2016Mar25");
        share.putExtra(Intent.EXTRA_STREAM, imageUri);
        startActivity(Intent.createChooser(share, "Share Image"));
    }

    @NeedsPermission({android.Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void canSave(){
    }

    private File savePhoto() {
        PhotoActivityPermissionsDispatcher.canSaveWithCheck(this);

        ImageView imageView = (ImageView)mViewPager.findViewById(R.id.photo_image);

        Bitmap photo = ((BitmapDrawable)imageView.getDrawable()).getBitmap();

        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File dir = new File(root + "/InternationalSwagg");

        if (!dir.exists()){
            dir.mkdirs();
        }

        File imageFile = new File(dir, photoName + ".jpg");

        try {
            FileOutputStream out = new FileOutputStream(imageFile);
            photo.compress(Bitmap.CompressFormat.JPEG, 50, out);
            out.close();
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        Toast.makeText(this, "Photo saved to Gallery", Toast.LENGTH_SHORT).show();

        return new File(String.valueOf(imageFile));
    }

    private void toggleToolbars() {
        if (showToolbar) {
            mToolbar.setVisibility(View.INVISIBLE);
        } else {
            mToolbar.setVisibility(View.VISIBLE);
            mToolbar.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mToolbar.setVisibility(View.INVISIBLE);
                    showToolbar = !showToolbar;
                }
            }, 1000);
        }
        showToolbar = !showToolbar;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Hide the window title
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Hide the status bar and other OS-level chrome
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        // Hide status bar
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        // Setup toolbars
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Hide toolbars
        mToolbar.setBackgroundColor(Color.TRANSPARENT);
        toggleToolbars();

        // Set header toolbar navigation
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Get data
        Intent intent = getIntent();
        photoName     = intent.getStringExtra(GalleryAdapter.PHOTO_NAME);
        List<Photo> mPhotos = intent.getParcelableArrayListExtra(GalleryAdapter.PHOTOS);
        int position = intent.getIntExtra(GalleryAdapter.POSITION, 0);

        // Setup View
        mViewPager = (ViewPager) findViewById(R.id.pager);
        PhotoAdapter mAdapter = new PhotoAdapter(this, mPhotos);
        mViewPager.setAdapter(mAdapter);

        // Set current item/position
        // displaying selected image first
        mViewPager.setCurrentItem(position);

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN){
                    toggleToolbars();
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_photo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_save_photo:
                File file = savePhoto();
                // Tell the media scanner about the new file so that it is
                // immediately available to the user.
                MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i(TAG, "Scanned " + path + ":");
                                Log.i(TAG, "-> uri=" + uri);
                            }
                        });


                return true;
            case R.id.menu_item_share_photo:
                sharePhoto();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        PhotoActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);

    }

    private void showRationaleDialog(@StringRes int messageResId, final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .setCancelable(false)
                .setMessage(messageResId)
                .show();
    }
}

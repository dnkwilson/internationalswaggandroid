package com.damagecontrolhq.internationalswagg.app;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.models.User;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.damagecontrolhq.internationalswagg.app.DamageControlAPI.CHAT_URL;
import static com.damagecontrolhq.internationalswagg.app.DamageControlAPI.HOST;
import static com.damagecontrolhq.internationalswagg.app.DamageControlAPI.V2_HOST;

/**
 * InternationalSwagg
 * Created by dwilson on 8/28/15.
 */
public class SwaggApp extends MultiDexApplication {
    private static final String TAG = SwaggApp.class.getSimpleName();

    public static final int NUM_OF_COLUMNS = 3;
    public static final int GRID_PADDING = 8;

    public static final String REQUESTED_PAGE = "requested_page";
    public static final String LOGIN_MESSAGE = "login_message";
    public static final String NOTIFICATION_CHANNEL = "international_swagg";

    public static final String FIRST_RUN = "first_run";
    public static final String INVITED_FRIENDS = "invited_friends";
    public static final String PACKAGE_NAME = "com.dnwilson.internationalswagg";

    public static final int NOTIFICATION_ID = 1225;

    // TODO: Add 'user_hometown', 'user_location', 'publish_action'
    public static final String[] FB_PERMISSIONS = {"email", "public_profile", "user_friends",
            "user_hometown", "user_location"};
    
    public static final String CHECK_LOGIN = "check_login";

    private SessionManager mSession;
    private String voiceNoteDir;
    private String photoDir;

    private static SwaggApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        this.mSession = new SessionManager(getApplicationContext());
        String swaggMediaDir = getApplicationContext().getApplicationInfo().dataDir + "/Media/";
        this.voiceNoteDir = swaggMediaDir + "VoiceNotes/";
        this.photoDir = swaggMediaDir + "Photos/";
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static synchronized SwaggApp getInstance(){
        return mInstance;
    }

    public String getVoiceNoteDir() {
        return voiceNoteDir;
    }

    public SessionManager getSession() {
        return mSession;
    }

    public User getUser(){ return mSession.getUser(); }

    public static ApiService api(boolean isV2){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        okhttp3.Request.Builder builder = chain.request().newBuilder()
                                .addHeader("Accept", "application/json;versions=1")
                                .addHeader("Content-Type", "application/json");

                        builder.addHeader("Authorization", getAuthString());
                        return chain.proceed(builder.build());
                    }}).build();

        String host = isV2 ? V2_HOST : HOST;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(host)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();

        return retrofit.create(ApiService.class);
    }

    private static String getAuthString() {
        String token = (getInstance().mSession.isLoggedIn()) ? getInstance().mSession.getUserToken() : getInstance().mSession.getLoginToken();
        return "Token token=\"" + token + "\"";
    }

    public static ApiService chatApi(){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        okhttp3.Request.Builder builder = chain.request().newBuilder()
                                .addHeader("Accept", "application/json;versions=1")
                                .addHeader("Content-Type", "application/json");
                        builder.addHeader("Authorization", getAuthString());
                        return chain.proceed(builder.build());
                    }}).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CHAT_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();

        return retrofit.create(ApiService.class);
    }

    public static ApiService downloadApi(){
        OkHttpClient client = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CHAT_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();

        return retrofit.create(ApiService.class);
    }

    public void checkLogin(){
        final SessionManager session = getInstance().getSession();
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; " +
                "charset=utf-8"), mSession.getLogin().toString());
        SwaggApp.api(false).login(body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()){
                    String string = response.body().getAsJsonObject().toString();
                    try {
                        JSONObject json = new JSONObject(string);
                        JSONObject auth = json.getJSONObject("meta");
                        if (auth.getBoolean("is_logged_in")){
                            User user = new User(json.getJSONObject("user"));
                            session.createSession(user);
                        }else{
                            session.clearSession();
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "Error checking login: " + e.getMessage());
                    }
                }else{
                    session.clearSession();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "Error checking login: " + t.getMessage());
                Log.d(TAG, "Login check failure. Clearing session...");
                session.clearSession();
            }
        });
    }
}

package com.damagecontrolhq.internationalswagg.app;

import com.damagecontrolhq.internationalswagg.BuildConfig;

/**
 * InternationalSwagg
 * Created by dwilson on 8/28/15.
 */
public class DamageControlAPI {
    private static String TAG = DamageControlAPI.class.getSimpleName();

    // Home
//    public static final String BASE_URL = "http://192.168.0.14:5000";
//    public static final String HOST   = "http://192.168.0.14:5000/";
//    public static final String CHAT_URL   = "http://192.168.0.14:4000/";

    // Work
//    public static final String BASE_URL = "http://10.68.60.130:5000";
//    public static final String HOST   = "http://10.68.60.130:5000/";
//    public static final String CHAT_URL = "http://10.68.60.130:4000/";

//    public static final String BASE_URL = "http://10.0.2.2:5000";
//    public static final String HOST   = "http://10.0.2.2:5000/";
//    public static final String CHAT_URL = "http://10.0.2.2:4000/";

    // Staging
    public static final String BASE_URL = BuildConfig.BASE_URL;
    public static final String HOST    = BuildConfig.HOST;
    public static final String CHAT_URL = BuildConfig.CHAT;
    public static final String V2_HOST = HOST + "v2/";

    // Production
//    public static final String BASE_URL = "http://api.damagecontrolhq.com";
//    private static final String HOST   = "http://api.damagecontrolhq.com/";
//    public static final String CHAT_URL = "http://internationalswagg.com";

    public static final String FEED                 = HOST + "feed";
    public static final String LOGIN                = HOST + "login";
    public static final String POSTS                = HOST + "posts";
    public static final String YOUTUBE_KEY          = "AIzaSyCvVOIHKUcJYNJeN8A0HoWiM4NJ4UP_L2I";
}

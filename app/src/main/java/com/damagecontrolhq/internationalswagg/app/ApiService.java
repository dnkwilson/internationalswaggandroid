package com.damagecontrolhq.internationalswagg.app;

import com.damagecontrolhq.internationalswagg.models.AffiliateList;
import com.damagecontrolhq.internationalswagg.models.Checkout;
import com.damagecontrolhq.internationalswagg.models.Event;
import com.damagecontrolhq.internationalswagg.models.EventList;
import com.damagecontrolhq.internationalswagg.models.ProductList;
import com.damagecontrolhq.internationalswagg.models.Reservation;
import com.damagecontrolhq.internationalswagg.models.User;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by dwilson on 1/29/17.
 */

public interface ApiService {

    @GET
    Call<ResponseBody> getFile(@Url String fileUrl);

    @GET("affiliates")
    Call<AffiliateList> getAffiliates();

    @GET("posts")
    Call<JsonObject> getPosts(@Query("post[category]") String category);

    @GET("charity")
    Call<JsonObject> getCharity();

    @GET("check_user")
    Call<User> checkUser(@Query("login") String login);

    @GET("events")
    Call<EventList> getEvents();

    @GET("events")
    Call<ArrayList<Event>> search(@QueryMap Map<String, String> params);

    @GET("galleries/{id}")
    Call<JsonObject> getGallery(@Path("id") int galleryId);

    @GET("generate_token")
    Call<JsonObject> getToken();

    @POST("login")
    Call<JsonObject> login(@Body RequestBody params);

    @GET("music")
    Call<JsonObject> getMusic();

    @GET("packages")
    Call<JsonObject> getPackages();

    @GET("packages/{id}")
    Call<Reservation> getPackage(@Path("id") int packageId);

    @GET("posts")
    Call<JsonObject> getPosts();

    @POST("posts/{id}/vote")
    Call<JsonObject> vote(@Path("id") int postId);

    @GET("products")
    Call<ProductList> getProducts(@Query("product[category_id]") int categoryId,
                                  @Query("product[price]") int price );

    @POST("push_notifications/store_token")
    Call<JsonObject> storePushToken(@Body RequestBody params);

    @POST("registrations")
    Call<User> register(@Body RequestBody params);

    @POST("sale_transactions")
    Call<JsonObject> checkout(@Body Checkout json);

    @GET("sale_transactions/my_orders")
    Call<JsonObject> myOrders();

    @POST("upload")
    Call<JsonObject> chatUpload(@Body RequestBody params);

    @GET("users/{id}")
    Call<JsonObject> getUser(@Path("id") int userId);

    @POST("users")
    Call<JsonObject> updateUser(@Body RequestBody params);

}

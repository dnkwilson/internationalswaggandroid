package com.damagecontrolhq.internationalswagg;


import android.content.Intent;
import android.support.v4.app.Fragment;

import com.damagecontrolhq.internationalswagg.fragments.LoginFragment;

public class LoginActivity extends BaseActivity{

    @Override
    protected Fragment createFragment() {
        return new LoginFragment();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }
}


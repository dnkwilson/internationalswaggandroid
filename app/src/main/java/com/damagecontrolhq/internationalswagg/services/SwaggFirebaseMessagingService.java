package com.damagecontrolhq.internationalswagg.services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.damagecontrolhq.internationalswagg.MainActivity;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.utils.NotificationUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by dwilson on 2/26/17.
 */

public class SwaggFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = SwaggFirebaseMessagingService.class.getName();

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        RemoteMessage.Notification notification = remoteMessage.getNotification();

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            JSONObject json = new JSONObject(remoteMessage.getData());
            Log.i(TAG, "Push received: " + json);

            try {
                String uri = json.getString("uri");

                Intent resultIntent = new Intent(getBaseContext(), MainActivity.class);
                resultIntent.putExtra(SwaggApp.REQUESTED_PAGE, uri);
                showNotificationMessage(getBaseContext(), notification.getTitle(), notification.getBody(), resultIntent);

                parsePushJson(getBaseContext(), json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

//        if (notification != null){
//            JSONObject json;
//            try {
//                json = new JSONObject(notification.toString());
//                JSONObject payload = json.getJSONObject("payload");
//                String uri = payload.getString("uri");
//
//                Intent resultIntent = new Intent(getBaseContext(), MainActivity.class);
//                resultIntent.putExtra(SwaggApp.REQUESTED_PAGE, uri);
//                showNotificationMessage(getBaseContext(), notification.getTitle(), notification.getBody(), resultIntent);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
    }

    private void parsePushJson(Context context, JSONObject json) {
        try {
//            boolean isBackground = json.getBoolean("isBackground");
            JSONObject data = json.getJSONObject("data");
            String title = data.getString("title");
            String message = data.getString("alert");
            String url = data.getString("uri");

            Intent resultIntent = new Intent(context, MainActivity.class);
            resultIntent.putExtra(SwaggApp.REQUESTED_PAGE, url);
            showNotificationMessage(context, title, message, resultIntent);

        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    private void showNotificationMessage(Context context, String title, String message, Intent intent) {

        notificationUtils = new NotificationUtils(context);

        intent.putExtras(intent.getExtras());

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        notificationUtils.showNotificationMessage(title, message, intent);
    }



}

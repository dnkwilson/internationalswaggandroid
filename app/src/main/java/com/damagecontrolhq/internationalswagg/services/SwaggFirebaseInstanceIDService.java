package com.damagecontrolhq.internationalswagg.services;

import android.util.Log;

import com.damagecontrolhq.internationalswagg.BuildConfig;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

import static com.damagecontrolhq.internationalswagg.app.SwaggApp.NOTIFICATION_CHANNEL;

/**
 * Created by dwilson on 2/26/17.
 */

public class SwaggFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = SwaggFirebaseInstanceIDService.class.getName();

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // Subscribe to swagg topic
        String channel = BuildConfig.isProduction ? NOTIFICATION_CHANNEL : NOTIFICATION_CHANNEL + "_debug";
        FirebaseMessaging.getInstance().subscribeToTopic(channel);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        SwaggApp.getInstance().getSession().setFirebaseToken(refreshedToken);

        Log.d(TAG, "Session token: " + SwaggApp.getInstance().getSession().getFirebaseToken());
    }
}

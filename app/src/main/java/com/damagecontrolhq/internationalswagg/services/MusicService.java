package com.damagecontrolhq.internationalswagg.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.damagecontrolhq.internationalswagg.MusicPlayerActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.interfaces.OnMusicServiceListener;
import com.damagecontrolhq.internationalswagg.models.Song;
import com.damagecontrolhq.internationalswagg.views.MusicNotificationView;

import java.util.ArrayList;
import java.util.Random;

/**
 * InternationalSwagg
 * Created by dwilson on 9/22/15.
 */
public class MusicService extends Service implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener{
    private static final String TAG = "MusicService";

    private static final int NOTIFICATION_ID = 1225;

    private OnMusicServiceListener listener = null;

    //media player
    private MediaPlayer player;
    //song list
    private ArrayList<Song> songs;
    //current position
    private int mPosition;
    //binder
    private final IBinder musicBind = new MusicBinder();
    //title of current song
    private String songTitle="";
    //shuffle flag and random
    private boolean shuffle = false;
    private Random rand;

    public void onCreate(){
        //create the service
        super.onCreate();
        //initialize position
        mPosition = 0;
        //random
        rand = new Random();
        //create player
        player = new MediaPlayer();
        //initialize
        initMusicPlayer();
    }

    private void initMusicPlayer(){
        //set player properties
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        //set listeners
        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
    }

    //pass song list
    public void setList(ArrayList<Song> theSongs){
        songs = theSongs;
    }

    //binder
    public class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    public MediaPlayer getPlayer() {
        return player;
    }

    //activity will bind to service
    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    //release resources when unbind
    @Override
    public boolean onUnbind(Intent intent){
        player.stop();
        player.release();
        return false;
    }

    //play a song
    public void playSong(){
        //play
        player.reset();

        //get song
        Song song = songs.get(mPosition);

        //get title
        songTitle = song.getTitle();

        Log.d(TAG, "SONG URL: " + song.getUrl());
        //set the data source
        try{
            player.setDataSource(song.getUrl());
        }
        catch(Exception e){
            Log.e(TAG, "Error setting data source" + e.toString());
        }
        player.prepareAsync();
    }

    public void resumeSong(){
        seek(player.getCurrentPosition());
        player.start();
    }

    //set the song
    public void setSong(int songIndex){
        mPosition = songIndex;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        //check if playback has reached the end of a track
        if(player.getCurrentPosition()>0){
            mp.reset();
            playNext();
            if (listener != null) listener.onSongCompleted();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.v(TAG, "Playback Error");
        mp.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        //start playback
        mp.start();

        if (listener != null) listener.onReady();

//        Intent intent = new Intent(this, MusicPlayerActivity.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Song song = songs.get(mPosition);
        MusicNotificationView notificationView = new MusicNotificationView(this, this.getPackageName(),
                song, R.layout.view_music_notification);
//        notificationView.setOnClickPendingIntent(R.id.layout_id, pendingIntent);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification_icon)
                        .setContentTitle(songTitle)
//                        .setOngoing(true)
                        .setWhen(System.currentTimeMillis())
                        .setContent(notificationView);

        Intent notificationIntent = new Intent(this, MusicPlayerActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(
                this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(contentIntent);
        NotificationManager nManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(NOTIFICATION_ID, mBuilder.build());

        // Broadcast intent to activity to let it know the media player has been prepared
        Intent onPreparedIntent = new Intent("MEDIA_PLAYER_PREPARED");
        LocalBroadcastManager.getInstance(this).sendBroadcast(onPreparedIntent);
    }

    //playback methods
    public int getPosition(){
        return player.getCurrentPosition();
    }

    public int getDuration(){
        return player.getDuration();
    }

    public boolean isPlaying(){
        return player.isPlaying();
    }

    public void pausePlayer(){
        player.pause();
    }

    public void seek(int posn){
        player.seekTo(posn);
    }

    public void go(){
        player.start();
    }

    //skip to previous track
    public void playPrev(){
        mPosition--;
        if(mPosition < 0) mPosition = songs.size() - 1;
        playSong();
    }

    //skip to next
    public void playNext(){
        if(shuffle){
            int newSong = mPosition;
            while (newSong == mPosition){
                newSong = rand.nextInt(songs.size());
            }
            mPosition = newSong;
        }
        else{
            mPosition++;
            if(mPosition >= songs.size()) mPosition = 0;
        }
        playSong();
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    //toggle shuffle
    public void setShuffle(){
        shuffle = !shuffle;
    }

    public void setOnMusicServiceListener(OnMusicServiceListener listener){
        this.listener = listener;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (!TextUtils.isEmpty(action)) {
                switch (action) {
                    case MusicNotificationView.ACTION_PREVIOUS:
                        playPrev();
                        break;
                    case MusicNotificationView.ACTION_NEXT:
                        playNext();
                        break;
                    default:
                        if (isPlaying()) {
                            pausePlayer();
                        } else {
                            playSong();
                        }
                        break;
                }
            }
        }

        if (listener != null) listener.updateUI(mPosition);

        return START_STICKY_COMPATIBILITY;
    }
}

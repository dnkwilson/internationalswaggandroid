package com.damagecontrolhq.internationalswagg.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * InternationalSwagg
 * Created by dwilson on 11/12/15.
 */
public class VoiceNoteService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener  {
    private static final String TAG = "VoiceNoteService";

    private MediaPlayer player;
    private String url;
    private final IBinder vnBind = new VoiceNoteBinder();

    public void onCreate(){
        super.onCreate();
        player = new MediaPlayer();
        initVoiceNotePlayer();
    }

    private void initVoiceNotePlayer(){
        player.setOnCompletionListener(this);
        player.setOnPreparedListener(this);
        player.setOnErrorListener(this);
    }

    public class VoiceNoteBinder extends Binder {
        public VoiceNoteService getService() {
            return VoiceNoteService.this;
        }
    }

    public void setUrl(String url){
        this.url = url;
    }

    public void playSong(){
        // play song
        player.reset();
        try{
            player.setDataSource(url);
            player.prepare();
        }catch (Exception e){
            Log.e(TAG, "Error setting data source: " + e.toString());
        }
    }

    //playback methods
    public int getPosition(){
        return player.getCurrentPosition();
    }

    public int getDuration(){
        return player.getDuration();
    }

    public boolean isPlaying(){
        return player.isPlaying();
    }

    public void pausePlayer(){
        player.pause();
    }

    public void stopPlayer() { player.stop(); }

    public void seekTo(int pos){
        player.seekTo(pos);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return vnBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        player.stop();
        player.reset();
        player.release();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        // start playback
        mp.start();
    }
}

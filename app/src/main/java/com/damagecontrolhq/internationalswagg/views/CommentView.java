package com.damagecontrolhq.internationalswagg.views;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Comment;

/**
 * InternationalSwagg
 * Created by dwilson on 10/24/15.
 */
public class CommentView extends LinearLayout {
    private static final String TAG = "CommentView";

    private ImageView avatar;
    private TextView username;
    private TextView comment;
    private TextView date;

    public CommentView(Context context) {
        super(context);
        initView();
    }

    public CommentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CommentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView(){
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_comment, this);
        this.avatar = (ImageView)this.findViewById(R.id.avatar_img);
        this.username = (TextView)this.findViewById(R.id.username_txt);
        this.comment = (TextView)this.findViewById(R.id.comment_txt);
        this.date = (TextView)this.findViewById(R.id.date_txt);
    }

    public void setItem(Comment c) {
//        ImageLoader imageLoader = SwaggApp.getInstance().getImageLoader();
//        imageLoader.get(c.getAvatar(), new ImageLoader.ImageListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.dc_logo_2014);
//                avatar.setImageDrawable(new RoundImage(bm));
//                Log.e(TAG, "Image Load Error: " + error.getMessage());
//            }
//
//            @Override
//            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                if (response.getBitmap() != null) {
//                    // load image
//                    avatar.setImageDrawable(new RoundImage(response.getBitmap()));
//                }
//            }
//        });
        date.setText(DateUtils.getRelativeTimeSpanString(c.getDate(),
                System.currentTimeMillis(), 0, DateUtils.FORMAT_ABBREV_RELATIVE));
        username.setText(c.getUserName());
        comment.setText(c.getComment());
    }
}

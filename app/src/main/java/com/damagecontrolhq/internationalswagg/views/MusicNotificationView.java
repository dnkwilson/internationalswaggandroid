package com.damagecontrolhq.internationalswagg.views;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Song;

/**
 * InternationalSwagg
 * Created by dwilson on 2/9/16.
 */
@SuppressLint("ParcelCreator")
public class MusicNotificationView extends RemoteViews {
    private final Context mContext;

    public static final String ACTION_PLAY = "com.damagecontrolhq.internationalswagg.ACTION_PLAY";

    public static final String ACTION_PREVIOUS = "com.damagecontrolhq.internationalswagg.ACTION_PREVIOUS";

    public static final String ACTION_NEXT = "com.damagecontrolhq.internationalswagg.ACTION_NEXT";

    public MusicNotificationView(Context context, String packageName, Song song, int layoutId){
        super(packageName, layoutId);
        mContext = context;

        setTextViewText(R.id.artist, song.getArtist());
        setTextViewText(R.id.song, song.getTitle());
//        ImageLoader imageLoader = SwaggApp.getInstance().getImageLoader();
//        imageLoader.get(song.getArtwork(), new ImageLoader.ImageListener() {
//            @Override
//            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
//                if (response.getBitmap() != null) {
//                    setImageViewBitmap(R.id.artwork,response.getBitmap());
//                }
//            }
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        });

        Intent intent = new Intent(ACTION_PLAY);
        PendingIntent pendingIntent = PendingIntent.getService(mContext.getApplicationContext(),100,
                intent,PendingIntent.FLAG_UPDATE_CURRENT);
        setOnClickPendingIntent(R.id.play, pendingIntent);
//        setOnClickPendingIntent(R.id.pause_control,pendingIntent);


        intent = new Intent(ACTION_PREVIOUS);
        pendingIntent = PendingIntent.getService(mContext.getApplicationContext(), 101,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        setOnClickPendingIntent(R.id.previous,pendingIntent);


        intent = new Intent(ACTION_NEXT);
        pendingIntent = PendingIntent.getService(mContext.getApplicationContext(),102,
                intent,PendingIntent.FLAG_UPDATE_CURRENT);
        setOnClickPendingIntent(R.id.next, pendingIntent);
    }
}

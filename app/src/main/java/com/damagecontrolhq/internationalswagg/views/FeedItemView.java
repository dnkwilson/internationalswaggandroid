package com.damagecontrolhq.internationalswagg.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.helpers.SwaggIcon;
import com.damagecontrolhq.internationalswagg.models.InstagramPost;
import com.damagecontrolhq.internationalswagg.utils.ImageUtils;
import com.squareup.picasso.Picasso;

/**
 * InternationalSwagg
 * Created by dwilson on 9/13/15.
 */
public class FeedItemView extends RelativeLayout {

    private ImageView mVideoIcon;
    private ImageView mImageView;

    public FeedItemView(Context context) {
        super(context);
        initViews(context);
    }

    public FeedItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public FeedItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }

    private void initViews(Context context){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_feed_item, this);
        this.mImageView = (ImageView)this.findViewById(R.id.ig_image);
        this.mVideoIcon = (ImageView)this.findViewById(R.id.ig_video_icon);
    }

    private void setVideoIcon(boolean isVideo, int imageWidth){
        if (isVideo){
            mVideoIcon.setScaleType(ImageView.ScaleType.CENTER);
            mVideoIcon.setLayoutParams(new LayoutParams(imageWidth,
                    imageWidth));
            Drawable icon = new SwaggIcon().getDrawable();
            mVideoIcon.setImageDrawable(icon);
            mVideoIcon.setVisibility(VISIBLE);
        }
    }

    public void buildView(InstagramPost post, int imageWidth){
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mImageView.setLayoutParams(new LayoutParams(imageWidth,
                imageWidth));
        Picasso.with(mImageView.getContext()).load(ImageUtils.getImageUrl(post.getImage()))
                .into(mImageView);
        setVideoIcon(post.getIsVideo(), imageWidth);
    }
}

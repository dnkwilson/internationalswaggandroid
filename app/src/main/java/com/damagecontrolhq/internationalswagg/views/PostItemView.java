package com.damagecontrolhq.internationalswagg.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.CommentListActivity;
import com.damagecontrolhq.internationalswagg.LoginActivity;
import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.DamageControlAPI;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.fragments.DashboardFragment;
import com.damagecontrolhq.internationalswagg.helpers.SessionManager;
import com.damagecontrolhq.internationalswagg.models.Post;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * InternationalSwagg
 * Created by dwilson on 10/16/15.
 */
public class PostItemView extends LinearLayout{
    private static final String TAG = "PostItemView";

    private static final int RECOVERY_DIALOG_REQUEST = 1;

    private TextView mTitle;
    private TextView mDate;
    private RelativeLayout mBody;
    private SocialButton mLike;
    private SocialButton mShare;
    private SocialButton mComment;
    private Post mPost;
    private final Context mContext;
    private SessionManager mSession;

    public PostItemView(Context context, Post post) {
        super(context);
        this.mContext = context;
        this.mPost = post;
        initView();
    }

    public PostItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initView();
    }

    public PostItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    private void initView() {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_post_item, this);
        this.mTitle   = (TextView)this.findViewById(R.id.tv_title);
        this.mDate    = (TextView)this.findViewById(R.id.tv_date);
        this.mBody    = (RelativeLayout)this.findViewById(R.id.post_container);
        this.mLike    = (SocialButton)this.findViewById(R.id.btn_like);
        this.mComment = (SocialButton)this.findViewById(R.id.btn_comment);
        this.mShare   = (SocialButton)this.findViewById(R.id.btn_share);
        this.mSession = SwaggApp.getInstance().getSession();

        mLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Like button clicked");
                mLike.setEnabled(false);
                String urlRequest = String.format("%s/%s/vote", DamageControlAPI.POSTS, mPost.getID());
                if (mSession.isLoggedIn()) {
                    voteOnPost(mLike, urlRequest);
                } else {
                    Intent i = new Intent(mContext, LoginActivity.class);
                    i.putExtra(SwaggApp.REQUESTED_PAGE, DashboardFragment.class.getName());
                    i.putExtra(SwaggApp.LOGIN_MESSAGE, "You must login before liking a post");
                    mContext.startActivity(i);
                }
            }
        });

        mComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CommentListActivity.class);
                intent.putExtra("post", mPost);
                Activity activity = (Activity) mContext;
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_up, R.anim.abc_fade_out);
            }
        });
    }



    private void voteOnPost(final SocialButton button, String url) {
        JSONObject params = null;
        try {
            params = new JSONObject().put(Post.KEY_ID, mPost.getID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        ApiRequest apiRequest = new ApiRequest(Request.Method.POST, url, params, mContext);
//        apiRequest.setOnVolleyResponseListener(new OnVolleyResponseListener() {
//            @Override
//            public void onComplete(JSONObject json) {
//                if (json != null) {
//                    try {
//                        boolean hasVoted = json.getJSONObject("meta").getBoolean("user_voted");
//                        if (hasVoted) {
//                            Vote userVote = new Vote(json.getJSONObject("meta").getJSONObject("user_vote"));
//                            mPost.getVotes().add(userVote);
//                        } else {
//                            mPost.getVotes().remove(mPost.userVote(mSession.getUser().getID()));
//                        }
//                        button.updateUI(mPost, "like");
//                        button.setEnabled(true);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onError(Throwable error) {
//
//            }
//        });
    }
}

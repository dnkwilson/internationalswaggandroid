package com.damagecontrolhq.internationalswagg.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.app.SwaggApp;
import com.damagecontrolhq.internationalswagg.models.Post;
import com.damagecontrolhq.internationalswagg.models.User;

/**
 * InternationalSwagg
 * Created by dwilson on 10/19/15.
 */
public class SocialButton extends RelativeLayout{

    private TextView mText;
    private ImageView mImage;
    private Post mPost;

    private OnClickListener listener;

    public SocialButton(Context context) {
        super(context);
        initView(context);
    }

    public SocialButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
        TypedArray attributes = context.obtainStyledAttributes(
                attrs, R.styleable.SocialButton);
        final int N = attributes.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = attributes.getIndex(i);
            switch (attr)
            {
                case R.styleable.SocialButton_btnText:
                    setText(attributes.getString(attr));
                    break;

                case R.styleable.SocialButton_src:
                    setImage(attributes.getResourceId(i, attr));
                    break;
            }
        }
        attributes.recycle();
    }

    public SocialButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(final Context context){
        setFocusable(true);
        setBackgroundColor(Color.TRANSPARENT);
        setClickable(true);

        LayoutInflater inflater = (LayoutInflater)context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_social_button, this);
        this.mText = (TextView)this.findViewById(R.id.tv_text);
        this.mImage = (ImageView)this.findViewById(R.id.iv_image);
    }

    private void setImage(int imageResId){
        mImage.setImageResource(imageResId);
    }

    private void setText(String text){
        mText.setText(text);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_UP) {
            if(listener != null) listener.onClick(this);
        }
        return super.dispatchTouchEvent(event);
    }

    public void updateUI(Post post, String btnType) {
        this.mPost = post;

        int itemCount;
        switch (btnType) {
            case "like":
                itemCount = mPost.getVotes().size();
                likeUI();
                break;
            case "comment":
                itemCount = mPost.getComments().size();
                break;
            default:
                itemCount = 0;
                break;
        }
        addCount(itemCount);
    }

    private void likeUI(){
        User user = SwaggApp.getInstance().getSession().getUser();
        if (mPost.userHasVoted(user.getID())){
            this.mText.setTextColor(ContextCompat.getColor(getContext(), R.color.primary));
            this.setActivated(true);
        }else{
            this.mText.setTextColor(ContextCompat.getColor(getContext(), R.color.divider));
            this.setActivated(false);
        }
    }

    private void addCount(int itemCount){
        if (itemCount > 0){
            this.setText(String.valueOf(itemCount));
        }else {mText.setText(null);}
    }
}

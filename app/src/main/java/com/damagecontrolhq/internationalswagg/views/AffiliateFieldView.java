package com.damagecontrolhq.internationalswagg.views;

import android.content.Context;
import android.content.Intent;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;
import com.damagecontrolhq.internationalswagg.models.Affiliate;
import com.damagecontrolhq.internationalswagg.utils.AppUtils;

/**
 * InternationalSwagg
 * Created by dwilson on 1/15/16.
 */
public class AffiliateFieldView extends LinearLayout {
    private TextView label;
    private TextView field;
    public AffiliateFieldView(Context context) {
        super(context);
        initView(context);
    }

    public AffiliateFieldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public AffiliateFieldView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_affiliate_field, this);
        this.label = (TextView)this.findViewById(R.id.label);
        this.field = (TextView)this.findViewById(R.id.field);
    }

    public void buildField(String icon, String text, int link){
        this.label.setText(icon);
        this.field.setText(text);

        if(link == Linkify.PHONE_NUMBERS){
            Linkify.addLinks(field, link);
            field.setAutoLinkMask(Linkify.PHONE_NUMBERS);
        }else if(link == Linkify.EMAIL_ADDRESSES){
            Linkify.addLinks(this.field, link);
        }

        field.setLinksClickable(true);
        field.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void buildField(String icon, final Affiliate item, final String string){
        this.label.setText(icon);
        String text = "";
        if (string.equals("Instagram")){
            text = item.getInstagram();
        }else if(string.equals("Facebook")){
            text = item.getFacebook();
        }
        this.field.setText(text);
        this.field.setLinksClickable(true);
        this.field.setMovementMethod(LinkMovementMethod.getInstance());
        this.field.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;
                if (string.equals("Instagram")){
                    i = AppUtils.launchInstagram(field.getContext(), item.getInstagram());
                    v.getContext().startActivity(i);
                }else if(string.equals("Facebook")){
                    i = AppUtils.launchFacebook(item.getFacebookId());
                    v.getContext().startActivity(i);
                }
            }
        });
    }
}

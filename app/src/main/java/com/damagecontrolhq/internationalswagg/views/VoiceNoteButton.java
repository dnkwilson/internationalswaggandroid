package com.damagecontrolhq.internationalswagg.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.damagecontrolhq.internationalswagg.R;

/**
 * InternationalSwagg
 * Created by dwilson on 11/8/15.
 */
public class VoiceNoteButton extends RelativeLayout {
    private ProgressBar progressBar;
    private ImageButton button;

    public VoiceNoteButton(Context context) {
        super(context);
    }

    public VoiceNoteButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VoiceNoteButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init(){
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_voice_note_button, this);
        progressBar = (ProgressBar) this.findViewById(R.id.progress);
        button = (ImageButton) this.findViewById(R.id.audio_player_action);
    }
}

package com.damagecontrolhq.internationalswagg.views;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.damagecontrolhq.internationalswagg.R;

/**
 * InternationalSwagg
 * Created by dwilson on 11/28/15.
 */
public class DashIconView extends LinearLayout {
    private ImageView icon;
    private TextView title;
    private LinearLayout container;
    private final Context context;

    public DashIconView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public DashIconView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    public DashIconView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //noinspection SuspiciousNameCombination
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    private void initView(){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_dash_item, this);
        this.icon = (ImageView)this.findViewById(R.id.icon);
        this.title = (TextView)this.findViewById(R.id.title);
        this.container = (LinearLayout)this.findViewById(R.id.container);
    }

    public ImageView getIcon() {
        return icon;
    }

    public void setImage(int image) {
        icon.setImageResource(image);
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(String text) {
        title.setText(text);
    }

    public void setButtonColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            container.setBackground(ContextCompat.getDrawable(context, color));
        }else{
            //noinspection deprecation,deprecation
            container.setBackgroundDrawable(ContextCompat.getDrawable(context, color));
        }
    }
}

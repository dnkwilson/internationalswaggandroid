package com.damagecontrolhq.internationalswagg;

import android.support.v4.app.Fragment;

import com.damagecontrolhq.internationalswagg.fragments.CommentListFragment;

/**
 * InternationalSwagg
 * Created by dwilson on 10/24/15.
 */
public class CommentListActivity extends BaseActivity{

    @Override
    protected Fragment createFragment() {
        return new CommentListFragment();
    }
}

# International Swagg Android App #

### Release 11 (1.6) ###
+ Add support for Android 6
+ Stabilize chatroom connections
+ Linkify to chat messages
+ Tweak profile page
+ Tweak event gallery page
- Use recyclerview for gallery images
